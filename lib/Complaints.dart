import 'package:flutter/material.dart';

class Complaints extends StatefulWidget {
  @override
  _ComplaintsState createState() => _ComplaintsState();
}

class _ComplaintsState extends State<Complaints> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text("Customer Complaints",
              style: TextStyle(color: Colors.white, fontSize: 15))),
    );
  }
}
