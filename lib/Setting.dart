// import 'dart:html';
import 'dart:io';

import 'package:akrbankios/Auth/screens/ChangeNickname.dart';
import 'package:akrbankios/Auth/screens/ChangePassword.dart';
import 'package:akrbankios/Auth/screens/ChangePin.dart';
import 'package:akrbankios/Auth/screens/ForgotPin.dart';
import 'package:akrbankios/Requests/screens/SalaryAdvance.dart';
import 'package:akrbankios/Services/localStorage.dart';
import 'package:akrbankios/Services/serviceLocator.dart';
import 'package:akrbankios/models/userModel.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:path_provider/path_provider.dart';

class Settings extends StatefulWidget {
  final User user;
  final Function onSettingsPressed;
  final Color color;
  // Settings(this.color, this.onSettingsPressed);
  const Settings({Key key, this.onSettingsPressed, this.color, this.user})
      : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text("Settings",
              style: TextStyle(color: Colors.white, fontSize: 15)),
        ),
        body: Stack(
          children: [
            Column(
              children: <Widget>[
                ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChangePassword(),
                    ),
                  ),
                  leading: Icon(
                    OMIcons.vpnKey,
                    size: 20.0,
                    color: Color(0XFFFFCC00),
                  ),
                  trailing: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  ),
                  title: Text(
                    "Change Password",
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                  // trailing: Icon(
                  //   Icons.arrow_right,
                  // ),
                ),
                Divider(),
                ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChangeNickname(),
                    ),
                  ),
                  leading: Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Icon(
                      OMIcons.dialpad,
                      size: 20.0,
                      color: Color(0XFFFFCC00),
                    ),
                  ),
                  trailing: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  ),

                  title: Text(
                    "Change nickname",
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                  // trailing: Icon(Icons.arrow_right),
                ),
                Divider(),
                ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChangePin(),
                    ),
                  ),
                  leading: Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Icon(
                      OMIcons.dialpad,
                      size: 20.0,
                      color: Color(0XFFFFCC00),
                    ),
                  ),
                  trailing: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  ),
                  title: Text(
                    "Change PIN",
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                  // trailing: Icon(Icons.arrow_right),
                ),
                Divider(),
                ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ForgotPin(),
                    ),
                  ),
                  leading: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.questionCircle,
                      size: 20.0,
                      color: Color(0XFFFFCC00),
                    ),
                  ),
                  trailing: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  ),
                  title: Text(
                    "Forgot PIN",
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                  // trailing: Icon(Icons.arrow_right, color: Colors.grey),
                ),
                Divider(),
                ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ForgotPin(),
                    ),
                  ),
                  leading: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.moneyCheck,
                      size: 20.0,
                      color: Color(0XFFFFCC00),
                    ),
                  ),
                  trailing: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  ),
                  title: Text(
                    "Fast Balance Activation",
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                  // trailing: Icon(Icons.arrow_right, color: Colors.grey),
                ),
                Divider(),
                ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ForgotPin(),
                    ),
                  ),
                  leading: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.moneyCheckAlt,
                      size: 20.0,
                      color: Color(0XFFFFCC00),
                    ),
                  ),
                  trailing: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  ),

                  title: Text(
                    "Fast Balance Deactivation",
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                  // trailing: Icon(Icons.arrow_right, color: Colors.grey),
                ),
                Divider(),
                ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ForgotPin(),
                    ),
                  ),
                  leading: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.questionCircle,
                      size: 20.0,
                      color: Color(0XFFFFCC00),
                    ),
                  ),
                  trailing: Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  ),
                  title: Text(
                    "Biometric Login Settings",
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                  // trailing: Icon(Icons.arrow_right, color: Colors.grey),
                ),
                Divider(),
              ],
            )
          ],
        ));
  }
}
