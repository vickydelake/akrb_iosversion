import 'dart:async';

import 'package:akrbankios/Auth/screens/Login_alt.dart';
import 'package:akrbankios/Providers/default.dart';
import 'package:akrbankios/Providers/phone_info.dart';
import 'package:akrbankios/Services/permission_service.dart';
import 'package:akrbankios/common_screens/splash_screen.dart';
import 'package:flutter/material.dart';

import 'package:akrbankios/Services/serviceLocator.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:akrbankios/Services/localStorage.dart';

import 'Auth/screens/Login.dart';
import 'Providers/user_info.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  debugPrint = (String message, {int wrapWidth}) {};
  // Constants.host("");
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Color(0XFF339933),
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarColor: Color(0XFF339933),
    systemNavigationBarIconBrightness: Brightness.light,
  ));

  LocalStorageService storageService = locator<LocalStorageService>();
  String name = storageService.username;

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<Default>.value(value: Default()),
        ChangeNotifierProvider<UserInfo>.value(value: UserInfo()),
        ChangeNotifierProvider<PhoneInfo>.value(value: PhoneInfo()),
        Provider<PermissionsService>(
          create: (context) => PermissionsService(),
        ),
      ],
      child: MyApp(name: name),
    ),
  );
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  final String name;

  const MyApp({Key key, this.name}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  Timer _timer;

  void initState() {
    super.initState();
    _initializeTimer();
  }

  LocalStorageService storageService = locator<LocalStorageService>();

  void _initializeTimer() {
    if (_timer != null) {
      _timer.cancel();
    }

    _timer = Timer(const Duration(minutes: 30), _logOutUser);
  }

  Future<void> _logOutUser() async {
    _timer?.cancel();
    _timer = null;
    String name = storageService.username;
    // Popping all routes and pushing login
    print('logged out..');
    _navigatorKey.currentState.pushAndRemoveUntil(
      MaterialPageRoute(
          builder: (BuildContext context) => LoginPage(
                name: name,
              )),
      ModalRoute.withName('/'),
    );
  }

  void _handleUserInteraction([_]) {
    _initializeTimer();
    print('INTERACTING!!!');
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: _handleUserInteraction,
      onPanDown: _handleUserInteraction,
      child: MaterialApp(
        navigatorKey: _navigatorKey,
        debugShowCheckedModeBanner: false,
        title: 'AKR Bank Mobile',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: SplashScreen(),
      ),
    );
  }
}
