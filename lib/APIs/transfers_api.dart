import 'dart:convert';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/models/security_question.dart';
import 'package:akrbankios/models/validated_customer.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:akrbankios/models/bioSetupModel.dart';
import 'package:akrbankios/models/forgotPassModel.dart';
import 'package:akrbankios/models/passSetupModel.dart';
import 'package:akrbankios/models/pinConfigModel.dart';

import 'package:akrbankios/models/changePasswordModel.dart';

import 'package:akrbankios/models/userModel.dart';

class Transfers {
  static String hostUrl = Constants.baseURL;
  static final dynamic headers = Constants.headers;

  static Future<PinConfigModel> pinSetup(
    String pin,
    String token,
    uniqueId,
    model,
    manufacturer,
    brand,
    country,
  ) async {
    final url = "$hostUrl/pinsetup";

    final response = await http.post(
      url,
      headers: headers,
      body: {
        "authToken": token,
        "pin": pin,
        "uniqueId": uniqueId,
        "model": model,
        "manufacturer": manufacturer,
        "brand": brand,
        "country": country,
      },
    );

    if (response.statusCode == 200) {
      PinConfigModel pinSetupResponse = pinConfigModelFromJson(response.body);
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (pinSetupResponse.mbResponse == '00') {
        print('PIN SETUP SUCCESS');
        return pinSetupResponse;
      } else {
        throw PlatformException(
          code: 'PIN_SETUP_ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: 'An error occurred. Please try again',
      );
    }
  }

  static Future<ForgetPassModel> forgetpassword(
    String username,
    String password,
    String ans,
    String ques,
    String deviceId,
    deviceIp,
    model,
    manufacturer,
    brand,
    country,
  ) async {
    final url = "$hostUrl/forgotpwd";

    final response = await http.post(url, headers: headers, body: {
      "userId": username,
      "password": password,
      "secQuestion": ques,
      "secAnswer": ans,
      "deviceId": deviceId,
      "deviceIp": deviceIp,
      "model": model,
      "manufacturer": manufacturer,
      "brand": brand,
      "country": country,
    });

    if (response.statusCode == 200) {
      ForgetPassModel passResponse = forgetPassModelFromJson(response.body);
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (passResponse.mbResponse == '00') {
        print('PASSWORD CREATION SUCCESS');
        return passResponse;
      } else {
        throw PlatformException(
          code: 'PASSWORD_SETUP_ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: 'An error occurred. Please try again',
      );
    }
  }

  static Future<User> login(
    String username,
    String password,
    String deviceId,
    String deviceIP,
    String model,
    String manufacturer,
    String brand,
    String country,
    String deviceOS,
    String appVersion,
    String fbcmToken,
  ) async {
    print('trying to login');

    final response = await http.post(
      "$hostUrl/signin",
      headers: headers,
      body: {
        "phoneNumber": username,
        "password": password,
        "device_id": deviceId,
        "device_ip": deviceIP,
        "model": model,
        "manufacturer": manufacturer,
        "brand": brand,
        "country": country,
        "deviceOS": deviceOS,
        "appVersion": appVersion,
        "fbcmToken": fbcmToken
      },
    );

    if (response.statusCode == 200) {
      print(response.body);
      User user = userFromJson(response.body);
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (user.mbResponse == '00') {
        print('LOGIN SUCCESS');
        return user;
      } else {
        throw PlatformException(
          code: 'SIGN_IN_ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: 'An error occurred. Please try again',
      );
    }
  }

  static Future<User> biologin(
    String username,
    String deviceID,
    String deviceIP,
    String model,
    String manufacturer,
    String brand,
    String country,
    String os,
    String appVersion,
    String fbcmToken,
  ) async {
    final response = await http.post(
      "$hostUrl/signinBio",
      body: {
        "phoneNumber": username,
        "deviceId": deviceID,
        "device_ip": deviceIP,
        "model": model,
        "manufacturer": manufacturer,
        "brand": brand,
        "country": country,
        "deviceOS": os,
        "appVersion": appVersion,
        "fbcmToken": fbcmToken
      },
      headers: headers,
    );

    // final questions = await http.post(
    //   "${ApiConstants.hostUrl}/getUserQues",
    //   body: {"authToken": result['tokenId']},
    //   headers: ApiConstants.headers,
    // );
    // Map<String, dynamic> qResult = json.decode(questions.body.toString());

    // String questionsResult = qResult['mb_accDetails'];
    // final questionsList = List<Question>();
    // List<String> afterSplit = questionsResult.split("`");
    // afterSplit.removeLast();
    // afterSplit.forEach((value) {
    //   List<String> list = value.split('~');
    //   Question question = Question(code: list[0], description: list[1]);
    //   questionsList.add(question);
    // });

    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      User user = userFromJson(response.body);
      if (user.mbResponse == '00') {
        print('BIO LOGIN SUCCESS');
        return user;
      } else {
        throw PlatformException(
          code: 'SIGN_IN_ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: 'An error occurred. Please try again',
      );
    }
  }

  static Future<PassConfigModel> passSetup(
    String username,
    String password,
    String token,
    String ques,
    String ans,
    deviceId,
    deviceIp,
    model,
    brand,
    country,
  ) async {
    final url = "$hostUrl/initialPasswordChange";

    final response = await http.post(
      url,
      body: {
        "authToken": token,
        "password": password,
        "secQuestion": ques,
        "secAnswer": ans,
        "deviceId": deviceId,
        "deviceIp": deviceIp,
        "model": model,
        "brand": brand,
        "country": country,
        "userId": username,
      },
      headers: headers,
    );

    if (response.statusCode == 200) {
      PassConfigModel passResponse = passConfigModelFromJson(response.body);
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (passResponse.mbResponse == '00') {
        print('PASSWORD SETUP SUCCESS');
        return passResponse;
      } else {
        throw PlatformException(
          code: 'PASSWORD_SETUP_ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: 'An error occurred. Please try again',
      );
    }
  }

  static Future<ChangepasswordModel> changePassword(
    String token,
    String oldpassword,
    String newpassword,
    String ans,
    String deviceIP,
  ) async {
    final url = "$hostUrl/passwordChange";

    final response = await http.post(
      url,
      body: {
        "authToken": token,
        "oldPassword": oldpassword,
        "newPassword": newpassword,
        "secAnswer": ans,
        "deviceIp": deviceIP
      },
      headers: headers,
    );

    if (response.statusCode == 200) {
      ChangepasswordModel passResponse =
          changepasswordModelFromJson(response.body);
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (passResponse.mbResponse == '00') {
        print('PASSWORD CHANGE SUCCESS');
        return passResponse;
      } else {
        throw PlatformException(
          code: 'PASSWORD_CHANGE_ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: 'An error occurred. Please try again',
      );
    }
  }

  static Future<BioSetupModel> activateBiometric(
    String token,
    String secPin,
    String uniqueID,
    String ans,
    String deviceIP,
  ) async {
    final url = "$hostUrl/bioSetup";

    final response = await http.post(
      url,
      body: {
        "authToken": token,
        "secPin": secPin,
        "uniqueId": uniqueID,
        "secAnswer": ans,
        "deviceIp": deviceIP
      },
      headers: headers,
    );

    if (response.statusCode == 200) {
      BioSetupModel bioSetupResponse = bioSetupModelFromJson(response.body);
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (bioSetupResponse.mbResponse == '00') {
        print('BIO SETUP SUCCESS');
        return bioSetupResponse;
      } else {
        throw PlatformException(
          code: 'BIOMETRIC_SETUP_ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: 'An error occurred. Please try again',
      );
    }
  }

  static Future<SecurityQuestion> getSecurityQuestion(String userID) async {
    final url = "$hostUrl/getUserQues";

    final response = await http.post(
      url,
      body: {"authToken": userID},
      headers: headers,
    );

    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        SecurityQuestion question = securityQuestionFromJson(response.body);
        return question;
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }

//getQuestions
  static Future<List<MbQuestion>> getQuestions(String authToken) async {
    final url = "$hostUrl/getquestions";

    final response = await http.post(
      url,
      body: {"authToken": authToken},
      headers: headers,
    );

    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        String questionsResult = result['mb_accDetails'];
        final questionsList = List<MbQuestion>();
        List<String> afterSplit = questionsResult.split("`");
        afterSplit.removeLast();
        afterSplit.forEach((value) {
          List<String> list = value.split('~');
          MbQuestion question = MbQuestion(code: list[0], question: list[1]);
          questionsList.add(question);
        });
        return questionsList;
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }

  static Future<ValidatedCustomer> validateCustomer(
      Map<String, String> details) async {
    details.forEach((key, value) => print('$key: $value'));

    final response = await http.post(
      "$hostUrl/validateCustomer",
      body: {
        "customerNumber": details['customerNumber'],
        "deviceIp": details['deviceIp'],
        "deviceId": details['deviceId'],
        "deviceModel": details['deviceModel'],
        "deviceManufacturer": details['deviceManufacturer'],
        "deviceBrand": details['deviceBrand'],
        "deviceCountry": details['deviceCountry'],
        "deviceOs": details['deviceOs'],
        "appVersion": details['appVersion'],
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      ValidatedCustomer validatedUser =
          validatedCustomerFromJson(response.body);
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (validatedUser.mbResponse == '00') {
        return validatedUser;
      } else {
        throw PlatformException(
          code: 'CUST_VALIDATION_ERROR',
          message: result['mb_idType'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }

  static Future<String> confirmCustomerDetails(
      Map<String, String> details) async {
    details.forEach((key, value) => print('$key: $value'));

    final response = await http.post(
      "$hostUrl/confirmCustomerDetails",
      body: {
        "customerNumber": details['customerNumber'],
        "dateOfBirth": details['dateOfBirth'],
        "idNumber": details['idNumber'],
        "telephone": details['telephone'],
        "authToken": details['authToken'],
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_message'];
      } else {
        throw PlatformException(
          code: 'SIGN_IN_ERROR',
          message: result['mb_message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }

  static Future<String> registerCustomer(Map<String, String> details) async {
    details.forEach((key, value) => print('$key: $value'));

    final response = await http.post(
      "$hostUrl/registerCustomer",
      body: {
        "customerNumber": details['customerNumber'],
        "secPin": details['secPin'],
        "authToken": details['authToken'],
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_message'];
      } else {
        throw PlatformException(
          code: 'SIGN_IN_ERROR',
          message: result['mb_message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }
}
