import 'dart:convert';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/models/accounts.dart';
import 'package:akrbankios/models/beneficiary.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class Beneficiaries {
  static String hostUrl = Constants.baseURL;
  static final dynamic headers = Constants.headers;

  static Future<List<MbBeneficiary>> getBeneficiaries(String authToken) async {
    final response = await http.post(
      "$hostUrl/getIntBeneficiaries",
      body: {
        "authToken": authToken,
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        List<MbBeneficiary> beneficiaryList =
            beneficiaryFromJson(response.body).mbBeneficiary;
        return beneficiaryList;
      } else {
        throw PlatformException(
          code: result['mb_response'],
          message: result['mb_message'] ?? 'Could not fetch beneficiaries',
        );
      }
    }
    return null;
  }

  static Future<String> createBeneficiary(Map<String, String> details) async {
    final response = await http.post(
      "$hostUrl/createIntBeneficiary",
      body: {
        "accountNumber": details["accountNumber"],
        "accountName": details["accountName"],
        "telephone": details["telephone"],
        "authToken": details["authToken"],
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_message'];
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'] ?? 'Could not fetch beneficiaries',
        );
      }
    }
    return null;
  }

  static Future<String> editBeneficiary(Map<String, String> details) async {
    final response = await http.post(
      "$hostUrl/editIntBeneficiary",
      body: {
        "accountNumber": details["accountNumber"],
        "accountName": details["accountName"],
        "telephone": details["telephone"],
        "beneficiaryCode": details["beneficiaryCode"],
        "authToken": details["authToken"],
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_message'];
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'],
        );
      }
    }
    return null;
  }

  static Future<String> deleteBeneficiary(Map<String, String> details) async {
    final response = await http.post(
      "$hostUrl/deleteBeneficiary",
      body: {
        "beneficiaryCode": details["beneficiaryCode"],
        "authToken": details["authToken"],
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_message'];
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'],
        );
      }
    }
    return null;
  }

  static Future<Accounts> getAccounts(String authToken) async {
    final response = await http.post("$hostUrl/getacc",
        body: {
          "authToken": authToken,
        },
        headers: headers);
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['responseCode'] == '00') {
        Accounts accounts = accountsFromJson(response.body);
        return accounts;
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }

  static Future<String> checkValidAmount(Map<String, String> details) async {
    final response = await http.post("$hostUrl/checkValidAmount",
        body: {
          "amount": details['amount'],
          "transType": details['transType'],
        },
        headers: headers);
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_response'];
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }

  static Future<String> getAmountLimit(String transType) async {
    final response = await http.post(
      "$hostUrl/getAmountLimit",
      body: {
        "transType": transType,
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_data'];
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }

  static Future<String> getAccountName(Map<String, String> details) async {
    final response = await http.post(
      "$hostUrl/getAccountName",
      body: {
        "accountNumber": details['accountNumber'],
        "authToken": details['authToken'],
      },
      headers: headers,
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      Map<String, dynamic> result = json.decode(response.body.toString());
      if (result['mb_response'] == '00') {
        return result['mb_message'];
      } else {
        throw PlatformException(
          code: 'ERROR',
          message: result['mb_message'],
        );
      }
    } else {
      throw PlatformException(
        code: response.statusCode.toString(),
        message: response.body,
      );
    }
  }
}
