enum MenuItem {
  balances,
  transfers,
  direct_debit,
  airtime,
  payments,
  requests,
  beneficiaries,
  complaints,
  branches,
}
