import 'dart:core';

import 'package:flutter/material.dart';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/common_widgets/uidata.dart';

class FundsTransferAccountCard extends StatelessWidget {
  final String accNum;
  final String accName;
  final String accType;
  final String availBalance;
  final String currency;

  const FundsTransferAccountCard(
      {Key key,
      this.accNum,
      this.accName,
      this.accType,
      this.availBalance,
      this.currency})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print('acctype: $accType');
    if (accName.isNotEmpty && accNum.isNotEmpty) {
      return accType != null
          ? Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff000000).withOpacity(0.3),
                    blurRadius: 1,
                    offset: Offset(0, 1.0),
                  )
                ],
              ),
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    accNum,
                    style: TextStyle(
                      fontSize: 17.0,
                      color: Colors.black87,
                      // fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    accName.toUpperCase(),
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.grey[700],
                      // fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      accType,
                      style: TextStyle(
                        fontSize: 14.0, color: Colors.black45,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          availBalance.isEmpty
                              ? ""
                              : '$currency ${Utilities.formatAmounts(availBalance)}',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: UIData.appPrimaryColor,
                            // fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.end,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          : Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff000000).withOpacity(0.3),
                    blurRadius: 1,
                    offset: Offset(0, 1.0),
                  )
                ],
              ),
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    accNum,
                    style: TextStyle(
                      fontSize: 17.0,
                      color: Colors.black87,
                      // fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    accName.toUpperCase(),
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.grey[700],
                      // fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            );
    } else {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Color(0xff000000).withOpacity(0.3),
              blurRadius: 1,
              offset: Offset(0, 1.0),
            )
          ],
        ),
        width: double.infinity,
        height: 110.0,
        child: Center(
          child: Text(
            'Tap to select account',
            style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
          ),
        ),
      );
    }
  }
}
