import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'enums/menuItems.dart';

class HomeMenuItems extends StatelessWidget {
  const HomeMenuItems({Key key, this.onSelectItem}) : super(key: key);
  // final MenuItem item;
  final ValueChanged<MenuItem> onSelectItem;

  // Color _color(MenuItem item) =>
  //     this.item == item ? UIData.appPrimaryColorDark : Colors.grey;

  static const Map<MenuItem, IconData> icons = {
    MenuItem.balances: OMIcons.accountBalanceWallet,
    MenuItem.transfers: OMIcons.send,
    MenuItem.direct_debit: OMIcons.transferWithinAStation,
    MenuItem.airtime: OMIcons.phoneAndroid,
    MenuItem.payments: FontAwesomeIcons.moneyBillAlt,
    MenuItem.requests: OMIcons.roomService,
    MenuItem.beneficiaries: OMIcons.people,
    MenuItem.complaints: OMIcons.receipt,
    MenuItem.branches: OMIcons.accountBalance,
  };
  static const Map<MenuItem, String> names = {
    MenuItem.balances: 'Balances',
    MenuItem.transfers: 'Transfers',
    MenuItem.direct_debit: 'Direct Debit',
    MenuItem.airtime: 'Airtime',
    MenuItem.payments: 'Payments',
    MenuItem.requests: 'Requests',
    MenuItem.beneficiaries: 'Beneficiaries',
    MenuItem.complaints: 'Complaints',
    MenuItem.branches: 'Branches',
  };

  Widget _buildItem(MenuItem item) {
    return Builder(
      builder: (context) => GestureDetector(
        onTap: () => onSelectItem(item),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.24,
          height: MediaQuery.of(context).size.height * 0.118,
          padding: EdgeInsets.all(7.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 3,
                blurRadius: 4,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                icons[item],
                size: 30.0,
                color: Color(0XFF339933),
                // color: Color(0xff4f4ff0),
              ),
              SizedBox(height: 15.0),
              Text(
                names[item],
                style: TextStyle(
                  color: Colors.grey[800],
                  fontSize: 14.0,
                  height: 1.0,
                ),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.46,
      padding: EdgeInsets.symmetric(
        horizontal: _width * 0.02,
        vertical: _height * 0.032,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildItem(MenuItem.values[0]),
              _buildItem(MenuItem.values[1]),
              _buildItem(MenuItem.values[2]),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildItem(MenuItem.values[3]),
              _buildItem(MenuItem.values[4]),
              _buildItem(MenuItem.values[5]),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildItem(MenuItem.values[6]),
              _buildItem(MenuItem.values[7]),
              _buildItem(MenuItem.values[8]),
            ],
          ),
        ],
      ),
    );
  }
}
