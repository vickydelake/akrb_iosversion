import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class AccountCard extends StatefulWidget {
  final AccountInfo accountInfo;
  final Function(bool) onActivated;

  const AccountCard({Key key, this.accountInfo, this.onActivated})
      : super(key: key);
  @override
  _AccountCardState createState() => _AccountCardState();
}

class _AccountCardState extends State<AccountCard> {
  double _screenHeight;
  double _screenWidth;
  bool cardState = false;
  Color color;
  @override
  Widget build(BuildContext context) {
    _screenHeight = MediaQuery.of(context).size.height;
    _screenWidth = MediaQuery.of(context).size.width;

    switch (widget.accountInfo.acc_type) {
      case 'Current Account':
        color = CupertinoColors.activeBlue.withOpacity(0.8);
        break;
      case 'Savings Account':
        color = CupertinoColors.activeGreen.withOpacity(0.8);
        break;
      case 'Investment Account':
        color = CupertinoColors.activeOrange.withOpacity(0.8);
        break;
      default:
    }

    return GestureDetector(
      onTap: () {
        setState(() {
          cardState = !cardState;
          widget.onActivated(cardState);
        });
      },
      child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background2.jpg'),
              fit: BoxFit.fill,
              // colorFilter: ColorFilter.mode(Colors.blue, BlendMode.difference),
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(25.0),
            ),
            // color: Color(0XFF339933).withOpacity(0.3),
          ),
          child: Container(
            height: _screenHeight * 0.2,
            // width: _screenWidth * 0.58,
            // margin: EdgeInsets.symmetric(horizontal: _screenWidth * 0.04),
            padding: EdgeInsets.symmetric(
                vertical: _screenHeight * 0.01,
                horizontal: _screenWidth * 0.025),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(25.0),
              ),
              color: color.withOpacity(0.75),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage("assets/images/akrblogo_big.png"),
                      width: _screenWidth * 0.08,
                      height: _screenHeight * 0.04,
                    ),
                    // SizedBox(height: 20.0),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            widget.accountInfo.acc_type,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.95),
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.7,
                            ),
                          ),
                          SizedBox(width: 10.0),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 25.0),
                Row(
                  children: [
                    SizedBox(width: 40.0),
                    Text(
                      widget.accountInfo.acc_number,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white.withOpacity(0.8),
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.0,
                      ),
                    ),
                  ],
                ),
                // SizedBox(height: _screenHeight * 0.001),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: _screenWidth * 0.04,
                      height: _screenHeight * 0.075,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          width: 2.0,
                          color: Colors.transparent,
                        ),
                      ),
                    ),
                    Text(
                      widget.accountInfo.acc_name.toUpperCase(),
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white70,
                      ),
                    ),
                    // SizedBox(width: 50.0),
                    Switch.adaptive(
                        value: cardState,
                        onChanged: (val) {
                          setState(() {
                            cardState = !cardState;
                          });
                          widget.onActivated(cardState);
                        })
                  ],
                ),
              ],
            ),
          )),
    );
  }
}

class AccountInfo {
  final acc_name;
  final acc_bal;
  final acc_number;
  final acc_type;
  final acc_currency;

  AccountInfo(this.acc_name, this.acc_bal, this.acc_number, this.acc_type,
      this.acc_currency);
}
