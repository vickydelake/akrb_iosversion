import 'package:flutter/material.dart';

class AnimatedLogo extends StatelessWidget {
  Widget build(context) {
    double _screenHeight = MediaQuery.of(context).size.height;
    double _screenWidth = MediaQuery.of(context).size.width;

    return Image.asset(
      "assets/images/akrblogo_big.png",
      width: _screenWidth * 0.90,
      height: _screenHeight * 0.45,
    );
  }
}
