import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class SearchBar extends StatelessWidget {
  final searchController;
  final callback;
  final double width;
  final Color color;

  const SearchBar(
      {Key key, this.searchController, this.callback, this.width, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15.0, right: 6.5, top: 2.5, bottom: 2.5),
      width: width != null ? width : double.infinity,
      decoration: BoxDecoration(
        color: color != null ? color : Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(35.0),
        ),
      ),
      child: TextField(
        controller: searchController,
        keyboardType: TextInputType.text,
        style: TextStyle(fontSize: 18.0),
        onChanged: callback,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Search .....",
          hintStyle: TextStyle(color: Colors.grey[400]),
          suffixIcon: Icon(CupertinoIcons.search),
        ),
      ),
    );
  }
}
