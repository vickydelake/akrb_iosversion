import 'package:flutter/material.dart';

class StepperProgress extends StatelessWidget {
  final int position;
  final List<String> tabs;

  const StepperProgress({this.position, this.tabs});
  @override
  Widget build(BuildContext context) {
    final _height = MediaQuery.of(context).size.height;
    // final _width = MediaQuery.of(context).size.width;
    return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        height: _height * 0.04,
        child: Row(
          children: [
            position == 0
                ? Active(number: 1)
                : position > 0
                    ? Completed()
                    : InActive(number: 1),
            position > 0 ? ActiveLine() : InActiveLine(),
            position == 1
                ? Active(
                    number: 2,
                  )
                : position > 1
                    ? Completed()
                    : InActive(number: 2),
            position > 1 ? ActiveLine() : InActiveLine(),

            position == 2
                ? Active(
                    number: 3,
                  )
                : position > 2
                    ? Completed()
                    : InActive(number: 3),
            position > 2 ? ActiveLine() : InActiveLine(),
            position == 3
                ? Active(
                    number: 4,
                  )
                : position > 3
                    ? Completed()
                    : InActive(number: 4),

            // InActiveLine(),
            // InActive(
            //   number: 2,
            // ),

            // InActive(
            //   number: 3,
            // ),
            // InActiveLine(),
            // InActive(
            //   number: 4,
            // ),
          ],
        )
        // Row(children: [
        //   for (var i = 0; i < length; i++)
        //   // if () {

        //   // } else {
        //   // }
        // ],)
        );
  }
}

class Active extends StatelessWidget {
  final int number;

  const Active({Key key, this.number}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.green,
          )),
      child: Center(
        child: Text(
          number.toString(),
          style: TextStyle(
            color: Colors.green,
            fontSize: 9,
          ),
        ),
      ),
    );
  }
}

class InActive extends StatelessWidget {
  final int number;

  const InActive({Key key, this.number}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.grey,
          )),
      child: Center(
        child: Text(
          number.toString(),
          style: TextStyle(
            color: Colors.grey,
            fontSize: 9,
          ),
        ),
      ),
    );
  }
}

class Completed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 10,
      backgroundColor: Colors.green,
      child: Icon(
        Icons.check,
        color: Colors.white,
        size: 10,
      ),
    );
  }
}

class InActiveLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: 1,
        color: Colors.grey,
      ),
    );
  }
}

class ActiveLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: 1,
        color: Colors.green,
      ),
    );
  }
}

class Steps extends StatelessWidget {
  final int position;

  const Steps({Key key, this.position}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    switch (position) {
      case 1:
        return Row(
          children: [
            Active(number: 1),
            InActiveLine(),
            InActive(number: 2),
            InActiveLine(),
            InActive(number: 3),
            InActiveLine(),
            InActive(number: 4),
            InActiveLine(),
            InActive(number: 5),
          ],
        );
        break;
      case 2:
        return Row(
          children: [
            Completed(),
            InActiveLine(),
            Active(number: 2),
            InActiveLine(),
            InActive(number: 3),
            InActiveLine(),
            InActive(number: 4),
            InActiveLine(),
            InActive(number: 5),
          ],
        );
        break;
      case 3:
        return Row(
          children: [
            Completed(),
            InActiveLine(),
            Completed(),
            InActiveLine(),
            Active(number: 3),
            InActiveLine(),
            InActive(number: 4),
            InActiveLine(),
            InActive(number: 5),
          ],
        );
        break;
      // case 1:
      //   return Row(
      //     children: [
      //       InActive(
      //         number: 1,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 2,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 3,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 4,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 5,
      //       ),
      //     ],
      //   );
      //   break;
      // case 1:
      //   return Row(
      //     children: [
      //       InActive(
      //         number: 1,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 2,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 3,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 4,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 5,
      //       ),
      //     ],
      //   );
      //   break;
      // case 1:
      //   return Row(
      //     children: [
      //       InActive(
      //         number: 1,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 2,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 3,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 4,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 5,
      //       ),
      //     ],
      //   );
      //   break;
      // case 1:
      //   return Row(
      //     children: [
      //       InActive(
      //         number: 1,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 2,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 3,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 4,
      //       ),
      //       InActiveLine(),
      //       InActive(
      //         number: 5,
      //       ),
      //     ],
      //   );
      //   break;

      // default:
    }
  }
}
