import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:akrbankios/common_widgets/uidata.dart';

class Utilities {
  static String formatAmounts(dynamic amt) {
    String formattedAmt;
    final formatter = new NumberFormat("#,##0.00", "en_US");
    // final formatter = new NumberFormat.simpleCurrency();

    if (amt == null || amt.isEmpty || amt == '0') {
      amt = "0,000.00";
      formattedAmt = amt;
    } else {
      double amtDouble = double.parse(amt);
      formattedAmt = formatter.format(amtDouble);
    }

    return formattedAmt;
  }

  static String dateFormat(DateTime date) {
    return DateFormat("dd-MMM-yyyy").format(date);
    // return DateFormat.d().add_yMMM().format(date);
  }

  static String dateFormat2(DateTime date) {
    return DateFormat("M/yy").format(date);
    // return DateFormat.d().add_yMMM().format(date);
  }

  static String dateFormat3(DateTime date) {
    return DateFormat("dd-MMM-yy").format(date).toUpperCase();
    // return DateFormat.d().add_yMMM().format(date);
  }

  static String formatCardNo(String cardNo) {
    String _cardNo = cardNo.substring(0, 3) +
        cardNo
            .substring(3, cardNo.length - 3)
            .replaceAll(RegExp(r'[0-9]'), '*') +
        cardNo.substring(cardNo.length - 3);

    StringBuffer _formattedCardNumber = StringBuffer();
    for (int i = 0; i < _cardNo.length; i++) {
      if (i % 4 == 0 && i != 0) {
        _formattedCardNumber.write(" ");
      }
      _formattedCardNumber.write(_cardNo[i]);
    }

    return _formattedCardNumber.toString();
  }

  static String getGreeting() {
    DateTime dateTime = DateTime.now();
    int hours = dateTime.hour;
    print('hours: $hours');
    String greeting;
    if (hours >= 00 && hours < 12) {
      greeting = 'Good morning';
    } else if (hours >= 12 && hours < 16) {
      greeting = "Good afternoon";
    } else if (hours >= 16 && hours <= 23) {
      greeting = 'Good evening';
    }
    return greeting;
  }

  static void showSuccessDialog2(
      BuildContext context, String successMessage, Function onButtonPressed) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: Text(
          'Successful',
        ),
        content: Text(
          successMessage ?? '',
          style: TextStyle(
            color: Colors.black54,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'OK',
              style: TextStyle(
                color: UIData.appPrimaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            onPressed: () {
              onButtonPressed();
            },
          ),
        ],
      ),
    );
  }

  static void showSuccessDialog(
      BuildContext context, String successMessage, Function onButtonPressed) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
          Radius.circular(4.0),
        )),
        backgroundColor: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              color: Colors.green[600],
              padding: EdgeInsets.all(5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.check_circle_outline,
                    color: Colors.white,
                    size: 36.0,
                  ),
                  SizedBox(width: 8.0),
                  Text(
                    "Successful",
                    style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(
                8.0,
                16.0,
                16.0,
                8.0,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      successMessage ?? '',
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black54,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(
                8.0,
                16.0,
                16.0,
                8.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: FlatButton(
                      color: Colors.blue,
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      onPressed: () {
                        onButtonPressed();
                      },
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  static void showInfoDialog(BuildContext context, String title, String message,
      String buttonText, Function onButtonPressed) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => AlertDialog(
        title: Text(
          title ?? 'Info',
        ),
        content: Text(
          message,
          style: TextStyle(
            color: Colors.black54,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(
              buttonText.toUpperCase() ?? 'OK',
              style: TextStyle(
                color: UIData.appPrimaryColor,
                fontSize: 15.0,
              ),
            ),
            onPressed: () => onButtonPressed(),
          ),
        ],
      ),
    );
  }

  static void showErrorDialog2(
      BuildContext context, PlatformException exception) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: Text(
          'Operation failed',
        ),
        content: Text(
          exception.message ?? 'Error',
          style: TextStyle(
            color: Colors.black54,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'OK',
              style: TextStyle(
                color: UIData.appPrimaryColor,
                fontSize: 15.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            onPressed: () => Navigator.of(context).pop(true),
          ),
        ],
      ),
    );
  }

  static void showErrorDialog(BuildContext context, PlatformException exception,
      {Function onButtonPressed}) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
          Radius.circular(8.0),
        )),
        backgroundColor: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              color: Colors.red[600],
              padding: EdgeInsets.all(5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.error_outline,
                    color: Colors.white,
                    size: 36.0,
                  ),
                  SizedBox(width: 8.0),
                  Text(
                    "Operation failed",
                    style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(
                8.0,
                16.0,
                16.0,
                8.0,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      exception.message ?? 'Error',
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black54,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(
                8.0,
                16.0,
                16.0,
                8.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Spacer(),
                  Expanded(
                    flex: 2,
                    child: FlatButton(
                      color: Colors.blue,
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      onPressed: () => onButtonPressed != null
                          ? onButtonPressed()
                          : Navigator.of(context).pop(true),
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Constants {
  static const int pinCodeLength = 4;

  static String baseURL =
      "https://app1.akrbank.com.gh:8685/MbankApi-test/webresources/unionsys";

  static final dynamic headers = {
    "Content-Type": "application/x-www-form-urlencoded"
  };
}
