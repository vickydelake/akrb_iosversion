import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class UIData {
  //routes
  // static const String homeRoute = "/home";
  // static const String profileOneRoute = "/View Profile";
  // static const String profileTwoRoute = "/Profile 2";
  // static const String notFoundRoute = "/No Search Result";
  // static const String timelineOneRoute = "/Feed";
  // static const String timelineTwoRoute = "/Tweets";
  // static const String settingsOneRoute = "/Device Settings";
  // static const String shoppingOneRoute = "/Shopping List";
  // static const String shoppingTwoRoute = "/Shopping Details";
  // static const String shoppingThreeRoute = "/Product Details";
  // static const String paymentOneRoute = "/Credit Card";
  // static const String paymentTwoRoute = "/Payment Success";
  // static const String loginOneRoute = "/Login With OTP";
  // static const String loginTwoRoute = "/Login 2";
  // static const String dashboardOneRoute = "/Dashboard 1";
  // static const String dashboardTwoRoute = "/Dashboard 2";

  //strings
  static const String appName = "UTB Mobile Banking";

  static const String settings = 'Settings';
  static const String logout = 'Logout';

  static const List<String> choices = <String>[settings, logout];

  //fonts
  static const String avenirFont = "AvenirNext";
  static const String museoFont = "MuseoSans";
  static const String ccFont = "CreditCard";

  //images
  static const String imageDir = "assets/images";
  static const String logoImage = "$imageDir/utb_logo.png";
  static const String logoImageLive = "$imageDir/logo.png";
  static const String atmCard = "$imageDir/atm_card.jpg";
  static const String logoInverted = "$imageDir/utb_logo_3.png";
  static const String emptyImage = "$imageDir/empty.png";
  static const String mapImage = "$imageDir/map.png";
  static const String usdImage = "$imageDir/usd.png";
  static const String gbpImage = "$imageDir/gbp.png";
  static const String eurImage = "$imageDir/euro.png";

  //login
  // static const String enter_code_label = "Phone Number";
  // static const String enter_code_hint = "10 Digit Phone Number";
  // static const String enter_otp_label = "OTP";
  // static const String enter_otp_hint = "4 Digit OTP";
  // static const String get_otp = "Get OTP";
  // static const String resend_otp = "Resend OTP";
  // static const String login = "Login";
  // static const String enter_valid_number = "Enter 10 digit phone number";
  // static const String enter_valid_otp = "Enter 4 digit otp";

  //gneric
  static const String error = "Error";
  static const String success = "Success";
  static const String ok = "OK";
  static const String forgot_password = "Forgot Password?";
  static const String something_went_wrong = "Something went wrong";
  static const String coming_soon = "Coming Soon";

  static const MaterialColor ui_kit_color = Colors.grey;

//colors
  // static const appPrimaryColor = Color(0xff0047dc);
  // static const appPrimaryColor = Color(0xff2323eb);
  static const appPrimaryColor = Color(0xff0000e0);

  // static const appPrimaryColorDark = Color(0xff0045d6);
  static const appPrimaryColorDark = Color(0xff2222bd);

  static List<Color> kitGradients = [
    // new Color.fromRGBO(103, 218, 255, 1.0),
    Color.fromRGBO(3, 169, 244, 1.0),
    Color.fromRGBO(0, 122, 193, 1.0),
    Colors.blueGrey.shade800,
    Colors.black87,
  ];

  static List<Color> kitGradientsInactive = [
    // Colors.blueGrey.shade400,
    Colors.grey[500],
    Colors.grey[700],
  ];

  static List<Color> kitGradients2 = [
    Colors.cyan.shade600,
    Colors.blue.shade900
  ];

  //randomcolor
  static final Random _random = new Random();

  /// Returns a random color.
  static Color next() {
    return new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }
}
