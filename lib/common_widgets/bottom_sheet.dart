import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomBottomSheet extends StatefulWidget {
  final int index;
  final BottomSheetItem item;

  const CustomBottomSheet({Key key, this.item, this.index}) : super(key: key);
  @override
  _CustomBottomSheetState createState() => _CustomBottomSheetState();
}

class _CustomBottomSheetState extends State<CustomBottomSheet> {
  int selected;

  @override
  Widget build(BuildContext context) {
    double _screenHeight = MediaQuery.of(context).size.height;
    double _screenWidth = MediaQuery.of(context).size.width;
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          setState(() {
            selected = widget.index;
          });
          Future.delayed(Duration(milliseconds: 200), () {
            Navigator.pop(context);
            if (widget.item.widget != null) {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => widget.item.widget),
              );
            } else {
              widget.item.handler();
            }
          });
        },
        child: Container(
          height: _screenHeight * 0.105,
          width: _screenWidth * 0.88,
          padding: EdgeInsets.only(
            left: _screenWidth * 0.05,
            right: _screenWidth * 0.01,
            top: _screenWidth * 0.01,
          ),
          margin: EdgeInsets.only(bottom: _screenHeight * 0.025),
          decoration: BoxDecoration(
            border: Border.all(
              width: 1.2,
              color: CupertinoColors.activeGreen,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          child: Row(
            children: [
              selected != widget.index
                  ? Container(
                      width: _screenWidth * 0.04,
                      height: _screenHeight * 0.075,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 1.2,
                            color: CupertinoColors.activeGreen,
                          )),
                    )
                  : Icon(
                      CupertinoIcons.checkmark_circle_fill,
                      color: CupertinoColors.activeGreen,
                    ),
              SizedBox(width: _screenWidth * 0.04),
              widget.item.icon != null
                  ? Icon(
                      widget.item.icon,
                      color: CupertinoColors.activeGreen,
                      size: _screenHeight * 0.05,
                    )
                  : Container(
                      height: _screenHeight * 0.05,
                      width: _screenHeight * 0.05,
                      child: widget.item.iconWidget,
                    ),
              SizedBox(width: _screenWidth * 0.038),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.item.title,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey[700],
                    ),
                  ),
                  SizedBox(height: _screenHeight * 0.001),
                  Text(
                    ' ${widget.item.subtitle}',
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.start,
                    style: TextStyle(fontSize: 14.0, color: Colors.grey[400]),
                  )
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Icon(
                        CupertinoIcons.info_circle,
                        color: Colors.grey[400],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class BottomSheetItem {
  final String title;
  final String subtitle;
  final handler;
  final Widget widget;
  final IconData icon;
  final Widget iconWidget;

  BottomSheetItem({
    this.title,
    this.subtitle,
    this.handler,
    this.icon,
    this.widget,
    this.iconWidget,
  });
}
