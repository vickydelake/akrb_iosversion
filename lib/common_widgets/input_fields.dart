import 'package:akrbankios/common_widgets/search_bar.dart';
import 'package:akrbankios/models/listModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CustomDropdown extends StatefulWidget {
  final String value;
  final String title;
  final List<dynamic> data;
  final Function(dynamic value) onChanged;
  final String description;
  final bool fillSpace;
  final Color color;
  final bool useBorder;

  const CustomDropdown({
    Key key,
    this.value,
    this.title,
    this.onChanged,
    this.description,
    this.fillSpace,
    this.color,
    this.useBorder,
    this.data,
  }) : super(key: key);
  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  TextEditingController searchController;
  String searchTerm;
  double _screenHeight;
  double _screenWidth;
  var selectedItem;
  List<ListModel> filteredItems = [];

  List<ListModel> getListModelFromQ(List<dynamic> items) {
    List<ListModel> list = items
        .map(
          (item) => ListModel(
            description: item.description,
            val: item.code,
          ),
        )
        .toList();

    return list;
  }

  @override
  void initState() {
    filteredItems = getListModelFromQ(widget.data);
    print(filteredItems);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _screenHeight = MediaQuery.of(context).size.height;
    _screenWidth = MediaQuery.of(context).size.width;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.description != null
            ? Text(
                " ${widget.description} : ",
              )
            : SizedBox(),
        SizedBox(height: _screenHeight * 0.005),
        Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              showDialog(
                context: context,
                builder: (context) {
                  return StatefulBuilder(
                    builder: (context, setState) {
                      return SimpleDialog(
                        title: Text(
                          widget.description != null
                              ? widget.description
                              : 'Select One',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                        titlePadding: EdgeInsets.symmetric(vertical: 20.0),
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 10.0,
                          horizontal: 15.0,
                        ),
                        children: [
                          SizedBox(height: _screenHeight * 0.01),
                          SearchBar(
                            width: _screenWidth * 0.88,
                            color: Colors.grey[100],
                            searchController: searchController,
                            callback: (value) {
                              setState(
                                () {
                                  searchTerm = value;
                                  filteredItems = getListModelFromQ(widget.data)
                                      .where(
                                        (item) => item.description
                                            .toLowerCase()
                                            .contains(
                                              value.toLowerCase(),
                                            ),
                                      )
                                      .toList();
                                },
                              );
                            },
                          ),
                          SizedBox(height: _screenHeight * 0.01),
                          Divider(
                            color: Colors.grey[300],
                            thickness: 1.0,
                            height: 5.0,
                          ),
                          SizedBox(height: _screenHeight * 0.015),
                          Column(
                            children: filteredItems
                                .map<Widget>(
                                  (item) => Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        setState(
                                          () {
                                            selectedItem = item;
                                            widget.onChanged(item.val);
                                          },
                                        );
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 15.0, horizontal: 8.0),
                                        margin: EdgeInsets.only(bottom: 10.0),
                                        decoration: BoxDecoration(
                                          color: selectedItem == item
                                              ? Colors.grey[400]
                                              : Colors.grey[200],
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              item.description,
                                              style: TextStyle(
                                                color: Colors.grey[700],
                                                fontSize: 15.0,
                                                letterSpacing: 0.4,
                                              ),
                                            ),
                                            // selectedItem == item
                                            //     ? Icon(
                                            //         CupertinoIcons.check_mark,
                                            //         color: Colors.green[300],
                                            //         size: 26.0,
                                            //       )
                                            //     : Text(''),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                          SizedBox(height: _screenHeight * 0.03),
                        ],
                      );
                    },
                  );
                },
              );
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
              height: _screenHeight * 0.072,
              width: widget.fillSpace ? _screenWidth : double.infinity,
              decoration: BoxDecoration(
                color: widget.color != null ? widget.color : Colors.transparent,
                border: widget.useBorder
                    ? Border.all(width: 1.5, color: Colors.grey[400])
                    : Border.fromBorderSide(BorderSide.none),
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    selectedItem == null
                        ? widget.description != null
                            ? 'Select ${widget.description}'
                            : 'Select One'
                        : selectedItem.description,
                    style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
                  ),
                  Icon(
                    Icons.arrow_drop_down,
                    color: Colors.grey[600],
                    size: 28.0,
                  ),
                  // SizedBox(width: _screenWidth * 0.1),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class DatePicker extends StatefulWidget {
  final String hint;
  final DateTime date;
  final bool filled;
  final Color color;
  final Function(DateTime) data;

  const DatePicker({this.hint, this.data, this.filled, this.date, this.color});

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime _date = DateTime.now();
  bool _isEmpty = true;
  String text = "Select Date";
  String selectedDate;
  String today = DateFormat("yyyy-M-dd").format(DateTime.now());
  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1930),
      lastDate: DateTime(2100),
    );
    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
        newdateFormat(_date);
        widget.data(_date);
        _isEmpty = false;
        text = null;
        print(_date.toString());
      });
    }
  }

  newdateFormat(DateTime date) {
    setState(() {
      selectedDate = DateFormat("yyyy-M-dd").format(date);
    });
    // return DateFormat("yyyy-M-dd").format(date);
    // return DateFormat.yMMMd().format(_date);
  }

  @override
  void initState() {
    if (widget.date != null) {
      newdateFormat(widget.date);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _screenHeight = MediaQuery.of(context).size.height;
    final _screenWidth = MediaQuery.of(context).size.width;
    // FocusNode myFocusNode = new FocusNode();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Text(
        //   " Date of Birth : ",
        //   style: TextStyle(
        //     color: Colors.grey[700],
        //     fontSize: 17.0,
        //   ),
        // ),
        // SizedBox(height: _screenHeight * 0.01),
        Container(
          height: MediaQuery.of(context).size.height * 0.0665,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.0),
            color: widget.filled && widget.color != null
                ? widget.color
                : Colors.transparent,
          ),
          padding: EdgeInsets.only(
            left: 1,
            right: 1,
            top: 4,
          ),
          child: InkWell(
            onTap: () => selectDate(context),
            child: Container(
              decoration: BoxDecoration(
                color: widget.filled ? widget.color : Colors.transparent,
                borderRadius: BorderRadius.circular(6.0),
              ),
              width: double.infinity,
              height: double.infinity,
              child: InputDecorator(
                  isEmpty: !_isEmpty,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 13.0, vertical: 4.5),
                    // labelText: !widget.filled ? widget.hint : '',
                    labelStyle: TextStyle(
                      fontSize: 15,
                      color: Colors.grey[700],
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      borderSide: widget.filled
                          ? BorderSide.none
                          : BorderSide(width: 1.0, color: Colors.grey[400]),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      borderSide: widget.filled
                          ? BorderSide.none
                          : BorderSide(width: 1.0, color: Colors.grey[400]),
                    ),
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 0.0),
                        child: Text(selectedDate ?? text,
                            style: TextStyle(
                                fontSize: 15.0, color: Colors.grey[700])),
                      ),
                      Icon(Icons.arrow_drop_down,
                          size: 26.0,
                          color:
                              Theme.of(context).brightness == Brightness.light
                                  ? Colors.grey.shade700
                                  : Colors.white70),
                    ],
                  )),
            ),
          ),
        ),
      ],
    );
  }
}
