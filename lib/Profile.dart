import 'package:akrbankios/models/userModel.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  final User user;
  final Function onSettingsPressed;
  final Color color;
  // Settings(this.color, this.onSettingsPressed);
  const Profile({Key key, this.onSettingsPressed, this.color, this.user})
      : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.color,
    );
  }
}
