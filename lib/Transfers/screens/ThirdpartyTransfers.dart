import 'package:akrbankios/common_widgets/account_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:akrbankios/common_widgets/funds_transfer_card.dart';

class ThirdPartyTransfer extends StatefulWidget {
  @override
  _ThirdPartyTransferState createState() => _ThirdPartyTransferState();
}

class _ThirdPartyTransferState extends State<ThirdPartyTransfer> {
  double _screenHeight;
  double _screenWidth;
  var ownAcctTransfer = new GlobalKey<ScaffoldState>();
  TextEditingController amountController = new TextEditingController();
  TextEditingController narrationController = new TextEditingController();
  final PageController _pageController = PageController(initialPage: 0);
  int currentPage = 0;

  List<AccountInfo> accounts = [
    AccountInfo(
        'King Sally', '595.00', '30** *** *** **32', 'Savings Account', ''),
    AccountInfo('Lans Kay Brooks', '238.00', '30** *** *** **21',
        'Current Account', ''),
    AccountInfo('Blaise Collenwood', '4765.00', '30** *** *** **43',
        'Investment Account', '')
  ];

  String debitAccNum = '';
  String creditAccNum = '';
  String debitAccName = '';
  String creditAccName = '';
  String aliasName = '';
  String debitAccType = '';
  String creditAccType = '';
  String debitAvailableBalance = '';
  String creditAvailableBalance = '';
  String debitAccCurrency = '';
  String creditAccCurrency = '';
  String secPin;
  String amount;
  String narration;

  selectDebitAcct(AccountInfo accountInfo) {
    setState(() {
      debitAccNum = accountInfo.acc_number;
      debitAccCurrency = accountInfo.acc_currency;
      debitAccName = accountInfo.acc_name;
      debitAccType = accountInfo.acc_type;
      debitAvailableBalance = accountInfo.acc_bal;
    });
  }

  selectCrebitAcct(AccountInfo accountInfo) {
    setState(() {
      creditAccNum = accountInfo.acc_number;
      creditAccCurrency = accountInfo.acc_currency;
      creditAccName = accountInfo.acc_name;
      creditAccType = accountInfo.acc_type;
      creditAvailableBalance = accountInfo.acc_bal;
    });
  }

  void _inform(String message) {
    ownAcctTransfer.currentState.showSnackBar(SnackBar(
      // width: MediaQuery.of(context).size.width * 0.88,
      padding: EdgeInsets.all(6.0),
      duration: Duration(milliseconds: 2000),
      backgroundColor: Colors.black.withOpacity(0.85),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(1.0),
        ),
      ),
      content: Container(
        margin: EdgeInsets.only(bottom: 25.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.cancel,
              size: 28.0,
              color: Colors.red[200],
            ),
            SizedBox(width: 50),
            Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 17.0,
                letterSpacing: 0.7,
                fontWeight: FontWeight.w400,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _screenHeight = MediaQuery.of(context).size.height;
    _screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        key: ownAcctTransfer,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Color(0XFF339933),
          title: Text("Own Account Transfer",
              style: TextStyle(color: Colors.white, fontSize: 16.0)),
        ),
        body: SingleChildScrollView(
          child: Container(
            height: _screenHeight,
            width: _screenWidth,
            padding: EdgeInsets.symmetric(
              horizontal: _screenWidth * 0.04,
              vertical: _screenHeight * 0.01,
            ),
            child: PageView(
              physics: ClampingScrollPhysics(),
              controller: _pageController,
              onPageChanged: (page) {
                setState(() {
                  currentPage = page;
                });
              },
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.only(top: 16.0, bottom: 5.0),
                          child: Text(
                            'SELECT SOURCE ACCOUNT',
                            style: TextStyle(
                              fontSize: 15.0,
                              letterSpacing: 0.4,
                              // fontWeight: FontWeight.w600,
                              color: Color(0XFF339933),
                            ),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: () => _showBottomSheet(
                          context, 'Select Debit Account', 'd'),
                      child: FundsTransferAccountCard(
                        accName: debitAccName,
                        accNum: debitAccNum,
                        accType: debitAccType,
                        availBalance: debitAvailableBalance,
                        currency: debitAccCurrency,
                      ),
                    ),
                    SizedBox(height: _screenHeight * 0.004),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.only(top: 16.0, bottom: 5.0),
                          child: Text(
                            'SELECT AMOUNT',
                            style: TextStyle(
                              fontSize: 15.0,
                              letterSpacing: 0.4,
                              // fontWeight: FontWeight.w600,
                              color: Color(0XFF339933),
                            ),
                          ),
                        ),
                      ],
                    ),
                    textField(amountController, 'Enter amount', 'n',
                        Icons.monetization_on),
                    SizedBox(height: _screenHeight * 0.004),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.only(top: 16.0, bottom: 5.0),
                          child: Text(
                            'SELECT DESTINATION ACCOUNT',
                            style: TextStyle(
                              fontSize: 15.0,
                              letterSpacing: 0.4,
                              // fontWeight: FontWeight.w600,
                              color: Color(0XFF339933),
                            ),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: () => _showBottomSheet(
                          context, 'Select Debit Account', 'c'),
                      child: FundsTransferAccountCard(
                        accName: creditAccName,
                        accNum: creditAccNum,
                        accType: creditAccType,
                        availBalance: creditAvailableBalance,
                        currency: creditAccCurrency,
                      ),
                    ),
                    SizedBox(height: _screenHeight * 0.004),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.only(top: 16.0, bottom: 5.0),
                          child: Text(
                            'ENTER NARRATION',
                            style: TextStyle(
                              fontSize: 15.0,
                              letterSpacing: 0.4,
                              // fontWeight: FontWeight.w600,
                              color: Color(0XFF339933),
                            ),
                          ),
                        ),
                      ],
                    ),
                    textField(
                      narrationController,
                      'Enter narration',
                      't',
                      Icons.receipt,
                    ),
                    SizedBox(height: _screenHeight * 0.05),
                    Container(
                      color: Color(0XFF339933),
                      width: _screenWidth * 0.5,
                      child: FlatButton(
                        child: Text('Proceed',
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            )),
                        onPressed: () {
                          if (debitAccName != '' &&
                              creditAccName != '' &&
                              amountController.text.isNotEmpty &&
                              narrationController.text.isNotEmpty) {
                            print('jumping');
                            _pageController.jumpToPage(currentPage + 1);
                          } else {
                            print('fill fields');
                            _inform('Please fill all the fields');
                          }
                        },
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                      child: Text('New Page'),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget textField(controller, description, type, icon) {
    return Column(
      children: [
        Container(
          width: _screenWidth * 0.9,
          padding:
              EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: TextField(
            keyboardType: type == 'n'
                ? TextInputType.number
                : type == 'e'
                    ? TextInputType.emailAddress
                    : TextInputType.text,
            controller: controller,
            onChanged: (value) {
              if (value != '') {
                print('');
              } else {
                print('');
              }
            },
            style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: description,
              hintStyle: TextStyle(color: Colors.grey[400]),
              // errorText: '',
              suffixIcon: Icon(icon),
            ),
          ),
        ),
      ],
    );
  }

  void _showBottomSheet(BuildContext context, String description, String type) {
    double _screenHeight = MediaQuery.of(context).size.height;
    double _screenWidth = MediaQuery.of(context).size.width;

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return Container(
                height: _screenHeight * 0.4,
                padding: EdgeInsets.symmetric(
                  horizontal: _screenWidth * 0.02,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  ),
                ),
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: _screenWidth * 0.08,
                    vertical: _screenHeight * 0.006,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: _screenHeight * 0.004),
                      Container(
                        width: _screenWidth * 0.16,
                        height: _screenHeight * 0.005,
                        color: Colors.grey[500],
                      ),
                      SizedBox(height: _screenHeight * 0.022),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          description,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.grey[700]),
                        ),
                      ),
                      SizedBox(height: _screenHeight * 0.006),
                      Text(
                        'Swipe to preferred account and tap on it to select',
                        style:
                            TextStyle(fontSize: 14.5, color: Colors.grey[500]),
                      ),
                      SizedBox(height: _screenHeight * 0.03),
                      Container(
                        height: _screenHeight * 0.2,
                        width: _screenWidth * 0.95,
                        child: new Swiper(
                          itemWidth: 350.0,
                          layout: SwiperLayout.STACK,
                          itemBuilder: (BuildContext context, int index) {
                            return AccountCard(
                              accountInfo: accounts[index],
                              onActivated: (state) {
                                if (state) {
                                  type == 'd'
                                      ? selectDebitAcct(accounts[index])
                                      : selectCrebitAcct(accounts[index]);
                                }
                              },
                            );
                          },
                          itemCount: 3,
                          // pagination: new SwiperPagination(),
                          // control: new SwiperControl(),
                        ),
                      ),
                    ],
                  ),
                ));
          },
        );
      },
    );
  }
}
