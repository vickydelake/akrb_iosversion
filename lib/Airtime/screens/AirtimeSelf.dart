import 'package:flutter/material.dart';

class AirtimeSelf extends StatefulWidget {
  @override
  _AirtimeSelfState createState() => _AirtimeSelfState();
}

class _AirtimeSelfState extends State<AirtimeSelf> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Color(0XFF339933),
      title: Text("Purchase Airtime (Self) ",
          style: TextStyle(color: Colors.white, fontSize: 15)),
    ));
  }
}
