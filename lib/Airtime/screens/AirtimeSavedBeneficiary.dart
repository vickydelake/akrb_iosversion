import 'package:flutter/material.dart';

class AirtimeSavedBeneficiary extends StatefulWidget {
  @override
  _AirtimeSavedBeneficiaryState createState() =>
      _AirtimeSavedBeneficiaryState();
}

class _AirtimeSavedBeneficiaryState extends State<AirtimeSavedBeneficiary> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Color(0XFF339933),
      title: Text("Purchase Airtime ( Saved Beneficiary ) ",
          style: TextStyle(color: Colors.white, fontSize: 15)),
    ));
  }
}
