import 'package:flutter/material.dart';

class AirtimeNewBeneficiary extends StatefulWidget {
  @override
  _AirtimeNewBeneficiaryState createState() => _AirtimeNewBeneficiaryState();
}

class _AirtimeNewBeneficiaryState extends State<AirtimeNewBeneficiary> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Color(0XFF339933),
      title: Text("Purchase Airtime (Beneficiary Number) ",
          style: TextStyle(color: Colors.white, fontSize: 15)),
    ));
  }
}
