import 'package:flutter/material.dart';

class Airtime extends StatefulWidget {
  @override
  _AirtimeState createState() => _AirtimeState();
}

class _AirtimeState extends State<Airtime> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0XFF339933), title: Text("Purchase Airtime")),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
              top: 20.0,
              left: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.person,
                  color: Color(0XFF339933),
                  size: 40,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    "Self",
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                ),
                Icon(
                  Icons.arrow_right,
                  size: 30,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 2,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 10.0,
              left: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.person_add,
                  color: Color(0XFF339933),
                  size: 40,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    "One-Time Beneficiary",
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                ),
                Icon(
                  Icons.arrow_right,
                  size: 30,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 2,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 10.0,
              left: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.group,
                  color: Color(0XFF339933),
                  size: 40,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    "Saved Beneficiary",
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                ),
                Icon(
                  Icons.arrow_right,
                  size: 30,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 2,
          ),
        ],
      ),
    );
  }
}
