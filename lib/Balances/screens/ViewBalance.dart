import 'package:akrbankios/common_widgets/account_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../widgets/chart.dart';
import 'package:akrbankios/models/transEnq.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

// import 'package:fl_chart/fl_chart.dart';
const kExpandedHeight = 240.0;

class ViewBalance extends StatefulWidget {
  final image;

  const ViewBalance({Key key, this.image}) : super(key: key);
  @override
  _ViewBalanceState createState() => _ViewBalanceState();
}

class _ViewBalanceState extends State<ViewBalance> {
  ScrollController _scrollController;
  bool _showTitle = false;
  double _screenHeight;
  double _screenWidth;
  final balances = [
    12.17,
    11.54,
    15.05,
    10.34,
    08.68,
    11.91,
    13.32,
  ];

  List<AccountInfo> accounts = [
    AccountInfo(
        'King Sally', '595.00', '30** *** *** **32', 'Savings Account', ''),
    AccountInfo('Lans Kay Brooks', '238.00', '30** *** *** **21',
        'Current Account', ''),
    AccountInfo('Blaise Collenwood', '4765.00', '30** *** *** **43',
        'Investment Account', '')
  ];

  Future<TransEnq> transactions;
  // final overlap = 15.0;

  @override
  void initState() {
    super.initState();
    transactions = Future.delayed(Duration(seconds: 10), () {
      return TransEnq(
        mbResponse: 'MBank Response',
        mbTransactions: [],
        mbMessage: 'MB Message',
      );
    });

    _scrollController = ScrollController()
      ..addListener(() {
        final isNotExpanded = _scrollController.hasClients &&
            _scrollController.offset > kExpandedHeight - kToolbarHeight;
        if (isNotExpanded != _showTitle) {
          setState(() {
            _showTitle = isNotExpanded;
          });
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    _screenHeight = MediaQuery.of(context).size.height;
    _screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverAppBar(
              pinned: true,
              // snap: true,
              floating: false,
              title: _showTitle
                  ? Text(
                      'Balance',
                    )
                  : Text(
                      'My Balances',
                      style: TextStyle(
                          fontSize: 22.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),
              expandedHeight: kExpandedHeight,
              flexibleSpace: _showTitle
                  ? null
                  : FlexibleSpaceBar(
                      // background: Container(
                      //   color: Colors.grey,
                      // ),
                      background: Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          buildBottomWidget(),
                          buildMiddleContainer(),
                          buildTopContainer(context),
                        ],
                      ),
                    ),
            ),
            cardSliver(),
            chartSliver(),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: _screenWidth * 0.08,
                  vertical: _screenHeight * 0.015,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Transactions',
                          style: TextStyle(
                              fontSize: 15.0, fontWeight: FontWeight.w400),
                        ),
                        Row(
                          children: [
                            Text('Monthly'),
                            SizedBox(width: 7.0),
                            Icon(Icons.arrow_drop_down)
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: _screenHeight * 0.016),
                  ],
                ),
              ),
            ),
            FutureBuilder<dynamic>(
              future: transactions,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return SliverFillRemaining(
                    child: Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                  );
                }

                if (snapshot.hasError) {
                  final error = snapshot.error as PlatformException;
                  String errorMesage = error.code == '500' ||
                          error.code == '502' ||
                          error.code == '404' ||
                          error == null
                      ? 'Could not fetch recent transactions'
                      : error.message;
                  return SliverFillRemaining(
                    child: Center(
                      child: Text(
                        errorMesage,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  );
                }

                if (snapshot.data.mbTransactions.isEmpty) {
                  return SliverFillRemaining(
                    child: Center(
                      child: Text(
                        'No recent transactions',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  );
                }
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return Container(
                        // height: 50,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(32),
                        color: Colors.orange[100 * (index % 9)],
                        child: Text('orange $index'),
                      );
                    },
                    childCount: 100,
                  ),
                );
                // return AnimationLimiter(
                //   child: SliverList(
                //     delegate: SliverChildBuilderDelegate(
                //       (context, index) {
                //         List<dynamic> mbTransaction =
                //             snapshot.data.mbTransactions;

                //         print('sliverlist length: ' +
                //             snapshot.data.mbTransactions.length.toString());
                //         return AnimationConfiguration.staggeredList(
                //           position: index,
                //           duration: const Duration(milliseconds: 500),
                //           child: FadeInAnimation(
                //             child: TransEnqListTile(
                //               transaction: mbTransaction[index],
                //               color: double.parse(mbTransaction[index].amount) > 0
                //                   ? Color(0xff07B226)
                //                   : Color(0xffE41F0D),
                //             ),
                //           ),
                //         );
                //       },
                //       childCount: snapshot.data.mbTransactions.length,
                //     ),
                //   ),
                // );
              },
            )
          ],
        ),
      ),
    );
  }

  Widget cardSliver() {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: _screenWidth * 0.08,
          vertical: _screenHeight * 0.015,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(65.0),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Accounts',
              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
            ),
            SizedBox(height: _screenHeight * 0.013),
            Container(
              height: _screenHeight * 0.2,
              width: _screenWidth * 0.95,
              child: new Swiper(
                itemWidth: 300.0,
                layout: SwiperLayout.STACK,
                itemBuilder: (BuildContext context, int index) {
                  return AccountCard(accountInfo: accounts[index]);
                },
                itemCount: 3,
                // pagination: new SwiperPagination(),
                // control: new SwiperControl(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget chartSliver() {
    return SliverToBoxAdapter(
      child: Container(
        height: _screenHeight * 0.31,
        width: _screenWidth * 0.8,
        padding: EdgeInsets.symmetric(
          horizontal: _screenWidth * 0.08,
          vertical: _screenHeight * 0.015,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(65.0),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Balance History',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                ),
                Row(
                  children: [
                    Text('Monthly'),
                    SizedBox(width: 7.0),
                    Icon(Icons.arrow_drop_down)
                  ],
                )
              ],
            ),
            SizedBox(height: _screenHeight * 0.016),
            BalanceChart(),
          ],
        ),
      ),
    );
  }

  Align buildTopContainer(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        // color: Colors.white,
        padding: EdgeInsets.symmetric(
          horizontal: _screenWidth * 0.02,
          vertical: _screenHeight * 0.005,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '\$20 646',
                  style: TextStyle(
                    fontSize: 42.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                    // .withOpacity(appBarTitleOpacity(shrink)),
                  ),
                ),
                SizedBox(width: _screenWidth * 0.035),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                  child: Text(
                    '- 570',
                    style: TextStyle(
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(14.0)),
                    color: Colors.grey[200].withOpacity(0.4),
                  ),
                ),
              ],
            ),
            SizedBox(height: _screenHeight * 0.0015),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  " Tue, Mar 26, 2020",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey[300].withOpacity(0.6),
                    fontSize: 15.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            SizedBox(height: _screenHeight * 0.035),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          width: _screenWidth * 0.025,
                          height: _screenHeight * 0.0124,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border:
                                Border.all(width: 1.5, color: Colors.white70),
                          ),
                        ),
                        Text(
                          '  INCOME',
                          style: TextStyle(
                            color: Colors.grey[300].withOpacity(0.75),
                            fontSize: 16.5,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: _screenHeight * 0.005),
                    Text('  - \$5 400',
                        style: TextStyle(
                          color: Colors.grey[300].withOpacity(0.8),
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ))
                  ],
                ),
                // SizedBox(width: _screenWidth * 0.07),
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          width: _screenWidth * 0.025,
                          height: _screenHeight * 0.0124,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border:
                                Border.all(width: 1.5, color: Colors.white70),
                          ),
                        ),
                        Text(
                          '  EXPENSES',
                          style: TextStyle(
                            color: Colors.grey[300].withOpacity(0.75),
                            fontSize: 16.5,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: _screenHeight * 0.005),
                    Text('  - \$5 400',
                        style: TextStyle(
                          color: Colors.grey[300].withOpacity(0.8),
                          fontSize: 16.5,
                          fontWeight: FontWeight.w500,
                        ))
                  ],
                ),
                // SizedBox(width: _screenWidth * 0.17),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Align buildMiddleContainer() {
    return Align(
      alignment: Alignment.topCenter,
      child: Opacity(
        opacity: 0.4,
        child: Container(
          // height: double.infinity / 2,
          // width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.grey[800],
                Colors.transparent,
              ],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              stops: [
                0.3,
                1.0,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildBottomWidget() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background2.jpg'),
          fit: BoxFit.fill,
          // colorFilter: ColorFilter.mode(Colors.blue, BlendMode.difference),
        ),
        color: Color(0XFF339933).withOpacity(0.8),
      ),
      child: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Color(0XFF339933).withOpacity(0.8),
        ),
        child: Column(),
      ),
    );
  }
}
