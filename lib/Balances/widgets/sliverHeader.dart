import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/models/userModel.dart';
import 'package:akrbankios/common_widgets/uidata.dart';

class AcctBalanceHeader implements SliverPersistentHeaderDelegate {
  AcctBalanceHeader({
    this.minExtent,
    @required this.maxExtent,
  });
  final double minExtent;
  final double maxExtent;

  @override
  double get minHeight => minExtent;
  @override
  double get maxHeight => math.max(minExtent, maxExtent);
  double _height;
  double _width;
  double shrink;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    shrink = shrinkOffset;
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        buildBottomWidget(),
        buildMiddleContainer(),
        buildTopContainer(context),
      ],
    );
  }

  bool buildMinHeightWidget(double shrinkOffset) {
    return shrinkOffset > 0;
  }

  double appBarTitleOpacity(double shrinkOffset) {
    return math.max(0.0, shrinkOffset) / maxExtent;
  }

  // double widgetOpacity(double shrinkOffset) {
  //   // simple formula: fade out text as soon as shrinkOffset > 0
  //   return 1.0 - math.max(0.0, shrinkOffset) / maxExtent;
  //   // more complex formula: starts fading out text when shrinkOffset > minExtent
  //   //return 1.0 - max(0.0, (shrinkOffset - minExtent)) / (maxExtent - minExtent);
  // }

  Align buildTopContainer(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        // color: Colors.white,
        padding: EdgeInsets.symmetric(
          horizontal: _width * 0.02,
          vertical: _height * 0.01,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  '\$20 646',
                  style: TextStyle(
                    fontSize: 42.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                    // .withOpacity(appBarTitleOpacity(shrink)),
                  ),
                ),
                SizedBox(width: _width * 0.035),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                  child: Text(
                    '- 570',
                    style: TextStyle(
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(14.0)),
                    color: Colors.grey[200].withOpacity(0.4),
                  ),
                ),
              ],
            ),
            SizedBox(height: _height * 0.005),
            Text(
              " Tue, Mar 26, 2020",
              style: TextStyle(
                color: Colors.grey[300].withOpacity(0.6),
                fontSize: 16.5,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: _height * 0.035),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          width: _width * 0.025,
                          height: _height * 0.0124,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border:
                                Border.all(width: 1.5, color: Colors.white70),
                          ),
                        ),
                        Text(
                          '  INCOME',
                          style: TextStyle(
                            color: Colors.grey[300].withOpacity(0.75),
                            fontSize: 16.5,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: _height * 0.005),
                    Text('  - \$5 400',
                        style: TextStyle(
                          color: Colors.grey[300].withOpacity(0.8),
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ))
                  ],
                ),
                SizedBox(width: _width * 0.07),
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          width: _width * 0.025,
                          height: _height * 0.0124,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border:
                                Border.all(width: 1.5, color: Colors.white70),
                          ),
                        ),
                        Text(
                          '  EXPENSES',
                          style: TextStyle(
                            color: Colors.grey[300].withOpacity(0.75),
                            fontSize: 16.5,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: _height * 0.005),
                    Text('  - \$5 400',
                        style: TextStyle(
                          color: Colors.grey[300].withOpacity(0.8),
                          fontSize: 16.5,
                          fontWeight: FontWeight.w500,
                        ))
                  ],
                ),
                SizedBox(width: _width * 0.17),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Align buildMiddleContainer() {
    return Align(
      alignment: Alignment.topCenter,
      child: Opacity(
        opacity: 0.4,
        child: Container(
          // height: double.infinity / 2,
          // width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.grey[800],
                Colors.transparent,
              ],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              stops: [
                0.3,
                1.0,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildBottomWidget() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background2.jpg'),
          fit: BoxFit.fill,
          // colorFilter: ColorFilter.mode(Colors.blue, BlendMode.difference),
        ),
        color: Color(0XFF339933).withOpacity(0.8),
      ),
      child: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Color(0XFF339933).withOpacity(0.8),
        ),
        child: Column(),
      ),
    );
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // return true;
    return maxHeight != oldDelegate.maxExtent ||
        minHeight != oldDelegate.minExtent;
  }

  @override
  FloatingHeaderSnapConfiguration get snapConfiguration => null;

  @override
  OverScrollHeaderStretchConfiguration get stretchConfiguration => null;

  @override
  // TODO: implement showOnScreenConfiguration
  PersistentHeaderShowOnScreenConfiguration get showOnScreenConfiguration =>
      PersistentHeaderShowOnScreenConfiguration();

  @override
  // TODO: implement vsync
  TickerProvider get vsync => throw UnimplementedError();
}
