import 'package:akrbankios/models/userModel.dart';
import 'package:flutter/material.dart';

class UserInfo with ChangeNotifier {
  String _currency;
  User _accountInfo;
  User get getInfo => _accountInfo;
  String get getCurrency => _currency;
  setInfo(User info) {
    _accountInfo = info;
    notifyListeners();
  }

  setCurrency(String info) {
    _currency = info;
    notifyListeners();
  }

/*
  List<MbTransDetail> _accData;
  List<MbTransDetail> get getAccData => _accData;
  setAccData(List<MbTransDetail> info) {
    _accData = info;
    notifyListeners();
  }
*/
  notifyListeners();
}
