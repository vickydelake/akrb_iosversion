import 'package:flutter/material.dart';

class Default with ChangeNotifier {
  bool _isloading = false;
  bool _error = false;
  String _pin = "";
  String _currency = "SLL";
  BuildContext _context;
  BuildContext get getContext => _context;
  bool get getIsLoading => _isloading;
  bool get geterror => _error;
  String get getPin => _pin;
  String get getCurrency => _currency;
  setLoading(bool load) {
    _isloading = load;
    print('== Loading..');
    notifyListeners();
  }

  setPin(String load) {
    _pin = load;
    notifyListeners();
  }

  setCurrency(String load) {
    _currency = load;
    notifyListeners();
  }

  seterror(bool load) {
    _error = load;
    // print("time out set at provider" + _error.toString());
    notifyListeners();
  }

  setContext(BuildContext context) {
    _context = context;
    notifyListeners();
  }
}
