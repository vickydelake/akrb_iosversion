import 'package:flutter/material.dart';

class PhoneInfo with ChangeNotifier {
  String _deviceId;
  String _deviceIp;
  String _model;
  String _manufacturer;
  String _brand;
  String _country;
  String _deviceOs;
  String _fbcmToken = "kaskjdhajsdhajsd";
  String _appVersion = "1.0";
  String get getdeviceId => _deviceId;
  String get getdeviceIp => _deviceIp;
  String get getmodel => _model;
  String get getmamanufacturer => _manufacturer;
  String get getbrand => _brand;
  String get getcountry => _country;
  String get getdeviceOs => _deviceOs;
  String get getfbcmToken => _fbcmToken;
  String get getappVersion => _appVersion;
  setdeviceID(String id) {
    _deviceId = id;
    notifyListeners();
  }

  setdeviceIp(String ip) {
    _deviceIp = ip;
    notifyListeners();
  }

  setmodel(String id) {
    _model = id;
    notifyListeners();
  }

  setmanufacturer(String id) {
    _manufacturer = id;
    notifyListeners();
  }

  setbrand(String id) {
    _brand = id;
    notifyListeners();
  }

  setcountry(String id) {
    _country = id;
    notifyListeners();
  }

  setdeviceOs(String id) {
    _deviceOs = id;
    notifyListeners();
  }

  notifyListeners();
}
