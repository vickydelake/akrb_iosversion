import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';

//bool _canCheckBiometrics;
//List<BiometricType> _availableBiometrics;
//String _authorized = 'Not Authorized';
//bool _isAuthenticating = false;
Future<bool> authenticate() async {
  final _auth = LocalAuthentication();

  bool isAuthenticated;
  try {
    isAuthenticated = await _auth.authenticateWithBiometrics(
      localizedReason: 'authenticate to access',
      useErrorDialogs: true,
      stickyAuth: true,
    );
    // print(isAuthenticated);
  } on PlatformException catch (e) {
    print(e);
  }
  return isAuthenticated;
}

Future<bool> checkBiometrics() async {
  final LocalAuthentication auth = LocalAuthentication();
  bool canCheckBiometrics;
  try {
    canCheckBiometrics = await auth.canCheckBiometrics;

    return canCheckBiometrics;
  } on PlatformException catch (e) {
    print(e);
  }
  return canCheckBiometrics;
}
/*
Future<void> _getAvailableBiometrics() async {
  List<BiometricType> availableBiometrics;
  try {
    availableBiometrics = await auth.getAvailableBiometrics();
  } on PlatformException catch (e) {
    print(e);
  }
  if (!mounted) return;

  setState(() {
    _availableBiometrics = availableBiometrics;
  });
}

Future<void> _authenticate() async {
  bool authenticated = false;
  try {
    setState(() {
      _isAuthenticating = true;
      _authorized = 'Authenticating';
    });
    authenticated = await auth.authenticateWithBiometrics(
        localizedReason: 'Scan your fingerprint to authenticate',
        useErrorDialogs: true,
        stickyAuth: true);
    setState(() {
      _isAuthenticating = false;
      _authorized = 'Authenticating';
    });
  } on PlatformException catch (e) {
    print(e);
  }
  if (!mounted) return;

  final String message = authenticated ? 'Authorized' : 'Not Authorized';
  setState(() {
    _authorized = message;
  });
}*/
