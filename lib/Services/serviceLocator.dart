import 'package:akrbankios/Services/callsEmailService.dart';
import 'package:akrbankios/Services/localStorage.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  var instance = await LocalStorageService.getInstance();
  locator.registerLazySingleton<LocalStorageService>(() => instance);
  locator.registerLazySingleton(() => UrlLauncherService());
  // LocalStorageService
}
