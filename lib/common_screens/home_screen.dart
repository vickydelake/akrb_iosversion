// import 'dart:html';
import 'dart:io';

import 'package:akrbankios/Airtime/screens/AirtimeNewBeneficiary.dart';
import 'package:akrbankios/Airtime/screens/AirtimePurchase.dart';
import 'package:akrbankios/Airtime/screens/AirtimeSavedBeneficiary.dart';
import 'package:akrbankios/Airtime/screens/AirtimeSelf.dart';
import 'package:akrbankios/Auth/screens/Login_alt.dart';
import 'package:akrbankios/Beneficiaries/screens/beneficiaries.dart';
import 'package:akrbankios/Transfers/screens/AccountToMomo.dart';
import 'package:akrbankios/Transfers/screens/MomoToAccount.dart';
import 'package:akrbankios/common_screens/Branches.dart';
import 'package:akrbankios/Requests/screens/ChequeBookReqt.dart';
import 'package:akrbankios/Complaints.dart';
import 'package:akrbankios/Auth/screens/Login.dart';
import 'package:akrbankios/Payments.dart';
import 'package:akrbankios/Requests.dart';
import 'package:akrbankios/Requests/screens/SalaryAdvance.dart';
import 'package:akrbankios/Setting.dart';
import 'package:akrbankios/Requests/screens/StatementRequest.dart';
import 'package:akrbankios/Requests/screens/StopCheque.dart';
import 'package:akrbankios/Transfers/screens/ThirdpartyTransfers.dart';
import 'package:akrbankios/TransferFunds.dart';
import 'package:akrbankios/Services/localStorage.dart';
import 'package:akrbankios/Services/serviceLocator.dart';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/common_widgets/enums/menuItems.dart';
import 'package:akrbankios/common_widgets/home_menu_items.dart';
import 'package:akrbankios/models/userModel.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:marquee/marquee.dart';

import 'package:akrbankios/Transfers/screens/OwnAccountTransfer.dart';
import 'package:akrbankios/Balances/screens/ViewBalance.dart';
import 'package:akrbankios/common_widgets/bottom_sheet.dart';

class HomeScreen extends StatefulWidget {
  final User user;
  final Function onSettingsPressed;
  const HomeScreen({Key key, this.onSettingsPressed, this.user})
      : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  User user;
  int _current = 0;
  int currentTabIndex = 0;
  // List<Widget> tabs = [Settings(Colors.orange), Settings(Colors.blue)];
  onTapped(int index) {
    setState(() {
      currentTabIndex = index;
    });
  }

  LocalStorageService storageService = locator<LocalStorageService>();
  bool biometric = false;

  File _image;

  @override
  void initState() {
    super.initState();
    user = widget.user;
    _loadProfilePic();
  }

  _loadProfilePic() {
    if (storageService.profilePic != null) {
      if (storageService.profilePic.isNotEmpty) {
        print('loading pic: ${storageService.profilePic}');
        final File localImage = File(storageService.profilePic);

        if (mounted) {
          setState(() {
            _image = localImage;
            print("**** set image is ${_image.path}");
          });
        }
      }
    } else {
      print("*** image does not exist");
    }
  }

  _setProfilePic(ImageSource imageSource) async {
    print('sharedpref is null?: ${storageService.profilePic}');
    setState(() {
      _image = null;
    });

    // getting a directory path for saving
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path;
    final picker = ImagePicker();

    final pickedFile = await picker.getImage(source: imageSource);
    //Check for valid file
    if (pickedFile == null) {
      final File localImage = File(storageService.profilePic);
      setState(() {
        _image = localImage;
      });
    } else {
      PaintingBinding.instance.imageCache.clear();
      // copy the file to a new path
      final File selectedFile = File(pickedFile.path);
      final File newImage =
          await selectedFile.copy('$path/akrb_profile_pic.png');

      if (mounted) {
        setState(() {
          _image = newImage;
        });
      }
      // print(newImage.path);
      storageService.profilePic = newImage.path;
    }
  }

  Widget imageSelector(context) {
    final _screenHeight = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: () {
        // Navigator.of(context).pop();
        _showProfileBottomSheet(context);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(_screenHeight * 0.065),
            bottomRight: Radius.circular(_screenHeight * 0.065)),
        child: Container(
          // height: _screenHeight * 0.06,
          decoration: BoxDecoration(color: Colors.black.withOpacity(0.4)),
          margin: EdgeInsets.only(top: _screenHeight * 0.08),
          child: Center(
              child: Icon(
            CupertinoIcons.camera,
            color: Color(0xFFFFCC00),
            size: 30.0,
          )),
        ),
      ),
    );
  }

  void _showProfileBottomSheet(context) {
    double _screenHeight = MediaQuery.of(context).size.height;
    double _screenWidth = MediaQuery.of(context).size.width;

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return Container(
              height: storageService.profilePic.isNotEmpty && _image != null
                  ? _screenHeight * 0.64
                  : _screenHeight * 0.415,
              // padding: EdgeInsets.symmetric(horizontal: _screenWidth * 0.02),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: _screenHeight * 0.005),
                  Container(
                    width: _screenWidth * 0.25,
                    height: _screenHeight * 0.005,
                    color: Colors.grey[700],
                  ),
                  SizedBox(height: _screenHeight * 0.02),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Select Option",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey[700]),
                    ),
                  ),
                  SizedBox(height: _screenHeight * 0.02),
                  Expanded(
                    child: Column(
                      children: [
                        storageService.profilePic.isNotEmpty && _image != null
                            ? CustomBottomSheet(
                                index: 2,
                                item: BottomSheetItem(
                                  title: "View Photo",
                                  subtitle: "View Current Profile Photo",
                                  handler: () {
                                    Navigator.of(context).pop();
                                    PaintingBinding.instance.imageCache.clear();
                                  },
                                  icon: OMIcons.photo,
                                ))
                            : Text(''),
                        CustomBottomSheet(
                          item: BottomSheetItem(
                            title: "Choose from Gallery",
                            subtitle: "Select profile photo from Gallery",
                            handler: () {
                              print('gallery');
                              // Navigator.of(context).pop();
                              _setProfilePic(ImageSource.gallery);
                            },
                            icon: OMIcons.photoLibrary,
                          ),
                          index: 0,
                        ),
                        CustomBottomSheet(
                          index: 1,
                          item: BottomSheetItem(
                            title: "Take Photo",
                            subtitle: "Take a new photo with Camera",
                            handler: () {
                              print('camera');
                              // Navigator.of(context).pop();
                              _setProfilePic(ImageSource.camera);
                            },
                            icon: OMIcons.addAPhoto,
                          ),
                        ),
                        storageService.profilePic.isNotEmpty && _image != null
                            ? CustomBottomSheet(
                                index: 2,
                                item: BottomSheetItem(
                                    title: "Remove Photo",
                                    subtitle: "Remove photo from Profile",
                                    handler: () {
                                      print('removed = ${_image.path}');
                                      Navigator.of(context).pop();
                                      PaintingBinding.instance.imageCache
                                          .clear();
                                      setState(() {
                                        _image = null;
                                      });
                                    },
                                    icon: OMIcons.removeCircleOutline))
                            : Text(''),
                      ],
                    ),
                  ),
                  SizedBox(height: _screenHeight * 0.005),
                ],
              ),
            );
          },
        );
      },
    );
  }

  void _showLogoutDialog(context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.zero,
            titlePadding: EdgeInsets.zero,
            title: Container(
              height: _height * 0.06,
              padding: EdgeInsets.symmetric(
                vertical: _height * 0.01,
              ),
              color: Color(0XFF339933),
              child: Center(
                child: Text(
                  'Confirm Logout'.toUpperCase(),
                  style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.white.withOpacity(0.9),
                  ),
                ),
              ),
            ),
            content: Container(
              height: _height * 0.12,
              padding: EdgeInsets.symmetric(
                horizontal: _width * 0.08,
                vertical: _height * 0.02,
              ),
              child: Center(
                child: const Text(
                  'Are you sure you want to \nlogout?',
                  style: TextStyle(
                    fontSize: 17.0,
                    color: Colors.black54,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                textColor: Theme.of(context).primaryColor,
                child: Text(
                  'Cancel'.toUpperCase(),
                  style: TextStyle(
                    // fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                textColor: Theme.of(context).primaryColor,
                child: Text(
                  'Logout'.toUpperCase(),
                  style: TextStyle(
                    // fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                  ),
                ),
                onPressed: () {
                  storageService.isLoggedIn = false;
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) => LoginPage()),
                    ModalRoute.withName('/'),
                  );
                },
              )
            ],
          );
        });
  }

  List imgList = [
    // 'https://www.itl.cat/pngfile/big/0-7755_nature-pier-bridge-d-river-water-sunset-night.jpg',
    // 'https://www.akrbank.com/media/n2/ss3/smartslider.min.css?1588063972',
    // 'https://www.facebook.com/Atwima-Kwanwoma-Rural-Bank-Ltd-146413846015711/photos/702466723743751',
    'https://www.akrbank.com/images/akrb_home_loan.jpg',
    'https://www.akrbank.com/images/akrb_home_footer-shares.jpg',
    'https://www.akrbank.com/images/akrb_home_footer-shares.jpg',
    'https://www.akrbank.com/images/akrb_home_footer-shares.jpg',
    // 'https://www.itl.cat/pngfile/big/8-80199_nature-sea-river-water-wave-sunset-sun-bokeh.jpg',
    // 'https://www.itl.cat/pngfile/big/14-141127_full-hd-1080p-nature-wallpapers-download-hd-4k.jpg',
    // 'https://www.itl.cat/pngfile/big/26-261045_10-top-nature-hd-wallpapers-1080p-full-hd.jpg',
  ];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  List<BottomSheetItem> transfersList = [
    BottomSheetItem(
      title: "Own Account Transfer",
      subtitle: "Transfer to your own account",
      widget: OwnAccountTransfer(),
      icon: OMIcons.person,
    ),
    BottomSheetItem(
      title: "MoMo to Account Transfer",
      subtitle: "From MoMo to Bank account",
      widget: MomoToAccount(),
      icon: OMIcons.mobileScreenShare,
    ),
    BottomSheetItem(
      title: "Account to MoMo Transfer",
      subtitle: "From Bank account to MoMo",
      widget: AccountToMomo(),
      icon: OMIcons.mobileFriendly,
    ),
    BottomSheetItem(
      title: "Third Party Transfers",
      subtitle: "Transfer to third-party account",
      widget: ThirdPartyTransfer(),
      icon: OMIcons.people,
    ),
  ];

  List<BottomSheetItem> requestsList = [
    BottomSheetItem(
      title: "Statement Request",
      subtitle: "Request for account statement",
      widget: StatementRequest(),
      icon: OMIcons.receipt,
    ),
    BottomSheetItem(
      title: "Cheque Book Request",
      subtitle: "Request for a new Cheque Book",
      widget: ChequeBookReqt(),
      icon: OMIcons.collectionsBookmark,
    ),
    BottomSheetItem(
      title: "Salary Advance",
      subtitle: "Request for a Salary Advance",
      widget: SalaryAdvance(),
      icon: OMIcons.businessCenter,
    ),
    BottomSheetItem(
      title: "Stop Cheque",
      subtitle: "Request to stop Cheque",
      widget: StopCheque(),
      icon: OMIcons.cancelPresentation,
    ),
  ];

  List<BottomSheetItem> airtimeOptionList = [
    BottomSheetItem(
      title: "Buy for Self",
      subtitle: "Buy Airtime for Self",
      widget: AirtimeSelf(),
      icon: OMIcons.person,
    ),
    BottomSheetItem(
      title: "Beneficiary Number",
      subtitle: "Buy Airtime for new beneficiary",
      widget: AirtimeNewBeneficiary(),
      icon: OMIcons.personAdd,
    ),
    BottomSheetItem(
      title: "Saved Beneficiary",
      subtitle: "Buy Airtime for saved beneficiary",
      widget: AirtimeSavedBeneficiary(),
      icon: OMIcons.people,
    ),
  ];

  double _height;
  double _width;

  void _onSelectMenuItem(MenuItem item, BuildContext context) {
    print('clicked!');
    switch (item) {
      case MenuItem.balances:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ViewBalance(image: _image)));
        break;
      case MenuItem.transfers:
        _showBottomSheet(context, transfersList, 'Transfer Options');
        break;
      case MenuItem.direct_debit:
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ViewBalance()));
        break;
      case MenuItem.airtime:
        _showBottomSheet(context, airtimeOptionList, 'Airtime Purchase Option');
        break;
      case MenuItem.beneficiaries:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => InternalBene(
              user: widget.user,
            ),
          ),
        );
        break;
      case MenuItem.requests:
        _showBottomSheet(context, requestsList, 'Request Options');
        break;
      case MenuItem.payments:
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Payments()));
        break;
      case MenuItem.complaints:
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Complaints()));
        break;
      case MenuItem.branches:
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Branches()));
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            children: [
              Container(
                // margin: EdgeInsets.only(top: _height * 0.027),
                height: _height * 0.25,
                width: _width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/background2.jpg'),
                    fit: BoxFit.fill,
                    // colorFilter: ColorFilter.mode(Colors.blue, BlendMode.difference),
                  ),
                  color: Color(0XFF339933).withOpacity(0.8),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(35.0),
                      bottomRight: Radius.circular(35.0)),
                ),
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  decoration: BoxDecoration(
                    color: Color(0XFF339933).withOpacity(0.8),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(35.0),
                        bottomRight: Radius.circular(35.0)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "AKR Bank Mobile",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17.0,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            GestureDetector(
                              child: Icon(
                                OMIcons.powerSettingsNew,
                                size: 30.0,
                                color: Color(0xFFFFCC00),
                              ),
                              onTap: () => _showLogoutDialog(context),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: _height * 0.02,
                      ),
                      Row(
                        children: [
                          _image == null
                              ? GestureDetector(
                                  onTap: () => _showProfileBottomSheet(context),
                                  child: Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Icon(
                                      Icons.camera_alt,
                                      color: Color(0xFFFFCC00),
                                      size: 30,
                                    ),
                                    width: _width * 0.22,
                                    height: _height * 0.10,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 2, color: Colors.white70),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        // topRight: Radius.circular(10),
                                        // bottomLeft: Radius.circular(10),
                                        bottomRight: Radius.circular(20),
                                      ),
                                    ),
                                  ),
                                )
                              : Container(
                                  margin: EdgeInsets.only(left: _height * 0.02),
                                  child: CircleAvatar(
                                    radius: _height * 0.06,
                                    backgroundImage: FileImage(_image),
                                    child: imageSelector(context),
                                  ),
                                ),
                          SizedBox(
                            width: _width * 0.055,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Utilities.getGreeting().toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 13),
                              ),
                              SizedBox(
                                height: _height * 0.01,
                              ),
                              Text(
                                widget.user.mbUserName.toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15.5,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: _height * 0.01,
                              ),
                              Text(
                                "AKRB PREMIUM CLIENT",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 11,
                                ),
                              ),
                              SizedBox(
                                height: _height * 0.01,
                              ),
                              Container(
                                width: _width * 0.6,
                                height: 20,
                                child: Marquee(
                                  text: widget.user.lastModificationDate,
                                  blankSpace: 40.0,
                                  pauseAfterRound: Duration(
                                    milliseconds: 280,
                                  ),
                                  // overflow: TextOverflow.ellipsis,
                                  // softWrap: false,
                                  style: TextStyle(
                                    color: Colors.white,
                                    letterSpacing: 0.2,
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: _height * 0.46,
                color: Colors.white,
                alignment: Alignment.center,
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                child: HomeMenuItems(
                  onSelectItem: (item) => _onSelectMenuItem(item, context),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: _height * 0.007),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CarouselSlider(
                      aspectRatio: 16 / 9,
                      height: _height * 0.13,
                      initialPage: 0,
                      enlargeCenterPage: true,
                      autoPlay: true,
                      reverse: false,
                      autoPlayInterval: Duration(seconds: 3),
                      autoPlayAnimationDuration: Duration(milliseconds: 3000),
                      onPageChanged: (index) {
                        setState(() {
                          _current = index;
                        });
                      },
                      items: imgList.map((imgUrl) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              margin: EdgeInsets.symmetric(
                                  horizontal: _width * 0.02),
                              decoration: BoxDecoration(color: Colors.green),
                              child: Image.network(
                                imgUrl,
                                fit: BoxFit.fill,
                              ),
                            );
                          },
                        );
                      }).toList(),
                    ),
                    SizedBox(
                      height: _height * 0.013,
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: map<Widget>(imgList, (index, url) {
                          return Container(
                            width: 5,
                            height: 5,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _current == index
                                    ? Color(0xFFFFCC00)
                                    : Colors.green),
                          );
                        }))
                  ],
                ),
              ),
            ],
          )),
    );
  }
}

void _showBottomSheet(
    BuildContext context, List<BottomSheetItem> items, String description) {
  double _screenHeight = MediaQuery.of(context).size.height;
  double _screenWidth = MediaQuery.of(context).size.width;

  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return Container(
            height: _screenHeight * (0.11 * items.length + 0.205),
            padding: EdgeInsets.symmetric(horizontal: _screenWidth * 0.045),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: _screenHeight * 0.01),
                Container(
                  width: _screenWidth * 0.2,
                  height: _screenHeight * 0.005,
                  color: Colors.grey[500],
                ),
                SizedBox(height: _screenHeight * 0.025),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    description,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey[700]),
                  ),
                ),
                SizedBox(height: _screenHeight * 0.028),
                Expanded(
                  child: ListView.builder(
                    itemCount: items.length,
                    itemBuilder: (context, index) => CustomBottomSheet(
                      item: items[index],
                      index: index,
                    ),
                  ),
                ),
                SizedBox(height: _screenHeight * 0.005),
              ],
            ),
          );
        },
      );
    },
  );
}
