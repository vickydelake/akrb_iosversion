import 'dart:async';

import 'package:akrbankios/Auth/screens/Login_alt.dart';
import 'package:flutter/material.dart';
import '../common_widgets/animated-logo.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  Animation<double> logoAnimation;
  AnimationController logoController;

  @override
  void initState() {
    super.initState();

    logoController = AnimationController(
      duration: Duration(milliseconds: 2500),
      vsync: this,
      value: 0.1,
    );

    logoAnimation = CurvedAnimation(
      parent: logoController,
      curve: Curves.linear,
    );

    logoController.forward();

    logoAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        Timer(
            Duration(milliseconds: 600),
            () => Navigator.push(
                context, MaterialPageRoute(builder: (context) => LoginPage())));
      }
    });
  }

  @override
  void dispose() {
    logoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFF339933),
      body: Stack(
        children: [
          Container(
            color: Color(0XFF339933),
            alignment: Alignment.center,
            child: ScaleTransition(
              scale: logoAnimation,
              child: AnimatedLogo(),
              alignment: Alignment.center,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 15.0),
              child: Text(
                'Atwima Kwanwoma Rural Bank (c) 2020',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                  // fontFamily: "Quicksand",
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
