import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  final history =
      "Atwima Kwanwoma Rural bank was established on the 6th of September 1983 as a financial institution empowered by the bank of Ghana to provide financial service and intermediation within a defined operation area of 32km radius from the main office at Pakyi No.2. As the 68th to be established in the country and the 13th for the Ashanti Region the bank has gone through a lot of changes.\nThe bank started with initial ordinary shares capital of ¢1.0million and ¢ 125,000 preference shares contributed by the indigenous people in the locality and the bank of Ghana";
  final vision =
      "\nThe bank’s vision is to transform from rural bank into a fully-fledged hybridized community bank cum micro finance institution.\n“The bank’s vision is to be the leading and preferred rural bank in Ghana”.";
  final mission =
      "\n“The Bank’s mission to be the leading and preferred rural bank in Ghana, poised to transform the lives and businesses of our stakeholders through motivated  and competent human resources; customer-driven products and the use of modern technology”Our Slogan is /' Pace Setters in Rural Banking/' ";
  final contacts =
      "Atwima Kwanwoma Rural Bank Ltd,\nP.O.Box 6921\nKumasi - Ghana";
  final Phone = "Phone Number\n+23332 229 8900";
  final email = "E-mail\ninfo@akrbank.com";
  final reachUsOn =
      "Reach us here\n website:akrbank.com\n facebook :facebook.com/akrbank";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0XFF339933),
        title: Text("ABOUT US"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "HISTORY",
                style: TextStyle(color: Color(0XFFFFCC00), fontSize: 15),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                history,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13.5,
                    fontStyle: FontStyle.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "VISION",
                style: TextStyle(
                  color: Color(0XFFFFCC00),
                  fontSize: 15.0,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                vision,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontStyle: FontStyle.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "MISSION",
                style: TextStyle(
                  color: Color(0XFFFFCC00),
                  fontSize: 15.0,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                mission,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontStyle: FontStyle.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "CONTACTS",
                style: TextStyle(
                  color: Color(0XFFFFCC00),
                  fontSize: 15.0,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                contacts,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontStyle: FontStyle.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                Phone,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontStyle: FontStyle.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                email,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontStyle: FontStyle.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                reachUsOn,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontStyle: FontStyle.normal),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
