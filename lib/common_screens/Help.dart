import 'package:flutter/material.dart';

class Help extends StatelessWidget {
  final help =
      "  1.	Why should I sign up for Mobile Banking? \n•	Mobile Banking is a fast, easy and secure way of accessing your bank account 24 hours a day 7 days a week from wherever you are.\n\n 2.	What is Mobile Banking?\n•	Mobile Banking is a service that lets you access your bank account from mobile phone.\n\n 3.	How do I register for Mobile Banking?\n•	Customers can register by walking into any AKRB Branch and fill the electronic banking request form.\n\n 4.	What transactions can I perform using this?\n•	Balance Enquiry \n•	Mini statement \n•	Funds transfer \n•	Request for cheque book \n•	Request for bank statement \n\n\n 5.	Funds Transfers ’third party and other banks”\n•	Will I be able to transfer funds even if I do not register the beneficiary account?No, you will have to add the beneficiary account before you are able to transfer the funds.  This can be done by selecting “add beneficiaries” in the menu. You can also view the list of beneficiary created on the mobile app.\n\n 6.	How do I transfer money to other bank accounts?\n This can be done by using Direct Credit\n\n 7.	Bill Payments/Airtime purchase\nComing soon…\n\n 8.	PIN related queries\n•	What do I do if I forget my PIN?Please call our Call Centre on (PHONE_NUMBER) and they will take your request for a new PIN or visit any of our branches.\n\n 9.	Access Related Queries•	Why are all my accounts not displayed on my handset?The only accounts displayed on Mobile Banking are the ones linked to your account number. Only linked accounts can be viewed at one go.\n\n  10.	What should I do in case my handset gets stolen? \n•	Please call our customer Contact Centre on +233244958517 and inform them of the loss.\n\n 11.	How secure are the transactions carried on my mobile phone?\nWhen you sign up for Mobile Banking, you create a unique PIN that ensures only you can access the accounts.  And all transactions will be authorized only after an ETAC code (which will be sent by the app) is entered for every transaction on the app.\n \n 12. Apply Now\n•	Call us now on +233244958517. \n•	Alternatively visit your nearest branch. \n•	Terms and conditions apply. \n•	Mobile Banking is not available for company accounts \n•	Charges are applicable as per the Tariff Guide \n";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0XFF339933),
        title: Text(
          "HELP",
          style: TextStyle(fontSize: 15),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("HELP",
                  style: TextStyle(fontSize: 15, color: Color(0XFFFFCC00))),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(help,
                  style: TextStyle(fontSize: 13.5, color: Colors.black)),
            )
          ],
        ),
      ),
    );
  }
}
