import 'package:flutter/material.dart';

class Branches extends StatefulWidget {
  @override
  _BranchesState createState() => _BranchesState();
}

class _BranchesState extends State<Branches> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text("View Akrb Branches ",
              style: TextStyle(color: Colors.white, fontSize: 15))),
    );
  }
}
