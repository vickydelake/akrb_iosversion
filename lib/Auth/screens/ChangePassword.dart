import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  var changePassword = new GlobalKey<ScaffoldState>();
  TextEditingController oldPassword = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController confirmPassword = new TextEditingController();
  TextEditingController securityAnswer = new TextEditingController();
  bool isPasswordActive = false;
  bool isConfirmPasswordActive = false;
  bool isOldPasswordActive = false;
  bool hidePassword = true;
  bool hideComfirmPassword = true;
  bool hideOldPassword = true;
  bool passwordActive = false;
  double _height;
  double _width;

  Widget passwordField(controller, bool confirm) {
    return Container(
      width: _width * 0.85,
      padding: EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.all(
          Radius.circular(8.0),
        ),
      ),
      child: TextField(
        obscureText: confirm ? hideComfirmPassword : hidePassword,
        keyboardType: TextInputType.emailAddress,
        controller: controller,
        onChanged: (value) {
          if (value != '') {
            setState(() {
              confirm ? isConfirmPasswordActive = true : passwordActive = true;
            });
          } else {
            !confirm ? passwordActive = false : isConfirmPasswordActive = false;
          }
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: confirm ? "Confirm Password" : "Password",
          hintStyle: TextStyle(color: Colors.grey[400]),
          suffixIcon: !confirm
              ? passwordActive
                  ? GestureDetector(
                      onTap: () {
                        setState(() {
                          hidePassword = !hidePassword;
                        });
                      },
                      child: Icon(
                        hidePassword ? Icons.visibility : Icons.visibility_off,
                      ),
                    )
                  : Icon(Icons.lock)
              : isConfirmPasswordActive
                  ? GestureDetector(
                      onTap: () {
                        setState(() {
                          hideComfirmPassword = !hideComfirmPassword;
                        });
                      },
                      child: Icon(
                        hideComfirmPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    )
                  : Icon(Icons.lock),
        ),
      ),
    );
  }

  Widget textField(controller, description, icon) {
    return Column(
      children: [
        Container(
          width: _width * 0.85,
          padding:
              EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: TextField(
            keyboardType: TextInputType.text,
            controller: controller,
            onChanged: (value) {
              if (value != '') {
                print('');
              } else {
                print('');
              }
            },
            style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: description,
              hintStyle: TextStyle(color: Colors.grey[400]),
              // errorText: '',
              suffixIcon: icon,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        key: changePassword,
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text('Change Password'),
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          reverse: true,
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(
                horizontal: _width * 0.05, vertical: _height * 0.03),
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(height: _height * 0.001),
                Container(
                  height: _height * 0.16,
                  width: _width * 0.34,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    // color: Colors.grey[200],
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                      "assets/images/account_access.svg",
                      height: _height * 0.13,
                      width: _width * 0.27,
                      // color: Colors.grey[800],
                    ),
                  ),
                ),
                // SizedBox(height: _height * 0.001),
                Text(
                  'Answer your security question in order to create a new password',
                  style: TextStyle(
                    fontSize: 14.5,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey[500],
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: _height * 0.02),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 3.0),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Text('QUES.',
                          style: TextStyle(
                              fontSize: 15.0, color: Color(0XFF339933))),
                      SizedBox(width: 10.0),
                      Text(
                        'What is your favorite soccer team?',
                        style:
                            TextStyle(fontSize: 15.0, color: Color(0XFF339933)),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: _height * 0.02),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Answer Security Question',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                textField(securityAnswer, 'Answer to security question',
                    Icon(Icons.question_answer)),
                SizedBox(height: _height * 0.012),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Enter old Password',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                textField(oldPassword, 'Old Password', Icon(Icons.lock)),
                SizedBox(height: _height * 0.012),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Enter new Password',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                passwordField(password, false),
                SizedBox(height: _height * 0.012),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Confirm new Password',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                passwordField(confirmPassword, true),
                SizedBox(height: _height * 0.04),
                RaisedButton(
                  padding: EdgeInsets.symmetric(
                      horizontal: _width * 0.08, vertical: _height * 0.015),
                  onPressed: () {},
                  color: Color(0XFF339933),
                  textColor: Colors.white,
                  child: Text(
                    'Confirm',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
