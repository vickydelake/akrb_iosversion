import 'dart:io';

import 'package:akrbankios/Auth/screens/ForgotPin.dart';
import 'package:akrbankios/Providers/phone_info.dart';
import 'package:akrbankios/controllers/auth_controller.dart';
import 'package:connectivity/connectivity.dart';
import 'package:device_info/device_info.dart';
import 'package:get_ip/get_ip.dart';
import 'package:provider/provider.dart';

import 'package:akrbankios/common_screens/AboutPage.dart';
import 'package:akrbankios/common_widgets/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:akrbankios/Services/fingerprint.dart';

import 'package:akrbankios/common_screens/Help.dart';

class Login extends StatefulWidget {
  final String name;

  const Login({Key key, this.name}) : super(key: key);
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  double _height;
  double _width;
  var login = new GlobalKey<ScaffoldState>();

  TextEditingController _password = new TextEditingController();
  TextEditingController _username;
  bool _isHidden = true;
  bool _isLoading = false;
  bool _isLoading2 = false;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  Future<bool> bioCheck;
  bool _hasBiometric = true;
  FocusNode myFocusNode;
  var deviceId;
  var deviceIp;
  var model;
  var manufacturer;
  var brand;
  var country;
  String deviceOS;
  var appVersion;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String _currentCountry;

  _getAddressFromLatLng() async {
    final _phoneInfo = Provider.of<PhoneInfo>(context, listen: false);

    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentCountry = place.country;
        _phoneInfo.setcountry(_currentCountry);
      });

      print('country: ${place.country}');
    } catch (e) {
      print(e);
    }
  }

  _getCurrentLocation() async {
    try {
      final position = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
      setState(() {
        _currentPosition = position;
      });
      _getAddressFromLatLng();
    } catch (e) {
      print(e);
    }
  }

  Future<Null> getPhoneinfo() async {
    String ipAddress = await GetIp.ipAddress;
    final _phoneInfo = Provider.of<PhoneInfo>(context, listen: false);

    deviceIp = ipAddress;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (androidInfo == null) {
      } else {
        deviceId = androidInfo.androidId;
        model = androidInfo.model;
        manufacturer = androidInfo.manufacturer;
        brand = androidInfo.brand;
        deviceOS = "A";
      }
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceId = iosInfo.identifierForVendor;
      model = iosInfo.model;
      manufacturer = iosInfo.name;
      brand = iosInfo.systemVersion;
      deviceOS = "i";
    }
    _phoneInfo.setbrand(brand);
    _phoneInfo.setdeviceID(deviceId);
    _phoneInfo.setdeviceOs(deviceOS);
    _phoneInfo.setmodel(model);
    _phoneInfo.setmanufacturer(manufacturer);
    _phoneInfo.setdeviceIp(deviceIp);
  }

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  void initState() {
    super.initState();
    checkBiometrics().then((value) {
      setState(() {
        _hasBiometric = value;
      });
    });
    _getCurrentLocation();
    getPhoneinfo();
    String user = widget.name;
    print(user);
    if (user == null || user.isEmpty || user.length == 0) {
      _username = new TextEditingController();
    } else {
      _username = new TextEditingController(text: user);
    }
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
//      backgroundColor: Color(0XFF339933),
        key: login,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
//          Image.asset("assets/loginbg.png"),
            Image(
              image: AssetImage("assets/loginbg.png"),
              fit: BoxFit.cover,
            ),
            Container(
              margin: EdgeInsets.only(top: _height * 0.38),
              padding: EdgeInsets.only(
                top: _height * 0.08,
                left: _height * 0.03,
                right: _height * 0.03,
              ),
              width: MediaQuery.of(context).size.width,
              height: 280,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  ),
                  color: Colors.white),
              child: Column(
                children: <Widget>[
                  SizedBox(height: _height * 0.06),
                  Container(
                    padding: const EdgeInsets.only(
                      right: 15.0,
                      left: 25.0,
                      top: 6.0,
                    ),
                    height: _height * 0.068,
                    width: _width * 0.9,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(6.0)),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "User ID",
                        hintStyle: TextStyle(color: Colors.grey[500]),
                        suffixIcon: Icon(Icons.person),
                      ),
                    ),
                  ),
                  SizedBox(height: _height * 0.03),
                  Container(
                    // padding: const EdgeInsets.only(top: 20, right: 30, left: 31),
                    width: _width * 0.9,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(
                            right: 15.0,
                            left: 25.0,
                            top: 6.0,
                          ),
                          height: _height * 0.068,
                          width:
                              _hasBiometric ? _width * 0.75 : _width * 0.8848,
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius:
                                BorderRadius.all(Radius.circular(6.0)),
                          ),
                          child: TextFormField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Password",
                              hintStyle: TextStyle(color: Colors.grey[500]),
                              suffixIcon: Icon(Icons.visibility_off),
                            ),
                          ),
                        ),
                        SizedBox(
                            width:
                                _hasBiometric ? _width * 0.01 : _width * 0.0),
                        _hasBiometric
                            ? IconButton(
                                icon: Icon(Icons.fingerprint),
                                iconSize: _width * 0.12,
                                color: Color(0XFF339933),
                                onPressed: () async {
                                  final authenticated = await authenticate();
                                  if (authenticated) {
                                    AuthController.bioLoginController(
                                      context: context,
                                      key: login,
                                      username: _username.text,
                                      deviceID: deviceId,
                                      deviceIP: deviceIp,
                                      model: model,
                                      manufacturer: manufacturer,
                                      brand: brand,
                                      country: country,
                                      os: deviceOS,
                                      appVersion: appVersion,
                                    );
                                  }
                                },
                              )
                            : Text(''),
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                        left: 3.0, right: 3.0, top: 15.0, bottom: 25.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ForgotPin(),
                              ),
                            );
                          },
                          child: Text(
                            "Forgot password?",
                            style: TextStyle(
                                color: Color(0XFFFFCC00), fontSize: 13.0),
                          ),
                        ),
                        Text(
                          "Fast Balance",
                          style: TextStyle(
                              color: Color(0XFFFFCC00), fontSize: 13.0),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: _height * 0.02),
                  Container(
                    width: _width,
                    height: _height * 0.06,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(6.0))),
                      child: Text("LOGIN",
                          style: TextStyle(
                              color: Color(0XFF339933), fontSize: 15)),
//                          color: Colors.yellow[600],
                      color: Color(0XFFFFCC00),
                      onPressed: () {
                        AuthController.loginController(
                          context: context,
                          key: login,
                          username: _username.text,
                          password: _password.text,
                          deviceId: deviceId,
                          deviceIP: deviceIp,
                          model: model,
                          manufacturer: manufacturer,
                          brand: brand,
                          country: country,
                          deviceOS: deviceOS,
                          appVersion: appVersion,
                        );
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => BottomNav()));
                      },
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              top: _height * 0.28,
              left: _width * 0.3,
              width: _width * 0.4,
              height: _height * 0.2,
              child: Image(
                image: AssetImage("assets/akrblogo_big.png"),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  color: Color(0XFF339933),
                  width: _width,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AboutUs()));
                        },
                        child: Text(
                          "ABOUT US",
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ),
                      Container(
                        width: 1,
                        height: 20,
                        color: Colors.white,
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Help()));
                        },
                        child: Text(
                          "HELP ",
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ),
                      Container(
                        width: 1,
                        height: 20,
                        color: Colors.white,
                      ),
                      Text(
                        "SELF ENROL",
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      )
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
