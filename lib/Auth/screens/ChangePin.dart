import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pinput/pin_put/pin_put.dart';

class ChangePin extends StatefulWidget {
  @override
  _ChangePinState createState() => _ChangePinState();
}

class _ChangePinState extends State<ChangePin> {
  var changePin = new GlobalKey<ScaffoldState>();
  TextEditingController oldPin = new TextEditingController();
  TextEditingController pin = new TextEditingController();
  TextEditingController confirmPin = new TextEditingController();
  TextEditingController securityAnswer = new TextEditingController();
  double _height;
  double _width;

  Widget pin_input(controller) {
    return Container(
      width: _width * 0.8,
      child: PinPut(
        fieldsCount: 4,
        controller: controller,
        // autofocus: true,
        onTap: () {},
        obscureText: '*',
        eachFieldHeight: _height * 0.08,
        eachFieldWidth: _width * 0.15,
        textStyle: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
        preFilledWidget: Container(
          decoration: BoxDecoration(
            border: Border.fromBorderSide(BorderSide.none),
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: Colors.grey[200],
          ),
        ),
        followingFieldDecoration: BoxDecoration(
          border: Border.fromBorderSide(BorderSide.none),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Colors.grey[200],
        ),
        submittedFieldDecoration: BoxDecoration(
          border: Border.all(width: 1.0, color: Colors.green[500]),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Colors.grey[200],
        ),
        selectedFieldDecoration: BoxDecoration(
          border: Border.all(width: 1.0, color: Colors.blue[500]),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Colors.grey[200],
        ),
      ),
    );
  }

  Widget textField(controller) {
    return Column(
      children: [
        Container(
          width: _width * 0.8,
          padding:
              EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: TextField(
            keyboardType: TextInputType.text,
            controller: controller,
            onChanged: (value) {
              if (value != '') {
                print('');
              } else {
                print('');
              }
            },
            style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Answer to Security Question",
              hintStyle: TextStyle(color: Colors.grey[400]),
              // errorText: '',
              suffixIcon: Icon(Icons.question_answer),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        key: changePin,
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text('Change Transaction PIN'),
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          reverse: true,
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(
                horizontal: _width * 0.05, vertical: _height * 0.03),
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(height: _height * 0.001),
                Container(
                  height: _height * 0.16,
                  width: _width * 0.34,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    // color: Colors.grey[200],
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                      "assets/images/validate_account.svg",
                      height: _height * 0.13,
                      width: _width * 0.27,
                      // color: Colors.grey[800],
                    ),
                  ),
                ),
                // SizedBox(height: _height * 0.001),
                Text(
                  'Answer your security question in order to create a new transaction PIN',
                  style: TextStyle(
                    fontSize: 14.5,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey[500],
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: _height * 0.02),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 3.0),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Text('QUES.',
                          style: TextStyle(
                              fontSize: 15.0, color: Color(0XFF339933))),
                      SizedBox(width: 10.0),
                      Text(
                        'What is your favorite soccer team?',
                        style:
                            TextStyle(fontSize: 15.0, color: Color(0XFF339933)),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: _height * 0.02),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Answer Security Question',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                textField(securityAnswer),
                SizedBox(height: _height * 0.012),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Eonter old PIN',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                pin_input(oldPin),
                SizedBox(height: _height * 0.012),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Enter new PIN',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                pin_input(pin),
                SizedBox(height: _height * 0.012),
                Container(
                  padding: EdgeInsets.all(4.0),
                  margin: EdgeInsets.only(left: 4.0, bottom: 4.0),
                  alignment: Alignment.centerLeft,
                  child: Text('Confirm new PIN',
                      style: TextStyle(fontSize: 15.0, color: Colors.black45)),
                ),
                pin_input(confirmPin),
                SizedBox(height: _height * 0.04),
                RaisedButton(
                  padding: EdgeInsets.symmetric(
                      horizontal: _width * 0.08, vertical: _height * 0.015),
                  onPressed: () {},
                  color: Color(0XFF339933),
                  textColor: Colors.white,
                  child: Text(
                    'Confirm',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
