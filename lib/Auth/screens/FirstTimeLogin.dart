import 'dart:io';

import 'package:akrbankios/Providers/default.dart';
import 'package:akrbankios/Providers/phone_info.dart';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/common_widgets/input_fields.dart';
import 'package:akrbankios/controllers/auth_controller.dart';
import 'package:akrbankios/models/listModel.dart';
import 'package:akrbankios/models/userModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';

import 'Login_alt.dart';

class FirstTimeLogin extends StatefulWidget {
  final User user;

  const FirstTimeLogin({Key key, this.user}) : super(key: key);
  @override
  _FirstTimeLoginState createState() => _FirstTimeLoginState();
}

class _FirstTimeLoginState extends State<FirstTimeLogin> {
  double _height;
  double _width;
  var firstLogin = new GlobalKey<ScaffoldState>();
  TextEditingController pin = TextEditingController();
  TextEditingController confirmPin = TextEditingController();
  TextEditingController username;
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  TextEditingController securityAnswer = TextEditingController();
  String securityQuestion;

  List<Question> securityQuestions;
  bool isPasswordActive = false;
  bool isConfirmPasswordActive = false;
  bool hidePassword = true;
  bool hideComfirmPassword = true;
  bool passwordActive = false;
  bool submittingPin = false;
  bool submittingSecQuestion = false;
  bool submittingPassword = false;

  Widget pin_input(controller) {
    return PinPut(
      fieldsCount: 4,
      controller: controller,
      // autofocus: true,
      onTap: () {},
      obscureText: '*',
      eachFieldHeight: _height * 0.08,
      eachFieldWidth: _width * 0.15,
      textStyle: TextStyle(
        fontSize: 20.0,
        fontWeight: FontWeight.bold,
      ),
      preFilledWidget: Container(
        decoration: BoxDecoration(
          border: Border.fromBorderSide(BorderSide.none),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Colors.grey[200],
        ),
      ),
      followingFieldDecoration: BoxDecoration(
        border: Border.fromBorderSide(BorderSide.none),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey[200],
      ),
      submittedFieldDecoration: BoxDecoration(
        border: Border.all(width: 1.0, color: Colors.green[500]),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey[200],
      ),
      selectedFieldDecoration: BoxDecoration(
        border: Border.all(width: 1.0, color: Colors.blue[500]),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey[200],
      ),
    );
  }

  Future<void> _setPin(BuildContext context) async {
    final deviceInfo = Provider.of<PhoneInfo>(context, listen: false);

    Map<String, dynamic> result = await AuthController.pinSetupController(
      context: context,
      key: firstLogin,
      token: widget.user.tokenId,
      uniqueId: deviceInfo.getdeviceId,
      pin: pin.text,
      confirmPin: confirmPin.text,
      model: deviceInfo.getmodel,
      manufacturer: deviceInfo.getmamanufacturer,
      brand: deviceInfo.getbrand,
      country: deviceInfo.getcountry,
    );

    print("token: ${widget.user.tokenId},");
    print("uniqueId: ${deviceInfo.getdeviceId},");
    print("pin: ${pin.text},");
    print("confirmPin: ${confirmPin.text},");
    print("model: ${deviceInfo.getmodel},");
    print("manufacturer: ${deviceInfo.getmamanufacturer},");
    print("brand: ${deviceInfo.getbrand},");
    print("country: ${deviceInfo.getcountry},");

    if (result != null) {
      print(result);

      setState(() {
        currentStep += 1;
      });
    }
  }

  void _setSecurityQuestion() {
    if (securityQuestion != null &&
        securityAnswer != null &&
        securityAnswer.text.isNotEmpty) {
      setState(() {
        currentStep += 1;
      });
    }
  }

  Future<void> _setPassword(BuildContext context) async {
    final deviceInfo = Provider.of<PhoneInfo>(context, listen: false);

    Map<String, dynamic> result = await AuthController.passSetupController(
      username: username.text,
      context: context,
      key: firstLogin,
      password: password.text,
      confirmPassword: confirmPassword.text,
      ques: securityQuestion,
      ans: securityAnswer.text,
      deviceId: deviceInfo.getdeviceId,
      deviceIp: deviceInfo.getdeviceIp,
      model: deviceInfo.getmodel,
      manufacturer: deviceInfo.getmamanufacturer,
      brand: deviceInfo.getbrand,
      country: deviceInfo.getcountry,
    );

    if (result != null) {
      print(result);

      setState(() {
        print(result);
        complete = true;
        currentStep += 1;

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => LoginPage(),
          ),
        );
      });
    }
  }

  int currentStep = 0;
  bool complete = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.user.questions);
    setState(() {
      username = TextEditingController(text: widget.user.mbUserName);
      securityQuestions = widget.user.questions;
    });
  }

  @override
  Widget build(BuildContext context) {
    final def = Provider.of<Default>(context, listen: false);
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    List<Step> steps = [
      Step(
        title: Text('Security\nQuestion'),
        state: currentStep > 0 ? StepState.complete : StepState.editing,
        isActive: currentStep >= 0,
        content: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
              horizontal: _width * 0.01, vertical: _height * 0.005),
          color: Colors.white,
          child: Column(
            children: [
              // SizedBox(height: _height * 0.01),
              Text(
                'Create Security Question',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.8,
                  color: Colors.grey[800],
                ),
              ),
              SizedBox(height: _height * 0.002),
              Container(
                height: _height * 0.19,
                width: _width * 0.38,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.grey[200],
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/images/questions.svg",
                    height: _height * 0.13,
                    width: _width * 0.27,
                    // color: Colors.grey[800],
                  ),
                ),
              ),
              SizedBox(height: _height * 0.005),
              Text(
                "You'll need to answer this question to make changes to account settings",
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey[500],
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: _height * 0.035),
              Container(
                padding: EdgeInsets.all(4.0),
                margin: EdgeInsets.only(left: 4.0, bottom: 6.0),
                alignment: Alignment.centerLeft,
                child: Text('Select Security Question',
                    style: TextStyle(fontSize: 15.0, color: Colors.black45)),
              ),
              CustomDropdown(
                value: 'code',
                title: 'description',
                fillSpace: true,
                useBorder: false,
                color: Colors.grey[200],
                data: securityQuestions,
                onChanged: (val) {
                  print(val);
                  setState(() {
                    securityQuestion = val;
                  });
                },
              ),
              SizedBox(height: _height * 0.03),
              Container(
                padding: EdgeInsets.all(4.0),
                margin: EdgeInsets.only(left: 4.0, bottom: 6.0),
                alignment: Alignment.centerLeft,
                child: Text('Enter Answer',
                    style: TextStyle(fontSize: 15.0, color: Colors.black45)),
              ),
              textField(securityAnswer, 'Answer to Security Question', 't',
                  Icons.question_answer),
              // SizedBox(height: _screenHeight * 0.025),
            ],
          ),
        ),
      ),
      Step(
        title: Text('Password'),
        state: currentStep > 1 ? StepState.complete : StepState.editing,
        isActive: currentStep >= 1,
        content: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
              horizontal: _width * 0.01, vertical: _height * 0.01),
          color: Colors.white,
          child: Column(
            children: [
              // SizedBox(height: _height * 0.01),
              Text(
                'Create Password',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.8,
                  color: Colors.grey[800],
                ),
              ),
              SizedBox(height: _height * 0.001),
              Container(
                height: _height * 0.16,
                width: _width * 0.36,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.grey[200],
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/images/account_access.svg",
                    height: _height * 0.12,
                    width: _width * 0.25,
                    // color: Colors.grey[800],
                  ),
                ),
              ),
              SizedBox(height: _height * 0.003),
              Text(
                'This Password will be required for subsequent logins, after you have logged out',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey[500],
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: _height * 0.03),
              Container(
                padding: EdgeInsets.all(4.0),
                margin: EdgeInsets.only(left: 4.0, bottom: 6.0),
                alignment: Alignment.centerLeft,
                child: Text('Username',
                    style: TextStyle(fontSize: 15.0, color: Colors.black45)),
              ),
              textField(username, 'Username', 't', Icons.person),
              SizedBox(height: _height * 0.01),
              Container(
                padding: EdgeInsets.all(4.0),
                margin: EdgeInsets.only(left: 4.0, bottom: 6.0),
                alignment: Alignment.centerLeft,
                child: Text('Enter Password',
                    style: TextStyle(fontSize: 15.0, color: Colors.black45)),
              ),
              passwordField(password, false),
              SizedBox(height: _height * 0.01),
              Container(
                padding: EdgeInsets.all(4.0),
                margin: EdgeInsets.only(left: 4.0, bottom: 6.0),
                alignment: Alignment.centerLeft,
                child: Text('Confirm Password',
                    style: TextStyle(fontSize: 15.0, color: Colors.black45)),
              ),
              passwordField(confirmPassword, true)
              // SizedBox(height: _screenHeight * 0.025),
            ],
          ),
        ),
      ),
      Step(
        title: Text('Pin'),
        state: currentStep > 2 ? StepState.complete : StepState.editing,
        isActive: currentStep == 2,
        content: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
              horizontal: _width * 0.01, vertical: _height * 0.01),
          color: Colors.white,
          child: Column(
            children: [
              // SizedBox(height: _height * 0.01),
              Text(
                'Create Transaction Pin',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.8,
                  color: Colors.grey[800],
                ),
              ),
              SizedBox(height: _height * 0.002),
              Container(
                height: _height * 0.19,
                width: _width * 0.38,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.grey[200],
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/images/validate_account.svg",
                    height: _height * 0.13,
                    width: _width * 0.27,
                    // color: Colors.grey[800],
                  ),
                ),
              ),
              SizedBox(height: _height * 0.003),
              Text(
                'This 4-digit PIN will be required for all your transactions you perform',
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey[500],
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: _height * 0.035),
              Container(
                padding: EdgeInsets.all(4.0),
                margin: EdgeInsets.only(left: 4.0, bottom: 6.0),
                alignment: Alignment.centerLeft,
                child: Text('Enter 4-digit PIN',
                    style: TextStyle(fontSize: 15.0, color: Colors.black45)),
              ),
              pin_input(pin),
              SizedBox(height: _height * 0.015),
              Container(
                padding: EdgeInsets.all(4.0),
                margin: EdgeInsets.only(left: 4.0, bottom: 6.0),
                alignment: Alignment.centerLeft,
                child: Text('Confirm PIN',
                    style: TextStyle(fontSize: 15.0, color: Colors.black45)),
              ),
              pin_input(confirmPin),
              // SizedBox(height: _screenHeight * 0.025),
            ],
          ),
        ),
      ),
    ];

    _goTo(int step) {
      print('moving to $step');
      setState(() {
        currentStep = step;
      });
    }

    _next(int step) async {
      switch (step) {
        case 1:
          print('setting security question');
          _setSecurityQuestion();
          break;
        case 2:
          print('setting password');
          _setPassword(context);
          break;
        case 3:
          print('setting pin');
          await _setPin(context);

          break;
        default:
      }
    }

    _cancel() {
      if (currentStep > 0) {
        setState(() {
          print('setting currentStep to ${currentStep - 1}');
          currentStep = currentStep - 1;
        });
      }
    }

    _buildContinueButtonText(int index) {
      String title;
      switch (index) {
        case 0:
          title = 'Save Question';
          break;
        case 1:
          title = 'Save Password';
          break;
        case 2:
          title = 'Save Pin';
          break;
        default:
      }
      return title.toUpperCase();
    }

    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: Scaffold(
          key: firstLogin,
          resizeToAvoidBottomInset: true,
          resizeToAvoidBottomPadding: true,
          appBar: AppBar(
            backgroundColor: Color(0XFF339933),
            title: Text('Setup Credentials'),
            leading: SizedBox(),
          ),
          body: SingleChildScrollView(
            reverse: true,
            // padding:
            //     EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              height: _height,
              color: Colors.white,
              padding: EdgeInsets.only(
                left: _width * 0.035,
                right: _width * 0.035,
                top: _height * 0.105,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Stepper(
                      controlsBuilder: (BuildContext context,
                          {VoidCallback onStepContinue,
                          VoidCallback onStepCancel}) {
                        return Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 36.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                      // child: null,
                                      ),
                                  RaisedButton(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: 20.0),
                                    // onPressed: () => print('going forward'),
                                    onPressed: onStepContinue,
                                    color: Color(0XFF339933),
                                    child: def.getIsLoading
                                        ? Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(6.0),
                                              child: CircularProgressIndicator(
                                                backgroundColor:
                                                    Color(0XFF339933),
                                              ),
                                            ),
                                          )
                                        : Text(
                                            _buildContinueButtonText(
                                              currentStep,
                                            ),
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16.0,
                                            ),
                                          ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                      type: StepperType.horizontal,
                      steps: steps,
                      currentStep: currentStep,
                      onStepContinue: () => _next(currentStep + 1),
                      onStepCancel: () => _cancel(),
                      onStepTapped: _goTo,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget passwordField(controller, bool confirm) {
    return Column(
      children: [
        Container(
          padding:
              EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: TextField(
            obscureText: confirm ? hideComfirmPassword : hidePassword,
            keyboardType: TextInputType.emailAddress,
            controller: controller,
            onChanged: (value) {
              if (value != '') {
                setState(() {
                  confirm
                      ? isConfirmPasswordActive = true
                      : passwordActive = true;
                });
              } else {
                !confirm
                    ? passwordActive = false
                    : isConfirmPasswordActive = false;
              }
            },
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: confirm ? "Confirm Password" : "Password",
              hintStyle: TextStyle(color: Colors.grey[400]),
              suffixIcon: !confirm
                  ? passwordActive
                      ? GestureDetector(
                          onTap: () {
                            setState(() {
                              hidePassword = !hidePassword;
                            });
                          },
                          child: Icon(
                            hidePassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        )
                      : Icon(Icons.lock)
                  : isConfirmPasswordActive
                      ? GestureDetector(
                          onTap: () {
                            setState(() {
                              hideComfirmPassword = !hideComfirmPassword;
                            });
                          },
                          child: Icon(
                            hideComfirmPassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        )
                      : Icon(Icons.lock),
            ),
          ),
        ),
      ],
    );
  }

  Widget textField(controller, description, type, icon) {
    return Column(
      children: [
        Container(
          width: _width * 0.9,
          padding:
              EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: TextField(
            keyboardType: type == 'n'
                ? TextInputType.number
                : type == 'e'
                    ? TextInputType.emailAddress
                    : TextInputType.text,
            controller: controller,
            onChanged: (value) {
              if (value != '') {
                print('');
              } else {
                print('');
              }
            },
            style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: description,
              hintStyle: TextStyle(color: Colors.grey[400]),
              // errorText: '',
              suffixIcon: Icon(icon),
            ),
          ),
        ),
      ],
    );
  }
}
