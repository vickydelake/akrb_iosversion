import 'dart:io';

import 'package:akrbankios/Providers/default.dart';
import 'package:akrbankios/Providers/phone_info.dart';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/common_widgets/input_fields.dart';
import 'package:akrbankios/common_widgets/uidata.dart';
import 'package:akrbankios/controllers/auth_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';
// import 'package:country_code_picker/country_code_picker.dart';

import 'Login_alt.dart';

class EnrollScreen extends StatefulWidget {
  @override
  _EnrollScreenState createState() => _EnrollScreenState();
}

class _EnrollScreenState extends State<EnrollScreen> {
  var enroll = new GlobalKey<ScaffoldState>();
  TextEditingController customerNumber = new TextEditingController();
  final idNumberController = TextEditingController();
  final telephoneController = TextEditingController();
  TextEditingController pinController = TextEditingController();
  String currentTextPin = "";
  int currentStep = 0;
  bool complete = false;
  double _height;
  double _width;
  bool _continuePressed = false;

  String _validatedCustNum;
  String _validatedCustName;
  String _validatedAuthToken;

  String _idType = 'ID';

  DateTime _dateOfBirth;
  String countryCode;

  // Widget _buildStartDate() {
  //   return DateTimePicker(
  //     selectedDate: _dateOfBirth,
  //     onSelectedDate: (date) => setState(() => _dateOfBirth = date),
  //   );
  // }

  @override
  void initState() {
    super.initState();
    final start = DateTime.now();
    _dateOfBirth = DateTime(start.year, start.month, start.day);
  }

  Future<void> _validateCustomer() async {
    final deviceInfo = Provider.of<PhoneInfo>(context, listen: false);

    Map<String, dynamic> details = {
      "key": enroll,
      "context": context,
      "customerNumber": customerNumber.text,
      "deviceIp": deviceInfo.getdeviceIp,
      "deviceId": deviceInfo.getdeviceId,
      "deviceModel": deviceInfo.getmodel,
      "deviceManufacturer": deviceInfo.getmamanufacturer,
      "deviceBrand": deviceInfo.getbrand,
      "deviceCountry": deviceInfo.getcountry,
      "deviceOs": deviceInfo.getdeviceOs,
      "appVersion": deviceInfo.getappVersion
    };

    details.forEach((key, value) {
      print('$key: $value');
    });

    final response = await AuthController.validateCustomer(details);
    if (response != null) {
      setState(() {
        _validatedCustName = response.mbCustomerName;
        _validatedCustNum = response.mbCustomerNumber;
        _idType = response.mbIdType;
        _validatedAuthToken = response.tokenId;
        currentStep += 1;
      });
    } else {
      print(response);
    }
  }

  Future<void> _confirmCustomerDetails() async {
    final deviceInfo = Provider.of<PhoneInfo>(context, listen: false);
    Map<String, String> details = {
      "customerNumber": _validatedCustNum,
      "dateOfBirth": Utilities.dateFormat(_dateOfBirth),
      "idNumber": idNumberController.text.trim(),
      "telephone": countryCode + telephoneController.text.trim(),
      "authToken": _validatedAuthToken,
    };

    final response = await AuthController.confirmCustDetails(details);
    if (response != null) {
      Utilities.showSuccessDialog(context, response, () {
        Navigator.of(context).pop();
        setState(() {
          currentStep += 1;
        });
      });
    } else {
      print(response);
    }
  }

  void _validateOTPAndRegister() async {
    if (pinController.text.length != 4) {
      print('pin: $currentTextPin');
    } else {
      Map<String, String> details = {
        "customerNumber": _validatedCustNum,
        "secPin": pinController.text.trim(),
        "authToken": _validatedAuthToken,
      };

      final response = await AuthController.registerCustomer(details);
      if (response != null) {
        Utilities.showSuccessDialog(context, response, () {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (BuildContext context) => LoginPage(),
            ),
          );
        });
      } else {
        print(response);
      }
    }
  }

  Widget textField(controller, description, type, icon) {
    return Column(
      children: [
        Container(
          width: _width * 0.9,
          padding:
              EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: TextField(
            keyboardType: type == 'n'
                ? TextInputType.number
                : type == 'e'
                    ? TextInputType.emailAddress
                    : TextInputType.text,
            controller: controller,
            onChanged: (value) {
              if (value != '') {
                print('');
              } else {
                print('');
              }
            },
            style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: description,
              hintStyle: TextStyle(color: Colors.grey[400]),
              // errorText: '',
              suffixIcon: Icon(icon),
            ),
          ),
        ),
      ],
    );
  }

  Widget pin_input(controller) {
    return PinPut(
      fieldsCount: 4,
      controller: controller,
      // autofocus: true,
      onTap: () {},
      obscureText: '*',
      eachFieldHeight: _height * 0.08,
      eachFieldWidth: _width * 0.15,
      textStyle: TextStyle(
        fontSize: 20.0,
        fontWeight: FontWeight.bold,
      ),
      preFilledWidget: Container(
        decoration: BoxDecoration(
          border: Border.fromBorderSide(BorderSide.none),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Colors.grey[200],
        ),
      ),
      followingFieldDecoration: BoxDecoration(
        border: Border.fromBorderSide(BorderSide.none),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey[200],
      ),
      submittedFieldDecoration: BoxDecoration(
        border: Border.all(width: 1.0, color: Colors.green[500]),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey[200],
      ),
      selectedFieldDecoration: BoxDecoration(
        border: Border.all(width: 1.0, color: Colors.blue[500]),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey[200],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final def = Provider.of<Default>(context, listen: false);
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    List<Step> steps = [
      Step(
        title: Text('Validate'),
        state: currentStep > 0 ? StepState.complete : StepState.editing,
        isActive: currentStep >= 0,
        content: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
              horizontal: _width * 0.01, vertical: _height * 0.01),
          color: Colors.transparent,
          child: Column(
            children: [
              Container(
                height: _height * 0.19,
                width: _width * 0.38,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.grey[200],
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/images/validate_account.svg",
                    height: _height * 0.13,
                    width: _width * 0.27,
                    // color: Colors.grey[800],
                  ),
                ),
              ),
              SizedBox(height: _height * 0.003),
              Text(
                'Please enter your Customer Number in order to enroll',
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey[500],
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: _height * 0.035),
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(
                  left: 2.0,
                  top: 5.0,
                  bottom: 8.0,
                ),
                child: Text(
                  'CUSTOMER NUMBER',
                  style: TextStyle(
                      fontSize: 15.0,
                      letterSpacing: 0.4,
                      color: Color(0XFF339933)),
                ),
              ),
              SizedBox(height: _height * 0.005),
              textField(customerNumber, 'Customer Number', 't',
                  CupertinoIcons.number),

              // SizedBox(height: _screenHeight * 0.025),
            ],
          ),
        ),
      ),
      Step(
        title: Text('Confirm'),
        content: Column(
          children: <Widget>[
            // SizedBox(height: _height * 0.003),
            // Text(
            //   'Confirm Account Details',
            //   style: TextStyle(
            //     fontSize: 16.0,
            //     fontWeight: FontWeight.w400,
            //     color: Colors.grey[500],
            //   ),
            //   textAlign: TextAlign.center,
            // ),
            // SizedBox(height: _height * 0.01),
            // Text('This is the first step.'),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 2.0,
                        left: 2.0,
                      ),
                      child: Text(
                        'ID TYPE',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 15.0,
                            letterSpacing: 0.4,
                            color: Color(0XFF339933)),
                      ),
                    ),
                    SizedBox(height: _height * 0.005),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.only(
                        top: 2.0,
                        left: 2.0,
                        bottom: 5.0,
                      ),
                      child: Text(
                        _idType ?? '',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 16.0,
                          letterSpacing: 0.4,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(height: _height * 0.015),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 2.0,
                            bottom: 5.0,
                          ),
                          child: Text(
                            'CUSTOMER NAME',
                            style: TextStyle(
                                fontSize: 15.0,
                                letterSpacing: 0.4,
                                color: Color(0XFF339933)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: _height * 0.005),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 2.0,
                            left: 2.0,
                          ),
                          child: Text(
                            _validatedCustName ?? '',
                            style: TextStyle(
                              fontSize: 16.0,
                              letterSpacing: 0.4,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  height: _height * 0.18,
                  width: _width * 0.36,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    // color: Colors.grey[200],
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                      "assets/images/confirm.svg",
                      height: _height * 0.15,
                      width: _width * 0.26,
                      // color: Colors.grey[800],
                    ),
                  ),
                ),
              ],
            ),
            // SizedBox(height: _height * 0.005),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.start,
            //   children: <Widget>[],
            // ),

            SizedBox(height: _height * 0.03),
            Divider(color: Color(0XFF339933)),
            SizedBox(height: _height * 0.02),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    left: 2.0,
                    top: 5.0,
                    bottom: 8.0,
                  ),
                  child: Text(
                    'DATE OF BIRTH',
                    style: TextStyle(
                        fontSize: 15.0,
                        letterSpacing: 0.4,
                        color: Color(0XFF339933)),
                  ),
                ),
              ],
            ),
            DatePicker(
              // hint: "Date Of Birth",
              date: DateTime.now(),
              color: Colors.grey[200],
              filled: true,
              data: (val) {
                print(val);
                setState(() {
                  _dateOfBirth = val;
                });
              },
            ),
            SizedBox(
              height: _height * 0.015,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    top: 5.0,
                    left: 2.0,
                  ),
                  child: Text(
                    '${_idType.toUpperCase()} NUMBER',
                    style: TextStyle(
                        fontSize: 15.0,
                        letterSpacing: 0.4,
                        color: Color(0XFF339933)),
                  ),
                ),
              ],
            ),
            SizedBox(height: _height * 0.01),
            textField(idNumberController, 'ID Number', 'n',
                OMIcons.confirmationNumber),
            SizedBox(height: _height * 0.015),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    top: 5.0,
                    left: 2.0,
                  ),
                  child: Text(
                    'TELEPHONE',
                    style: TextStyle(
                        fontSize: 15.0,
                        letterSpacing: 0.4,
                        color: Color(0XFF339933)),
                  ),
                ),
              ],
            ),
            SizedBox(height: _height * 0.01),
            textField(telephoneController, 'Telephone Number', 'n',
                CupertinoIcons.phone_fill),
          ],
        ),
        isActive: currentStep >= 1,
        state: currentStep > 1 ? StepState.complete : StepState.editing,
      ),
      Step(
        title: Text('Register'),
        state: currentStep > 2 ? StepState.complete : StepState.editing,
        isActive: currentStep == 2,
        content: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
              horizontal: _width * 0.01, vertical: _height * 0.01),
          color: Colors.white,
          child: Column(
            children: [
              Container(
                height: _height * 0.19,
                width: _width * 0.38,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.grey[200],
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/images/register.svg",
                    height: _height * 0.13,
                    width: _width * 0.27,
                    // color: Colors.grey[800],
                  ),
                ),
              ),
              SizedBox(height: _height * 0.005),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 15.0,
                  vertical: 20.0,
                ),
                child: Text(
                  'Please enter the One-Time Passcode sent to you by SMS, in order to complete registration',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.black54,
                  ),
                ),
              ),
              SizedBox(height: _height * 0.02),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: pin_input(pinController),
              ),
            ],
          ),
        ),
      ),
    ];

    _goTo(int step) {
      print('moving to $step');
      setState(() {
        currentStep = step;
      });
    }

    _next(int index) {
      setState(() {
        _continuePressed = true;
      });
      switch (index) {
        case 0:
          FocusScope.of(context).requestFocus(FocusNode());
          if (customerNumber.text.isNotEmpty) _validateCustomer();
          print('validating customer');
          break;
        case 1:
          if (idNumberController.text.isNotEmpty &&
              telephoneController.text.isNotEmpty &&
              _dateOfBirth != null) _confirmCustomerDetails();
          print('confirming customer details');
          break;
        case 2:
          if (pinController.text.isNotEmpty) _validateOTPAndRegister();
          print('validating and registering');
          break;
        default:
      }
    }

    _cancel() {
      if (currentStep > 0) {
        setState(() {
          print('setting currentStep to ${currentStep - 1}');
          currentStep = currentStep - 1;
        });
      }
    }

    _buildContinueButtonText(int index) {
      String title;
      switch (index) {
        case 0:
          title = 'Validate';
          break;
        case 1:
          title = 'Confirm';
          break;
        case 2:
          title = 'Register';
          break;
        default:
      }
      return title.toUpperCase();
    }

    return SafeArea(
      child: Scaffold(
        key: enroll,
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text('Enroll'),
        ),
        body: SingleChildScrollView(
          reverse: true,
          // padding:
          //     EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Container(
            height: _height,
            color: Colors.white,
            padding: EdgeInsets.only(
              left: _width * 0.035,
              right: _width * 0.035,
              top: _height * 0.105,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Stepper(
                    controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                        VoidCallback onStepCancel}) {
                      return Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 36.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    // child: null,
                                    ),
                                RaisedButton(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 20.0),
                                  // onPressed: () => print('going forward'),
                                  onPressed: onStepContinue,
                                  color: Color(0XFF339933),
                                  child: def.getIsLoading
                                      ? Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(6.0),
                                            child: CircularProgressIndicator(
                                              backgroundColor:
                                                  Color(0XFF339933),
                                            ),
                                          ),
                                        )
                                      : Text(
                                          _buildContinueButtonText(currentStep),
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    },
                    type: StepperType.horizontal,
                    steps: steps,
                    currentStep: currentStep,
                    onStepContinue: () => _next(currentStep + 1),
                    onStepCancel: () => _cancel(),
                    onStepTapped: _goTo,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
