import 'dart:io';

import 'package:akrbankios/Auth/screens/ForgotPassword.dart';
import 'package:akrbankios/Auth/screens/enroll_screen.dart';
import 'package:akrbankios/Providers/default.dart';
import 'package:akrbankios/Providers/phone_info.dart';
import 'package:akrbankios/controllers/auth_controller.dart';
import 'package:device_info/device_info.dart';
import 'package:get_ip/get_ip.dart';
import 'package:provider/provider.dart';

import 'package:akrbankios/common_screens/AboutPage.dart';
import 'package:akrbankios/common_widgets/bottom_nav.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:akrbankios/Services/fingerprint.dart';

import 'package:akrbankios/common_screens/Help.dart';

class LoginPage extends StatefulWidget {
  final String name;

  const LoginPage({Key key, this.name}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  double _height;
  double _width;
  var login = new GlobalKey<ScaffoldState>();

  bool _isHidden = true;
  TextEditingController _password = new TextEditingController();
  TextEditingController _username;
  bool passwordActive = false;
  bool hidePassword = true;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  Future<bool> bioCheck;
  bool _hasBiometric = true;
  FocusNode myFocusNode;
  var deviceId;
  var deviceIp;
  var model;
  var manufacturer;
  var brand;
  var country = "Ghana";
  String deviceOS;
  var appVersion;
  String fbcmToken;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;

  _getAddressFromLatLng() async {
    final _phoneInfo = Provider.of<PhoneInfo>(context, listen: false);

    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        country = place.country != null ? place.country : country;
        _phoneInfo.setcountry(place.country ?? country);
        print('country: $country');
      });
    } catch (e) {
      print('LatLng Error = ${e.toString()}');
    }
  }

  _getCurrentLocation() async {
    try {
      final position = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      setState(() {
        _currentPosition = position;
      });
      _getAddressFromLatLng();
    } catch (e) {
      print('Location Error : ${e.toString()}');
    }
  }

  Future<Null> getPhoneinfo() async {
    String ipAddress = await GetIp.ipAddress;
    final _phoneInfo = Provider.of<PhoneInfo>(context, listen: false);

    deviceIp = ipAddress;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      if (androidInfo == null) {
        print('cannot get Android Info');
      } else {
        deviceId = androidInfo.androidId;
        model = androidInfo.model;
        manufacturer = androidInfo.manufacturer;
        brand = androidInfo.brand;
        deviceOS = "A";
      }
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceId = iosInfo.identifierForVendor;
      model = iosInfo.model;
      manufacturer = iosInfo.name;
      brand = iosInfo.systemVersion;
      deviceOS = "I";
    }
    _phoneInfo.setbrand(brand);
    _phoneInfo.setdeviceID(deviceId);
    _phoneInfo.setdeviceOs(deviceOS);
    _phoneInfo.setmodel(model);
    _phoneInfo.setmanufacturer(manufacturer);
    _phoneInfo.setdeviceIp(deviceIp);
    setState(() {
      appVersion = _phoneInfo.getappVersion;
      fbcmToken = _phoneInfo.getfbcmToken;
    });
  }

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  void initState() {
    super.initState();
    checkBiometrics().then((value) {
      setState(() {
        _hasBiometric = value;
      });
    });
    _getCurrentLocation();
    getPhoneinfo();
    if (widget.name == null || widget.name == '') {
      _username = new TextEditingController();
    } else {
      _username = new TextEditingController(text: widget.name);
    }
  }

  @override
  Widget build(BuildContext context) {
    final def = Provider.of<Default>(context);
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: Scaffold(
          key: login,
          resizeToAvoidBottomPadding: false,
//      backgroundColor: Color(0XFF339933),
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
//          Image.asset("assets/loginbg.png"),
              Image(
                image: AssetImage("assets/images/loginbg.png"),
                fit: BoxFit.cover,
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(
                    right: _width * 0.05,
                    left: _width * 0.05,
                    top: _height * 0.2,
                  ),
                  padding: EdgeInsets.symmetric(
                      horizontal: _width * 0.04, vertical: _height * 0.045),
                  width: MediaQuery.of(context).size.width,
                  height: _height * 0.43,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      // Text(
                      // //   'Login',
                      // //   style: TextStyle(
                      // //       fontSize: 22,
                      // //       fontWeight: FontWeight.w600,
                      // //       color: Colors.grey[700]),
                      // // ),
                      // // SizedBox(height: _height * 0.04),
                      Container(
                        padding: const EdgeInsets.only(
                          right: 10.0,
                          left: 20.0,
                          top: 6.0,
                        ),
                        height: _height * 0.068,
                        width: _width * 0.82,
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.all(Radius.circular(6.0)),
                        ),
                        child: TextFormField(
                          controller: _username,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "User ID",
                            hintStyle: TextStyle(
                                color: Colors.grey[500], fontSize: 15.0),
                            suffixIcon: Icon(Icons.person),
                          ),
                        ),
                      ),
                      SizedBox(height: _height * 0.03),
                      Container(
                        // padding: const EdgeInsets.only(top: 20, right: 30, left: 31),
                        width: _width * 0.82,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.only(
                                right: 10.0,
                                left: 20.0,
                                top: 6.0,
                              ),
                              height: _height * 0.068,
                              width: _hasBiometric
                                  ? _width * 0.67
                                  : _width * 0.815,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0)),
                              ),
                              child: TextFormField(
                                controller: _password,
                                obscureText: hidePassword,
                                onChanged: (value) {
                                  if (value != '') {
                                    setState(() {
                                      passwordActive = true;
                                    });
                                  } else {
                                    passwordActive = false;
                                  }
                                },
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Password",
                                  hintStyle: TextStyle(
                                      color: Colors.grey[500], fontSize: 15.0),
                                  suffixIcon: passwordActive
                                      ? GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              hidePassword = !hidePassword;
                                            });
                                          },
                                          child: Icon(
                                            hidePassword
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            size: 24.0,
                                          ),
                                        )
                                      : Icon(
                                          Icons.lock,
                                          size: 24.0,
                                        ),
                                ),
                              ),
                            ),
                            SizedBox(
                                width: _hasBiometric
                                    ? _width * 0.001
                                    : _width * 0.0),
                            _hasBiometric
                                ? IconButton(
                                    icon: Icon(Icons.fingerprint),
                                    iconSize: _width * 0.11,
                                    color: Color(0XFF339933),
                                    onPressed: () async {
                                      final authenticated =
                                          await authenticate();
                                      if (authenticated) {
                                        AuthController.bioLoginController(
                                          context: context,
                                          key: login,
                                          username: _username.text,
                                          deviceID: deviceId,
                                          deviceIP: deviceIp,
                                          model: model,
                                          manufacturer: manufacturer,
                                          brand: brand,
                                          country: country,
                                          os: deviceOS,
                                          appVersion: appVersion,
                                          fbcmToken: fbcmToken,
                                        );
                                      }
                                    },
                                  )
                                : Text(''),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 15.0, bottom: 25.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ForgotPassword(),
                                  ),
                                );
                              },
                              child: Text(
                                "Forgot password?",
                                style: TextStyle(
                                    color: Color(0XFFFFCC00), fontSize: 13.0),
                              ),
                            ),
                            Text(
                              "Fast Balance",
                              style: TextStyle(
                                  color: Color(0XFFFFCC00), fontSize: 13.0),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: _height * 0.01),
                      Container(
                        width: _width * 0.81,
                        height: _height * 0.06,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(6.0))),
                          child: def.getIsLoading
                              ? Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: CircularProgressIndicator(
                                      backgroundColor: Color(0XFF339933),
                                    ),
                                  ),
                                )
                              : Text(
                                  "LOGIN",
                                  style: TextStyle(
                                      color: Color(0XFF339933), fontSize: 15),
                                ),
//                          color: Colors.yellow[600],
                          color: Color(0XFFFFCC00),
                          onPressed: () {
                            // Navigator.push(context,
                            //     MaterialPageRoute(builder: (_) => BottomNav()));
                            AuthController.loginController(
                              context: context,
                              key: login,
                              username: _username.text,
                              password: _password.text,
                              deviceId: deviceId,
                              deviceIP: deviceIp,
                              model: model,
                              manufacturer: manufacturer,
                              brand: brand,
                              country: country,
                              deviceOS: deviceOS,
                              appVersion: appVersion,
                              fbcmToken: fbcmToken,
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                top: _height * 0.11,
                left: _width * 0.3,
                width: _width * 0.4,
                height: _height * 0.2,
                child: Image(
                  image: AssetImage("assets/images/akrblogo_big.png"),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    color: Color(0XFF339933),
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AboutUs()));
                          },
                          child: Text(
                            "ABOUT US",
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 20,
                          color: Colors.white,
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Help()));
                          },
                          child: Text(
                            "HELP ",
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                        Container(
                          width: 1,
                          height: 20,
                          color: Colors.white,
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EnrollScreen()));
                          },
                          child: Text(
                            "SELF ENROLL",
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
