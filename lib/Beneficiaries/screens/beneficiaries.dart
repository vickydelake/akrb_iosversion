import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:random_color/random_color.dart';
import 'package:provider/provider.dart';
import 'package:akrbankios/common_widgets/enums/beneficiary_type.dart';
import 'package:akrbankios/models/beneficiary.dart';
import 'package:akrbankios/models/userModel.dart';
import 'package:akrbankios/controllers/bene_controller.dart';
import 'create_beneficiary.dart';
import 'package:akrbankios/common_widgets/uidata.dart';

class InternalBene extends StatefulWidget {
  final User user;

  const InternalBene({Key key, this.user}) : super(key: key);
  @override
  _InternalBeneState createState() => _InternalBeneState();
}

class _InternalBeneState extends State<InternalBene> {
  final GlobalKey<ScaffoldState> internalBene = GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  Future<List<MbBeneficiary>> beneficiaryFuture;
  bool _isLoading = false;
  double _height;
  double _width;

  @override
  void initState() {
    super.initState();
    beneficiaryFuture = BeneController.getBeneController(
      authToken: widget.user.tokenId,
      context: context,
      key: internalBene,
    );
  }

  _createInternalBeneficiary(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => CreateBeneficiary(
          user: widget.user,
          onBeneficiaryCreated: () {
            setState(() {
              beneficiaryFuture = BeneController.getBeneController(
                  authToken: widget.user.tokenId);
            });
          },
        ),
      ),
    );
  }

  _editInternalBeneficiary(BuildContext context, String accName, String accNum,
      String phoneNumber, String code) {
    List<String> beneficiaryDetails = [accName, accNum, phoneNumber, code];
    FocusScope.of(context).requestFocus(FocusNode());

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => CreateBeneficiary(
          beneficiaryDetails: beneficiaryDetails,
          onBeneficiaryCreated: () {
            setState(() {
              beneficiaryFuture = BeneController.getBeneController(
                authToken: widget.user.tokenId,
                context: context,
                key: internalBene,
              );
            });
          },
        ),
      ),
    );
  }

  _confirmDeletion(String code, String name) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
              title: Text(
                'Confirm Deletion',
              ),
              content: Text(
                'Are you sure you want to delete $name?',
                style: TextStyle(
                  color: Colors.black54,
                ),
                // textAlign: TextAlign.justify,
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    'CANCEL',
                    style: TextStyle(
                      color: UIData.appPrimaryColor,
                      fontSize: 15.0,
                      // fontWeight: FontWeight.w700,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text(
                    'DELETE',
                    style: TextStyle(
                      color: UIData.appPrimaryColor,
                      fontSize: 15.0,
                      // fontWeight: FontWeight.w700,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _deleteBeneficiary(context, code);
                  },
                ),
              ],
            ));
  }

  _deleteBeneficiary(BuildContext context, String code) async {
    Map<String, String> details = {
      "beneficiaryCode": code,
      "authToken": widget.user.tokenId,
    };
    await BeneController.deleteBeneController(details);
  }

  Future<void> _refresh() {
    setState(() {
      beneficiaryFuture =
          BeneController.getBeneController(authToken: widget.user.tokenId);
    });
    return beneficiaryFuture;
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Theme(
      data: Theme.of(context).copyWith(canvasColor: Colors.white),
      child: Scaffold(
        key: internalBene,
        backgroundColor: Colors.white,
        appBar: AppBar(
          brightness: Brightness.dark,
          title: Text(
            'Internal Beneficiaries',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          elevation: 0,
          actions: Platform.isIOS
              ? [
                  IconButton(
                    icon: Icon(
                      Icons.group_add,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      _createInternalBeneficiary(context);
                    },
                  )
                ]
              : [
                  FlatButton.icon(
                    label: Text(
                      'CREATE',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    icon: Icon(
                      Icons.group_add,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      _createInternalBeneficiary(context);
                    },
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  )
                ],
        ),
        body: FutureBuilder<List<MbBeneficiary>>(
            future: beneficiaryFuture,
            // initialData: ,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting &&
                  snapshot.data == null) {
                return Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SpinKitCircle(
                        color: Colors.blue,
                      ),
                    ],
                  ),
                );
              }

              if (snapshot.hasError) {
                final error = snapshot.error as PlatformException;
                print('error code is: ${error.code}');

                return Container(
                  width: double.infinity,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          UIData.emptyImage,
                          fit: BoxFit.scaleDown,
                        ),
                        Text(
                          error.message,
                          style: TextStyle(
                            // color: Colors.white,
                            fontSize: 18.0,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        if (error.code != '12') ...[
                          FlatButton(
                              child: Text('RETRY'),
                              textColor: Colors.white,
                              color: UIData.appPrimaryColor,
                              onPressed: () {
                                setState(() {
                                  final user =
                                      Provider.of<User>(context, listen: false);
                                  beneficiaryFuture =
                                      BeneController.getBeneController(
                                          authToken: user.tokenId,
                                          context: context,
                                          key: internalBene);
                                });
                              }),
                        ],
                      ]),
                );
              }
              var beneficiaries = snapshot.data;
              beneficiaries?.sort((a, b) => a.name.compareTo(b.name));

              return Container(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: RefreshIndicator(
                        key: _refreshIndicatorKey,
                        onRefresh: _refresh,
                        child: AnimationLimiter(
                          child: ListView.builder(
                            itemCount: beneficiaries.length,
                            itemBuilder: (context, index) {
                              Color _color = RandomColor().randomColor(
                                colorHue: ColorHue.green,
                                colorSaturation:
                                    ColorSaturation.mediumSaturation,
                                colorBrightness: ColorBrightness.light,
                              );

                              final beneficiary = beneficiaries[index];
                              return AnimationConfiguration.staggeredList(
                                position: index,
                                duration: const Duration(milliseconds: 375),
                                child: SlideAnimation(
                                  verticalOffset: 50.0,
                                  child: FadeInAnimation(
                                    child: Slidable(
                                      actionPane: SlidableDrawerActionPane(),
                                      actionExtentRatio: 0.18,
                                      showAllActionsThreshold: 0.1,
                                      child: ListTile(
                                        leading: CircleAvatar(
                                          backgroundColor: _color,
                                          child: Text(
                                              '${beneficiary.name.substring(0, 1).toUpperCase()}'),
                                          foregroundColor: Colors.white,
                                        ),
                                        title: Text(
                                          beneficiary.name.toUpperCase(),
                                          // style: TextStyle(color: UIData.appPrimaryColor),
                                        ),
                                        subtitle: Text(
                                          beneficiary.account,
                                        ),
                                        contentPadding: EdgeInsets.symmetric(
                                          horizontal: 8.0,
                                          vertical: 6.0,
                                        ),
                                      ),
                                      secondaryActions: <Widget>[
                                        IconSlideAction(
                                            caption: 'Delete',
                                            color: Colors.red,
                                            icon: Icons.delete,
                                            onTap: () {
                                              print(
                                                  'Delete ${beneficiary.name}');
                                              _confirmDeletion(beneficiary.code,
                                                  beneficiary.name);
                                            }),
                                        IconSlideAction(
                                          caption: 'Edit',
                                          color: Colors.black45,
                                          icon: Icons.edit,
                                          onTap: () {
                                            print('Edit ${beneficiary.name}');
                                            _editInternalBeneficiary(
                                                context,
                                                beneficiary.name.toUpperCase(),
                                                beneficiary.account,
                                                beneficiary.telephone,
                                                beneficiary.code);
                                          },
                                        ),
                                        IconSlideAction(
                                            caption: 'Transfer',
                                            color: Colors.blue,
                                            icon: Icons.send,
                                            onTap: () {
                                              print(
                                                  'Transfer Funds ${beneficiary.name}');
                                            }),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                    _isLoading ? LinearProgressIndicator() : Container()
                  ],
                ),
              );
            }),
      ),
    );
  }
}
