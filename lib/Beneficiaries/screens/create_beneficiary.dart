import 'package:akrbankios/controllers/bene_controller.dart';
import 'package:easy_contact_picker/easy_contact_picker.dart';
import 'package:flutter/services.dart';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/common_widgets/enums/beneficiary_type.dart';
// import 'package:utb_mbank/models/banks.dart';
import 'package:akrbankios/models/userModel.dart';
import 'package:akrbankios/services/permission_service.dart';
import 'package:akrbankios/common_widgets/uidata.dart';

// import '../../widgets/common/selection_modal.dart';

class CreateBeneficiary extends StatefulWidget {
  final User user;
  final List<String> beneficiaryDetails;
  final Function onBeneficiaryCreated;

  const CreateBeneficiary(
      {Key key, this.beneficiaryDetails, this.onBeneficiaryCreated, this.user})
      : super(key: key);

  @override
  _CreateBeneficiaryState createState() => _CreateBeneficiaryState();
}

class _CreateBeneficiaryState extends State<CreateBeneficiary> {
  double _height;
  double _width;
  TextEditingController nameController;
  TextEditingController accountNumController;
  TextEditingController phoneNumController;

  final FocusNode accountNumNode = FocusNode();
  final FocusNode phoneNumNode = FocusNode();

  // final NativeContactPicker _contactPicker = new NativeContactPicker();
  final EasyContactPicker _contactPicker = EasyContactPicker();

  Contact _contact;
  String _phoneNumber;
  bool _isLoading = false;
  bool _submitted = false;
  String _accountName = "";

  bool get showBeneficiaryNameErrorText =>
      _submitted && nameController.text.isEmpty;

  bool isAccNumValid = false;

  @override
  void dispose() {
    super.dispose();
    accountNumController.dispose();
    nameController.dispose();
    phoneNumController.dispose();
    accountNumNode.dispose();
  }

  @override
  void initState() {
    super.initState();

    accountNumNode.addListener(() {
      if (accountNumNode.hasFocus || isAccNumValid == true) {
        return;
      } else {
        if (accountNumController.text.trim().isNotEmpty) {
          _getAccountName(accountNumController.text.trim());
        }
      }
    });

    if (widget.beneficiaryDetails == null) {
      nameController = TextEditingController();
      accountNumController = TextEditingController();
      phoneNumController = TextEditingController();
    } else {
      nameController =
          TextEditingController(text: widget.beneficiaryDetails.elementAt(0));
      accountNumController =
          TextEditingController(text: widget.beneficiaryDetails.elementAt(1));
      phoneNumController =
          TextEditingController(text: widget.beneficiaryDetails.elementAt(2));
    }
  }

  _checkPermissionAndSelectContact() async {
    final permissionService = Provider.of<PermissionsService>(context);
    final contactsPermissionGranted =
        await permissionService.hasContactsPermission();
    if (contactsPermissionGranted) {
      Contact contact = await _contactPicker.selectContactWithNative();
      String _contactNum;
      print(contact.fullName);
      if (contact.phoneNumber.contains('+')) {
        _contactNum = contact.phoneNumber.replaceAll('+', '');
      } else {
        _contactNum = contact.phoneNumber;
      }

      setState(() {
        _contact = contact;
        _phoneNumber = _contactNum;
        phoneNumController.text = _phoneNumber;
      });
    } else {
      await permissionService.requestContactsPermission(
          onPermissionDenied: () => print('Permission Denied'));
    }

    // if (requestContactPermission) {
    //   try {
    //     Contact contact = await _contactPicker.selectContactWithNative();
    //     print(contact.fullName);
    //     // setState(() {
    //     //   _contact = contact;
    //     //   phoneNumber = _contact.phoneNumber;
    //     //   phoneNumController.text = phoneNumber;
    //     // });
    //   } catch (e) {
    //     print(e);
    //   }
    // }
  }

  Future<void> _getAccountName(String accNum) async {
    Map<String, String> details = {
      "accountNumber": accNum,
      "authToken": widget.user.tokenId,
    };

    String response = await BeneController.getAcctNameController(details);

    if (response != null) {
      setState(() {
        isAccNumValid = true;
        _accountName = response;
      });
    } else {
      print(response);
    }
  }

  Future<void> _createBeneficiary() async {
    Map<String, String> details = {
      "accountNumber": accountNumController.text.trim(),
      "accountName": nameController.text.trim(),
      "telephone": phoneNumController.text.trim(),
      "authToken": widget.user.tokenId,
    };
    details.forEach((k, v) => print('$k : $v'));

    await BeneController.createBeneController(details);
  }

  Future<void> _editBeneficiary() async {
    Map<String, String> details = {
      "accountNumber": accountNumController.text.trim(),
      "accountName": nameController.text.trim(),
      "telephone": phoneNumController.text.trim(),
      "beneficiaryCode": widget.beneficiaryDetails.elementAt(3),
      "authToken": widget.user.tokenId,
    };

    await BeneController.editBeneController(details);
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    String _errorText() {
      String text;
      if (_submitted && accountNumController.text.isEmpty) {
        text = 'Account Number can\'t be empty';
        return text;
      } else if (_submitted && isAccNumValid == false) {
        text = 'Account Number has not been verified';
        return text;
      } else {
        return null;
      }
    }

    Text _buildTitle(List<String> beneficiaryDetails) {
      Text title;
      switch (beneficiaryDetails == null) {
        case true:
          title = Text('Create internal beneficiary');
          break;
        case false:
          title = Text('Edit internal beneficiary');
          break;
        default:
      }
      return title;
    }

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      behavior: HitTestBehavior.opaque,
      child: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.white,
        ),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 8.0,
            title: _buildTitle(widget.beneficiaryDetails),
          ),
          body: Container(
            padding: EdgeInsets.all(20.0),
            // color: Colors.white10,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Text(
                            'ALIAS NAME',
                            style: TextStyle(
                                fontSize: 15.0, color: Color(0XFF339933)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: _height * 0.01),
                    textField(
                        nameController, 'Enter alias name', 't', Icons.person),
                    SizedBox(height: _height * 0.02),
                    // Container(
                    //   margin:
                    //       EdgeInsets.only(left: 2.0, right: 2.0, bottom: 5.0),
                    //   child: TextField(
                    //     textAlign: TextAlign.start,
                    //     autofocus: false,
                    //     key: Key('name'),
                    //     textInputAction: TextInputAction.next,
                    //     style: TextStyle(
                    //       letterSpacing: 0.5,
                    //       // fontWeight: FontWeight.bold,
                    //       color: UIData.appPrimaryColor,
                    //       fontSize: 16.0,
                    //     ),
                    //     keyboardType: TextInputType.text,
                    //     controller: nameController,
                    //     inputFormatters: [
                    //       WhitelistingTextInputFormatter(
                    //           RegExp(r"[a-zA-Z]+|\s"))
                    //     ],
                    //     decoration: InputDecoration(
                    //       errorText: showBeneficiaryNameErrorText
                    //           ? 'Beneficiary Alias can\'t be empty'
                    //           : null,
                    //       focusedBorder: UnderlineInputBorder(
                    //         borderSide: BorderSide(
                    //             color: Color(0XFF339933), width: 1.5),
                    //       ),
                    //       enabledBorder: UnderlineInputBorder(
                    //         borderSide: BorderSide(
                    //             color: Color(0XFF339933).withOpacity(0.7),
                    //             width: 1.0),
                    //       ),
                    //     ),
                    //     onEditingComplete: () {
                    //       FocusScope.of(context).requestFocus(accountNumNode);
                    //     },
                    //   ),
                    // ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Text(
                            'ACCOUNT NUMBER',
                            style: TextStyle(
                                fontSize: 15.0, color: Color(0XFF339933)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: _height * 0.01),
                    textField(accountNumController, 'Account Number', 'n',
                        Icons.account_box_rounded),
                    // Container(
                    //   margin:
                    //       EdgeInsets.only(left: 2.0, right: 2.0, bottom: 5.0),
                    //   child: TextField(
                    //       textAlign: TextAlign.start,
                    //       key: Key('accNum'),
                    //       focusNode: accountNumNode,
                    //       textInputAction: TextInputAction.next,
                    //       style: TextStyle(
                    //         letterSpacing: 0.5,
                    //         // fontWeight: FontWeight.bold,
                    //         color: Color(0XFF339933),
                    //         fontSize: 16.0,
                    //       ),
                    //       onChanged: (value) {
                    //         if (value.isNotEmpty) {
                    //           setState(() {
                    //             isAccNumValid = false;
                    //             _accountName = '';
                    //           });
                    //         }
                    //       },
                    //       keyboardType: TextInputType.number,
                    //       controller: accountNumController,
                    //       inputFormatters: [
                    //         WhitelistingTextInputFormatter(RegExp(r"[0-9]+"))
                    //       ],
                    //       decoration: InputDecoration(
                    //         errorText: _errorText(),
                    //         focusedBorder: UnderlineInputBorder(
                    //           borderSide: BorderSide(
                    //               color: Color(0XFF339933), width: 1.5),
                    //         ),
                    //         enabledBorder: UnderlineInputBorder(
                    //           borderSide: BorderSide(
                    //               color: Color(0XFF339933).withOpacity(0.7),
                    //               width: 1.0),
                    //         ),
                    //       ),
                    //       onEditingComplete: () {
                    //         FocusScope.of(context).unfocus();
                    //       }),
                    // ),
                    _accountName.isEmpty
                        ? Container()
                        : Card(
                            margin: EdgeInsets.symmetric(vertical: 10.0),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 8.0,
                                vertical: 10.0,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Text(
                                    'ACCOUNT NAME',
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      color: Color(0XFF339933),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    _accountName,
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.black,
                                    ),
                                    // textAlign: TextAlign.end,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ),
                            ),
                          ),
                    SizedBox(height: _height * 0.02),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Text(
                            'PHONE NUMBER',
                            style: TextStyle(
                                fontSize: 15.0, color: Color(0XFF339933)),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: _height * 0.01),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          // child: Container(
                          //   margin: EdgeInsets.only(
                          //       left: 2.0, right: 2.0, bottom: 5.0),
                          //   child: TextField(
                          //     textAlign: TextAlign.start,
                          //     key: Key('phoneNum'),
                          //     focusNode: phoneNumNode,
                          //     textInputAction: TextInputAction.done,
                          //     style: TextStyle(
                          //       letterSpacing: 0.5,
                          //       // fontWeight: FontWeight.bold,
                          //       color: Color(0XFF339933),
                          //       fontSize: 16.0,
                          //     ),
                          //     keyboardType: TextInputType.number,
                          //     controller: phoneNumController,
                          //     inputFormatters: [
                          //       WhitelistingTextInputFormatter(
                          //           RegExp(r"[0-9]+"))
                          //     ],
                          //     decoration: InputDecoration(
                          //       focusedBorder: UnderlineInputBorder(
                          //         borderSide: BorderSide(
                          //             color: Color(0XFF339933), width: 1.5),
                          //       ),
                          //       enabledBorder: UnderlineInputBorder(
                          //         borderSide: BorderSide(
                          //             color: Color(0XFF339933).withOpacity(0.7),
                          //             width: 1.0),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                          child: textField(
                            phoneNumController,
                            'Phone Number',
                            'n',
                            Icons.phone,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              left: 2.0, right: 2.0, bottom: 5.0),
                          child: IconButton(
                              icon: Icon(
                                Icons.contacts,
                                size: 28.0,
                              ),
                              alignment: Alignment.center,
                              color: Color(0XFF339933),
                              onPressed: () {
                                _checkPermissionAndSelectContact();
                              }),
                        )
                      ],
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(bottom: _height * 0.05),
                  decoration: BoxDecoration(
                    color: Colors.green.shade400,
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  width: double.infinity,
                  child: _isLoading
                      ? Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(7.0),
                                child: SpinKitCircle(
                                  color: Colors.white,
                                  size: 32.0,
                                ),
                              ),
                              // CircularProgressIndicator(
                              //   valueColor:
                              //       AlwaysStoppedAnimation(Colors.white),
                              // ),
                            ])
                      : FlatButton(
                          // color: UIData.appPrimaryColor,
                          padding: EdgeInsets.all(12.0),
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          child: Text(
                            widget.beneficiaryDetails == null
                                ? 'ADD BENEFICIARY'
                                : 'EDIT BENEFICIARY',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            ),
                          ),
                          onPressed: () {
                            print('pressed!');
                            FocusScope.of(context).requestFocus(FocusNode());
                            setState(() {
                              _submitted = true;
                            });
                            if (nameController.text.isNotEmpty &&
                                accountNumController.text.trim().isNotEmpty &&
                                // accountNumController.text.trim().length ==
                                //     18 &&
                                _submitted) {
                              if (widget.beneficiaryDetails == null) {
                                if (isAccNumValid) {
                                  Utilities.showInfoDialog(
                                      context,
                                      'Confirm account name',
                                      'Do you want to add $_accountName as a beneficiary?',
                                      'CONFIRM', () {
                                    Navigator.of(context).pop();
                                    _createBeneficiary();
                                  });
                                } else {
                                  return;
                                }
                              } else {
                                if (isAccNumValid) {
                                  _editBeneficiary();
                                } else {
                                  return;
                                }
                              }
                            } else {
                              return;
                            }
                          },
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget textField(controller, description, type, icon) {
    return Column(
      children: [
        Container(
          width: _width * 0.9,
          padding:
              EdgeInsets.only(left: 20.0, right: 8.0, top: 6.0, bottom: 6.0),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(8.0),
            ),
          ),
          child: TextField(
            keyboardType: type == 'n'
                ? TextInputType.number
                : type == 'e'
                    ? TextInputType.emailAddress
                    : TextInputType.text,
            controller: controller,
            onChanged: (value) {
              if (value != '') {
                print('');
              } else {
                print('');
              }
            },
            style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: description,
              hintStyle: TextStyle(color: Colors.grey[400]),
              // errorText: '',
              suffixIcon: Icon(icon),
            ),
          ),
        ),
      ],
    );
  }
}
