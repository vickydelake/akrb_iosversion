import 'package:flutter/material.dart';

class Transfers extends StatefulWidget {
  @override
  _TransfersState createState() => _TransfersState();
}

class _TransfersState extends State<Transfers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text("Transfers",
              style: TextStyle(color: Colors.white, fontSize: 18))),
    );
  }
}
