import 'dart:math';

import 'package:akrbankios/APIs/auth_api.dart';
import 'package:akrbankios/Auth/screens/FirstTimeLogin.dart';
import 'package:akrbankios/Providers/default.dart';
import 'package:akrbankios/Providers/user_info.dart';
import 'package:akrbankios/Services/internetCheck.dart';
import 'package:akrbankios/Services/localStorage.dart';
import 'package:akrbankios/Services/serviceLocator.dart';
import 'package:akrbankios/common_widgets/bottom_nav.dart';
import 'package:akrbankios/models/security_question.dart';
import 'package:akrbankios/models/userModel.dart';
import 'package:akrbankios/models/validated_customer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void _inform(
    GlobalKey<ScaffoldState> key, BuildContext context, String message) {
  key.currentState.showSnackBar(SnackBar(
    // width: MediaQuery.of(context).size.width * 0.88,
    padding: EdgeInsets.all(6.0),
    duration: Duration(milliseconds: 2000),
    backgroundColor: Colors.black.withOpacity(0.85),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(1.0),
      ),
    ),
    content: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          Icons.cancel,
          size: 28.0,
          color: Colors.red[200],
        ),
        SizedBox(width: 50),
        Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 17.0,
            letterSpacing: 0.7,
            fontWeight: FontWeight.w400,
            color: Colors.white,
          ),
        ),
      ],
    ),
  ));
}

class TransfersController {
  static LocalStorageService storageService = locator<LocalStorageService>();

  static Future pinSetupController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String pin,
    String confirmPin,
    String token,
    uniqueId,
    model,
    manufacturer,
    brand,
    country,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (pin != null &&
            pin.isNotEmpty &&
            confirmPin != null &&
            confirmPin.isNotEmpty) {
          if (confirmPin == pin) {
            var response = await Auth.pinSetup(
              pin,
              token,
              uniqueId,
              model,
              manufacturer,
              brand,
              country,
            );

            print(response);
            return response;
          } else {
            _inform(key, context, 'PINs must match');
          }
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future forgotPasswordController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String username,
    String password,
    String ans,
    String ques,
    String deviceId,
    deviceIp,
    model,
    manufacturer,
    brand,
    country,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        //  final login =
        var user = await Auth.forgetpassword(
          username,
          password,
          ans,
          ques,
          deviceId,
          deviceIp,
          model,
          manufacturer,
          brand,
          country,
        );
        //  _user.setUser(login);

        print("login successful");
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BottomNav(),
          ),
        );
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      print('not loading');
      def.setLoading(false);
    }
  }

  static Future loginController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String username,
    String password,
    String deviceId,
    String deviceIP,
    String model,
    String manufacturer,
    String brand,
    String country,
    String deviceOS,
    String appVersion,
    String fbcmToken,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        print('username = $username');
        print('password = $password');
        print('deviceId = $deviceId');
        print('deviceIP = $deviceIP');
        print('model = $model');
        print('manufacturer = $manufacturer');
        print('brand= $brand');
        print('country = $country');
        print('deviceOS = $deviceOS');
        print('appVersion = $appVersion');
        print('fbcmToken = $fbcmToken');

        if (username != null &&
            username.isNotEmpty &&
            password != null &&
            password.isNotEmpty) {
          var user = await Auth.login(
            username,
            password,
            deviceId,
            deviceIP,
            model,
            manufacturer,
            brand,
            country,
            deviceOS,
            appVersion,
            fbcmToken,
          );
          // _user.setInfo(user);

          print("login successful == [\n$user\n]");

          if (user != null) {
            if (user.changePass == true) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BottomNav(user: user),
                ),
              );
            } else {
              storageService.isLoggedIn = true;
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BottomNav(user: user),
                ),
              );
            }
          }
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      print('Error Message = ${e.message}');
      _inform(key, context, e.message);
    } finally {
      print('Error = ${e.toString()}');
      def.setLoading(false);
    }
  }

  static Future bioLoginController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String username,
    String deviceID,
    String deviceIP,
    String model,
    String manufacturer,
    String brand,
    String country,
    String os,
    String appVersion,
    String fbcmToken,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (username != null && username.isNotEmpty) {
          var user = await Auth.biologin(
            username,
            deviceID,
            deviceIP,
            model,
            manufacturer,
            brand,
            country,
            os,
            appVersion,
            fbcmToken,
          );
          _user.setInfo(user);

          print("login successful");
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => BottomNav(),
            ),
          );
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future passSetupController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String username,
    String password,
    String confirmPassword,
    String ans,
    String ques,
    String deviceId,
    deviceIp,
    model,
    manufacturer,
    brand,
    country,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (password != null &&
            password.isNotEmpty &&
            confirmPassword != null &&
            confirmPassword.isNotEmpty) {
          if (confirmPassword == password) {
            var response = await Auth.passSetup(
              username,
              password,
              ans,
              ques,
              deviceId,
              deviceIp,
              model,
              manufacturer,
              brand,
              country,
            );
            print(response);
            return response;
          } else {
            _inform(key, context, 'Passwords must match');
          }
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      print("Pass Error = ${e.message}");
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future changePassController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String token,
    String oldpassword,
    String newpassword,
    String ans,
    String deviceIP,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        //  final login =
        var user = await Auth.changePassword(
          token,
          oldpassword,
          newpassword,
          ans,
          deviceIP,
        );
        //  _user.setUser(login);

        print("login successful");
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BottomNav(),
          ),
        );
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future activateBioController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String token,
    String secPin,
    String uniqueID,
    String ans,
    String deviceIP,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        //  final login =
        var user = await Auth.activateBiometric(
          token,
          secPin,
          uniqueID,
          ans,
          deviceIP,
        );
        //  _user.setUser(login);

        print("login successful");
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BottomNav(),
          ),
        );
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future<MbQuestion> getUserQuestion({String userID}) async {
    final result = await Auth.getSecurityQuestion(userID);
    return result.mbQuestions[0];
  }

  static Future validateCustomer(details) async {
    var key = details["key"];
    var context = details["context"];
    final def = Provider.of<Default>(context, listen: false);
    // final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (details["customerNumber"] != null &&
            details["customerNumber"].isNotEmpty) {
          var user = await Auth.validateCustomer(details);

          return user;
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future confirmCustDetails(details) async {
    var key = details["key"];
    var context = details["context"];
    final def = Provider.of<Default>(context, listen: false);
    // final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (details["customerNumber"] != null &&
            details["customerNumber"].isNotEmpty) {
          var user = await Auth.confirmCustomerDetails(details);

          return user;
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future registerCustomer(details) async {
    var key = details["key"];
    var context = details["context"];
    final def = Provider.of<Default>(context, listen: false);
    // final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (details["customerNumber"] != null &&
            details["customerNumber"].isNotEmpty) {
          var user = await Auth.registerCustomer(details);

          return user;
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }
}
