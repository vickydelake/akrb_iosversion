import 'package:akrbankios/APIs/beneficiaries_api.dart';
import 'package:akrbankios/Providers/default.dart';
import 'package:akrbankios/Providers/user_info.dart';
import 'package:akrbankios/Services/internetCheck.dart';
import 'package:akrbankios/Services/localStorage.dart';
import 'package:akrbankios/Services/serviceLocator.dart';
import 'package:akrbankios/common_widgets/constants.dart';
import 'package:akrbankios/models/beneficiary.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void _inform(
    GlobalKey<ScaffoldState> key, BuildContext context, String message) {
  key.currentState.showSnackBar(SnackBar(
    // width: MediaQuery.of(context).size.width * 0.88,
    padding: EdgeInsets.all(6.0),
    duration: Duration(milliseconds: 2000),
    backgroundColor: Colors.black.withOpacity(0.85),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(1.0),
      ),
    ),
    content: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          Icons.cancel,
          size: 28.0,
          color: Colors.red[200],
        ),
        SizedBox(width: 50),
        Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 17.0,
            letterSpacing: 0.7,
            fontWeight: FontWeight.w400,
            color: Colors.white,
          ),
        ),
      ],
    ),
  ));
}

class BeneController {
  static LocalStorageService storageService = locator<LocalStorageService>();

  static Future<List<MbBeneficiary>> getBeneController({
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    String authToken,
  }) async {
    final def = Provider.of<Default>(context, listen: false);
    final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (authToken != null) {
          var response = await Beneficiaries.getBeneficiaries(authToken);

          print(response);
          return response;
        } else {
          _inform(key, context, 'Please provide User auth token');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future<String> createBeneController(details) async {
    var key = details["key"];
    var context = details["context"];
    final def = Provider.of<Default>(context, listen: false);
    // final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (details["accountName"] != null &&
            details["accountName"].isNotEmpty &&
            details["accountNumber"] != null &&
            details["accountNumber"].isNotEmpty &&
            details["telephone"] != null &&
            details["telephone"].isNotEmpty) {
          var user = await Beneficiaries.createBeneficiary(details);

          return user;
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future<String> editBeneController(details) async {
    var key = details["key"];
    var context = details["context"];
    final def = Provider.of<Default>(context, listen: false);
    // final _user = Provider.of<UserInfo>(context, listen: false);
    int count = 0;
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (details["accountName"] != null &&
            details["accountName"].isNotEmpty &&
            details["accountNumber"] != null &&
            details["accountNumber"].isNotEmpty &&
            details["telephone"] != null &&
            details["telephone"].isNotEmpty &&
            details["beneficiaryCode"] != null &&
            details["beneficiaryCode"].isNotEmpty) {
          var response = await Beneficiaries.editBeneficiary(details);

          if (response != null) {
            Utilities.showSuccessDialog(
              context,
              response,
              () {
                Navigator.popUntil(context, (route) {
                  return count++ == 2;
                });
                // widget.onBeneficiaryCreated();
              },
            );
          } else {
            print(response);
          }
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future<String> deleteBeneController(details) async {
    var key = details["key"];
    var context = details["context"];
    final def = Provider.of<Default>(context, listen: false);
    // final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (details["authToken"] != null &&
            details["authToken"].isNotEmpty &&
            details["beneficiaryCode"] != null &&
            details["beneficiaryCode"].isNotEmpty) {
          var user = await Beneficiaries.deleteBeneficiary(details);

          return user;
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }

  static Future getAcctNameController(details) async {
    var key = details["key"];
    var context = details["context"];
    final def = Provider.of<Default>(context, listen: false);
    // final _user = Provider.of<UserInfo>(context, listen: false);
    try {
      final checkinternet = await internetCheck();
      def.setLoading(true);
      if (checkinternet) {
        if (details["authToken"] != null &&
            details["authToken"].isNotEmpty &&
            details["accountName"] != null &&
            details["accountName"].isNotEmpty) {
          var user = await Beneficiaries.getAccountName(details);

          return user;
        } else {
          _inform(key, context, 'Please fill all fields');
        }
      } else {
        _inform(key, context, 'Please check your Internet');
      }
    } on PlatformException catch (e) {
      _inform(key, context, e.message);
    } finally {
      def.setLoading(false);
    }
  }
}
