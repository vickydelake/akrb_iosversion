import 'package:flutter/material.dart';

class StatementRequest extends StatefulWidget {
  @override
  _StatementRequestState createState() => _StatementRequestState();
}

class _StatementRequestState extends State<StatementRequest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Color(0XFF339933),
      title: Text("Statememt Request",
          style: TextStyle(color: Colors.white, fontSize: 15)),
    ));
  }
}
