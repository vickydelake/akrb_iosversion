import 'package:flutter/material.dart';

class StopCheque extends StatefulWidget {
  @override
  _StopChequeState createState() => _StopChequeState();
}

class _StopChequeState extends State<StopCheque> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Color(0XFF339933),
      title: Text("Stop Cheque ",
          style: TextStyle(color: Colors.white, fontSize: 15)),
    ));
  }
}
