import 'package:flutter/material.dart';

class SalaryAdvance extends StatefulWidget {
  @override
  _SalaryAdvanceState createState() => _SalaryAdvanceState();
}

class _SalaryAdvanceState extends State<SalaryAdvance> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Color(0XFF339933),
      title: Text("Salary Advance ",
          style: TextStyle(color: Colors.white, fontSize: 15)),
    ));
  }
}
