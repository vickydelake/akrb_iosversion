import 'package:flutter/material.dart';

class Requests extends StatefulWidget {
  @override
  _RequestsState createState() => _RequestsState();
}

class _RequestsState extends State<Requests> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0XFF339933),
          title: Text(
            "Requests",
            style: TextStyle(color: Colors.white, fontSize: 18),
          )),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
              top: 10.0,
              left: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset("assets/statement.png"),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    "Statement Request",
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                ),
                Icon(
                  Icons.arrow_right,
                  size: 30,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 2,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
              left: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset("assets/chequebk_reqt.png"),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    "Cheque Book Request",
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                ),
                Icon(
                  Icons.arrow_right,
                  size: 30,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 2,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
              left: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset("assets/statement.png"),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    "Salary Advance",
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                ),
                Icon(
                  Icons.arrow_right,
                  size: 30,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 2,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
              left: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset("assets/chequebk_reqt.png"),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    "Stop Cheque",
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                ),
                Icon(
                  Icons.arrow_right,
                  size: 30,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 2,
          ),
        ],
      ),
    );
  }
}
