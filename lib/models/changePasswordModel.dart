// To parse this JSON data, do
//
//     final changepasswordModel = changepasswordModelFromJson(jsonString);

import 'dart:convert';

ChangepasswordModel changepasswordModelFromJson(String str) =>
    ChangepasswordModel.fromJson(json.decode(str));

String changepasswordModelToJson(ChangepasswordModel data) =>
    json.encode(data.toJson());

class ChangepasswordModel {
  dynamic mbUserId;
  String mbResponse;
  String lastModificationDate;
  String creationTime;
  dynamic tokenId;
  String id;
  String mbMessage;

  ChangepasswordModel({
    this.mbUserId,
    this.mbResponse,
    this.lastModificationDate,
    this.creationTime,
    this.tokenId,
    this.id,
    this.mbMessage,
  });

  factory ChangepasswordModel.fromJson(Map<String, dynamic> json) =>
      ChangepasswordModel(
        mbUserId: json["mb_userId"],
        mbResponse: json["mb_response"],
        lastModificationDate: json["lastModificationDate"],
        creationTime: json["creationTime"],
        tokenId: json["tokenId"],
        id: json["id"],
        mbMessage: json["mb_message"],
      );

  Map<String, dynamic> toJson() => {
        "mb_userId": mbUserId,
        "mb_response": mbResponse,
        "lastModificationDate": lastModificationDate,
        "creationTime": creationTime,
        "tokenId": tokenId,
        "id": id,
        "mb_message": mbMessage,
      };
}
