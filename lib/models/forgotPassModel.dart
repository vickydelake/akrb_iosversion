// To parse this JSON data, do
//
//     final forgetPassModel = forgetPassModelFromJson(jsonString);

import 'dart:convert';

ForgetPassModel forgetPassModelFromJson(String str) =>
    ForgetPassModel.fromJson(json.decode(str));

String forgetPassModelToJson(ForgetPassModel data) =>
    json.encode(data.toJson());

class ForgetPassModel {
  String mbUserId;
  String mbResponse;
  String lastModificationDate;
  String creationTime;
  dynamic tokenId;
  String id;
  String mbMessage;

  ForgetPassModel({
    this.mbUserId,
    this.mbResponse,
    this.lastModificationDate,
    this.creationTime,
    this.tokenId,
    this.id,
    this.mbMessage,
  });

  factory ForgetPassModel.fromJson(Map<String, dynamic> json) =>
      ForgetPassModel(
        mbUserId: json["mb_userId"],
        mbResponse: json["mb_response"],
        lastModificationDate: json["lastModificationDate"],
        creationTime: json["creationTime"],
        tokenId: json["tokenId"],
        id: json["id"],
        mbMessage: json["mb_message"],
      );

  Map<String, dynamic> toJson() => {
        "mb_userId": mbUserId,
        "mb_response": mbResponse,
        "lastModificationDate": lastModificationDate,
        "creationTime": creationTime,
        "tokenId": tokenId,
        "id": id,
        "mb_message": mbMessage,
      };
}
