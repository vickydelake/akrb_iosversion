// To parse this JSON data, do
//
//     final bioSetupModel = bioSetupModelFromJson(jsonString);

import 'dart:convert';

BioSetupModel bioSetupModelFromJson(String str) =>
    BioSetupModel.fromJson(json.decode(str));

String bioSetupModelToJson(BioSetupModel data) => json.encode(data.toJson());

class BioSetupModel {
  String mbResponse;
  String mbMessage;

  BioSetupModel({
    this.mbResponse,
    this.mbMessage,
  });

  factory BioSetupModel.fromJson(Map<String, dynamic> json) => BioSetupModel(
        mbResponse: json["mb_response"],
        mbMessage: json["mb_message"],
      );

  Map<String, dynamic> toJson() => {
        "mb_response": mbResponse,
        "mb_message": mbMessage,
      };
}
