class ListModel {
  final String val;
  final String description;

  ListModel({this.val, this.description});
}
