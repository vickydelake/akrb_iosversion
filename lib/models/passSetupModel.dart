// To parse this JSON data, do
//
//     final passConfigModel = passConfigModelFromJson(jsonString);

import 'dart:convert';

PassConfigModel passConfigModelFromJson(String str) => PassConfigModel.fromJson(json.decode(str));

String passConfigModelToJson(PassConfigModel data) => json.encode(data.toJson());

class PassConfigModel {
    String mbUserId;
    String mbResponse;
    String lastModificationDate;
    String creationTime;
    String mbToken;
    String mbMessage;

    PassConfigModel({
        this.mbUserId,
        this.mbResponse,
        this.lastModificationDate,
        this.creationTime,
        this.mbToken,
        this.mbMessage,
    });

    factory PassConfigModel.fromJson(Map<String, dynamic> json) => PassConfigModel(
        mbUserId: json["mb_userId"],
        mbResponse: json["mb_response"],
        lastModificationDate: json["lastModificationDate"],
        creationTime: json["creationTime"],
        mbToken: json["mb_token"],
        mbMessage: json["mb_message"],
    );

    Map<String, dynamic> toJson() => {
        "mb_userId": mbUserId,
        "mb_response": mbResponse,
        "lastModificationDate": lastModificationDate,
        "creationTime": creationTime,
        "mb_token": mbToken,
        "mb_message": mbMessage,
    };
}
