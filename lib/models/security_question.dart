// To parse this JSON data, do
//
//     final securityQuestion = securityQuestionFromJson(jsonString);

import 'dart:convert';

SecurityQuestion securityQuestionFromJson(String str) =>
    SecurityQuestion.fromJson(json.decode(str));

String securityQuestionToJson(SecurityQuestion data) =>
    json.encode(data.toJson());

class SecurityQuestion {
  String mbResponse;
  List<MbQuestion> mbQuestions;
  String mbMessage;

  SecurityQuestion({
    this.mbResponse,
    this.mbQuestions,
    this.mbMessage,
  });

  factory SecurityQuestion.fromJson(Map<String, dynamic> json) =>
      SecurityQuestion(
        mbResponse: json["mb_response"],
        mbQuestions: List<MbQuestion>.from(
            json["mb_questions"].map((x) => MbQuestion.fromJson(x))),
        mbMessage: json["mb_message"],
      );

  Map<String, dynamic> toJson() => {
        "mb_response": mbResponse,
        "mb_questions": List<dynamic>.from(mbQuestions.map((x) => x.toJson())),
        "mb_message": mbMessage,
      };
}

class MbQuestion {
  String code;
  String question;

  MbQuestion({
    this.code,
    this.question,
  });

  factory MbQuestion.fromJson(Map<String, dynamic> json) => MbQuestion(
        code: json["code"],
        question: json["question"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "question": question,
      };
}
