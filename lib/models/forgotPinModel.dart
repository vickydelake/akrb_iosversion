// To parse this JSON data, do
//
//     final forgotPinModel = forgotPinModelFromJson(jsonString);

import 'dart:convert';

ForgotPinModel forgotPinModelFromJson(String str) =>
    ForgotPinModel.fromJson(json.decode(str));

String forgotPinModelToJson(ForgotPinModel data) => json.encode(data.toJson());

class ForgotPinModel {
  String mbAccDetails;
  String mbResponse;

  ForgotPinModel({
    this.mbAccDetails,
    this.mbResponse,
  });

  factory ForgotPinModel.fromJson(Map<String, dynamic> json) => ForgotPinModel(
        mbAccDetails: json["mb_accDetails"],
        mbResponse: json["mb_response"],
      );

  Map<String, dynamic> toJson() => {
        "mb_accDetails": mbAccDetails,
        "mb_response": mbResponse,
      };
}
