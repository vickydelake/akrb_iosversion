// To parse this JSON data, do
//
//     final accounts = accountsFromJson(jsonString);

import 'dart:convert';

Accounts accountsFromJson(String str) => Accounts.fromJson(json.decode(str));

String accountsToJson(Accounts data) => json.encode(data.toJson());

class Accounts {
  List<Datum> data;
  String authToken;
  String message;
  String responseCode;

  Accounts({
    this.data,
    this.authToken,
    this.message,
    this.responseCode,
  });

  factory Accounts.fromJson(Map<String, dynamic> json) => Accounts(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        authToken: json["authToken"],
        message: json["message"],
        responseCode: json["responseCode"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "authToken": authToken,
        "message": message,
        "responseCode": responseCode,
      };
}

class Datum {
  String accountName;
  String ledgerBalance;
  String accountType;
  String currency;
  String accountNumber;
  String customerNumber;
  String availableBalance;

  Datum({
    this.accountName,
    this.ledgerBalance,
    this.accountType,
    this.currency,
    this.accountNumber,
    this.customerNumber,
    this.availableBalance,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        accountName: json["accountName"],
        ledgerBalance: json["ledgerBalance"],
        accountType: json["accountType"],
        currency: json["currency"],
        accountNumber: json["accountNumber"],
        customerNumber: json["customerNumber"],
        availableBalance: json["availableBalance"],
      );

  Map<String, dynamic> toJson() => {
        "accountName": accountName,
        "ledgerBalance": ledgerBalance,
        "accountType": accountType,
        "currency": currency,
        "accountNumber": accountNumber,
        "customerNumber": customerNumber,
        "availableBalance": availableBalance,
      };
}
