// To parse this JSON data, do
//
//     final validatedCustomer = validatedCustomerFromJson(jsonString);

import 'dart:convert';

ValidatedCustomer validatedCustomerFromJson(String str) =>
    ValidatedCustomer.fromJson(json.decode(str));

String validatedCustomerToJson(ValidatedCustomer data) =>
    json.encode(data.toJson());

class ValidatedCustomer {
  String mbResponse;
  String lastModificationDate;
  String mbCustomerNumber;
  String mbIdType;
  String creationTime;
  String tokenId;
  String mbCustomerName;
  String id;

  ValidatedCustomer({
    this.mbResponse,
    this.lastModificationDate,
    this.mbCustomerNumber,
    this.mbIdType,
    this.creationTime,
    this.tokenId,
    this.mbCustomerName,
    this.id,
  });

  factory ValidatedCustomer.fromJson(Map<String, dynamic> json) =>
      ValidatedCustomer(
        mbResponse: json["mb_response"],
        lastModificationDate: json["lastModificationDate"],
        mbCustomerNumber: json["mb_customerNumber"],
        mbIdType: json["mb_idType"],
        creationTime: json["creationTime"],
        tokenId: json["tokenId"],
        mbCustomerName: json["mb_customerName"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "mb_response": mbResponse,
        "lastModificationDate": lastModificationDate,
        "mb_customerNumber": mbCustomerNumber,
        "mb_idType": mbIdType,
        "creationTime": creationTime,
        "tokenId": tokenId,
        "mb_customerName": mbCustomerName,
        "id": id,
      };
}
