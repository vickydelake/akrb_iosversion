// To parse this JSON data, do
//
//     final pinConfigModel = pinConfigModelFromJson(jsonString);

import 'dart:convert';

PinConfigModel pinConfigModelFromJson(String str) => PinConfigModel.fromJson(json.decode(str));

String pinConfigModelToJson(PinConfigModel data) => json.encode(data.toJson());

class PinConfigModel {
  String mbResponse;
  String mbToken;
  String mbMessage;

  PinConfigModel({
    this.mbResponse,
    this.mbToken,
    this.mbMessage,
  });

  factory PinConfigModel.fromJson(Map<String, dynamic> json) => PinConfigModel(
    mbResponse: json["mb_response"],
    mbToken: json["mb_token"],
    mbMessage: json["mb_message"],
  );

  Map<String, dynamic> toJson() => {
    "mb_response": mbResponse,
    "mb_token": mbToken,
    "mb_message": mbMessage,
  };
}
