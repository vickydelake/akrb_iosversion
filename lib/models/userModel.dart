// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  String mbUserId;
  String updateFlag;
  String creationTime;
  String tokenId;
  String mbInternal;
  List<Question> questions;
  bool changePass;
  String mbExternal;
  String mbAccDetails;
  bool setPin;
  String mbResponse;
  String lastModificationDate;
  List<MbTransDetail> mbTransDetails;
  bool hardReset;
  String id;
  String mbUserName;

  User({
    this.mbUserId,
    this.updateFlag,
    this.creationTime,
    this.tokenId,
    this.mbInternal,
    this.questions,
    this.changePass,
    this.mbExternal,
    this.mbAccDetails,
    this.setPin,
    this.mbResponse,
    this.lastModificationDate,
    this.mbTransDetails,
    this.hardReset,
    this.id,
    this.mbUserName,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        mbUserId: json["mb_userId"],
        updateFlag: json["updateFlag"],
        creationTime: json["creationTime"],
        tokenId: json["tokenId"],
        mbInternal: json["mb_internal"],
        questions: json["questions"] != null
            ? List<Question>.from(
                json["questions"].map((x) => Question.fromJson(x)))
            : List<Question>(),
        changePass: json["changePass"],
        mbExternal: json["mb_external"],
        mbAccDetails: json["mb_accDetails"],
        setPin: json["setPin"],
        mbResponse: json["mb_response"],
        lastModificationDate: json["lastModificationDate"],
        mbTransDetails: json["mb_transDetails"] != null
            ? List<MbTransDetail>.from(
                json["mb_transDetails"].map((x) => MbTransDetail.fromJson(x)))
            : List<MbTransDetail>(),
        hardReset: json["hardReset"],
        id: json["id"],
        mbUserName: json["mb_userName"],
      );

  Map<String, dynamic> toJson() => {
        "mb_userId": mbUserId,
        "updateFlag": updateFlag,
        "creationTime": creationTime,
        "tokenId": tokenId,
        "mb_internal": mbInternal,
        "questions": List<dynamic>.from(questions.map((x) => x.toJson())),
        "changePass": changePass,
        "mb_external": mbExternal,
        "mb_accDetails": mbAccDetails,
        "setPin": setPin,
        "mb_response": mbResponse,
        "lastModificationDate": lastModificationDate,
        "mb_transDetails":
            List<dynamic>.from(mbTransDetails.map((x) => x.toJson())),
        "hardReset": hardReset,
        "id": id,
        "mb_userName": mbUserName,
      };
}

class MbTransDetail {
  String accountName;
  String ledgerBalance;
  String accountType;
  String currency;
  String customerNumber;
  String accountNumber;
  String availableBalance;

  MbTransDetail({
    this.accountName,
    this.ledgerBalance,
    this.accountType,
    this.currency,
    this.customerNumber,
    this.accountNumber,
    this.availableBalance,
  });

  factory MbTransDetail.fromJson(Map<String, dynamic> json) => MbTransDetail(
        accountName: json["accountName"],
        ledgerBalance: json["ledgerBalance"],
        accountType: json["accountType"],
        currency: json["currency"],
        customerNumber: json["customerNumber"],
        accountNumber: json["accountNumber"],
        availableBalance: json["availableBalance"],
      );

  Map<String, dynamic> toJson() => {
        "accountName": accountName,
        "ledgerBalance": ledgerBalance,
        "accountType": accountType,
        "currency": currency,
        "customerNumber": customerNumber,
        "accountNumber": accountNumber,
        "availableBalance": availableBalance,
      };
}

class Question {
  String code;
  String description;

  Question({
    this.code,
    this.description,
  });

  factory Question.fromJson(Map<String, dynamic> json) => Question(
        code: json["code"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "description": description,
      };
}
