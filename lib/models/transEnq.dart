// To parse this JSON data, do
//
//     final transEnq = transEnqFromJson(jsonString);

import 'dart:convert';

TransEnq transEnqFromJson(String str) => TransEnq.fromJson(json.decode(str));

String transEnqToJson(TransEnq data) => json.encode(data.toJson());

class TransEnq {
  String mbResponse;
  List<MbTransaction> mbTransactions;
  String mbMessage;

  TransEnq({
    this.mbResponse,
    this.mbTransactions,
    this.mbMessage,
  });

  factory TransEnq.fromJson(Map<String, dynamic> json) => TransEnq(
        mbResponse: json["mb_response"],
        mbTransactions: List<MbTransaction>.from(
            json["mb_transactions"].map((x) => MbTransaction.fromJson(x))),
        mbMessage: json["mb_message"],
      );

  Map<String, dynamic> toJson() => {
        "mb_response": mbResponse,
        "mb_transactions":
            List<dynamic>.from(mbTransactions.map((x) => x.toJson())),
        "mb_message": mbMessage,
      };
}

class MbTransaction {
  String postingSysDate;
  String amount;
  String narration;
  String transactionNumber;
  String postingSysTime;
  String documentReference;
  String valueDate;
  String branch;
  String runningBalance;
  String batchNumber;

  MbTransaction({
    this.postingSysDate,
    this.amount,
    this.narration,
    this.transactionNumber,
    this.postingSysTime,
    this.documentReference,
    this.valueDate,
    this.branch,
    this.runningBalance,
    this.batchNumber,
  });

  factory MbTransaction.fromJson(Map<String, dynamic> json) => MbTransaction(
        postingSysDate: json["postingSysDate"],
        amount: json["amount"],
        narration: json["narration"],
        transactionNumber: json["transactionNumber"],
        postingSysTime: json["postingSysTime"],
        documentReference: json["documentReference"],
        valueDate: json["valueDate"],
        branch: json["branch"],
        runningBalance: json["runningBalance"],
        batchNumber: json["batchNumber"],
      );

  Map<String, dynamic> toJson() => {
        "postingSysDate": postingSysDate,
        "amount": amount,
        "narration": narration,
        "transactionNumber": transactionNumber,
        "postingSysTime": postingSysTime,
        "documentReference": documentReference,
        "valueDate": valueDate,
        "branch": branch,
        "runningBalance": runningBalance,
        "batchNumber": batchNumber,
      };
}
