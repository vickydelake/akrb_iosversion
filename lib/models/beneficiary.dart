// To parse this JSON data, do
//
//     final beneficiary = beneficiaryFromJson(jsonString);

import 'dart:convert';

Beneficiary beneficiaryFromJson(String str) =>
    Beneficiary.fromJson(json.decode(str));

String beneficiaryToJson(Beneficiary data) => json.encode(data.toJson());

class Beneficiary {
  String mbResponse;
  String mbMessage;
  List<MbBeneficiary> mbBeneficiary;

  Beneficiary({
    this.mbResponse,
    this.mbMessage,
    this.mbBeneficiary,
  });

  factory Beneficiary.fromJson(Map<String, dynamic> json) => Beneficiary(
        mbResponse: json["mb_response"],
        mbMessage: json["mb_message"],
        mbBeneficiary: List<MbBeneficiary>.from(
            json["mb_beneficiary"].map((x) => MbBeneficiary.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mb_response": mbResponse,
        "mb_message": mbMessage,
        "mb_beneficiary":
            List<dynamic>.from(mbBeneficiary.map((x) => x.toJson())),
      };
}

class MbBeneficiary {
  String bankCode;
  String code;
  String telephone;
  String name;
  String account;

  MbBeneficiary({
    this.bankCode,
    this.code,
    this.telephone,
    this.name,
    this.account,
  });

  factory MbBeneficiary.fromJson(Map<String, dynamic> json) => MbBeneficiary(
        bankCode: json["bankCode"],
        code: json["code"],
        telephone: json["telephone"],
        name: json["name"],
        account: json["account"],
      );

  Map<String, dynamic> toJson() => {
        "bankCode": bankCode,
        "code": code,
        "telephone": telephone,
        "name": name,
        "account": account,
      };
}
